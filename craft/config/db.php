<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost:8889',
        'database' => 'bark-and-fly',
        'user' => 'root',
        'password' => 'root'
    ),

    'baf.loc' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
        'database' => 'bark-and-fly',
        'user' => 'root',
        'password' => ''
    ),

    'staging.website.barknfly.newborne.co.uk' => array(
        'server' => '127.0.0.1',
        'database' => 'barknfly_craftcms_website',
        'user' => 'barknfly_website',
        'password' => 'Zan5gEZn3MATaEYb',
    ),
    'website.barknfly.newborne.co.uk' => array(
        'server' => '127.0.0.1',
        'database' => 'barknfly_craftcms_website',
        'user' => 'barknfly_website',
        'password' => 'Zan5gEZn3MATaEYb',
    )

);
