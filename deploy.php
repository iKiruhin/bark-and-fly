<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'barknfly.brand.website');

// Project repository
set('repository', 'git@bitbucket.org-ru:newborn_k/craft-based-web.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', [
    'public/assets/craft/site',
    'public/uploads',
    'public/imager',
    'craft/storage'
]);

// Writable dirs by web server
set('writable_dirs', ['craft/config']);
set('allow_anonymous_stats', false);

set('writable_mode', 'chmod');
//set('writable_use_sudo', true);
set('writable_chmod_mode', '777');
//set('cleanup_use_sudo', true);

// Hosts

host('89.145.85.40')
    ->user('deploy')
    ->stage('staging')
    ->set('branch', 'master')
    ->set('deploy_path', '~/sites/{{application}}.staging');

// Tasks

desc('Deploy Bark-And-Fly project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:npm',
    'deploy:gulp',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

task('deploy:npm', 'npm install');
task('deploy:gulp', 'gulp build');

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

