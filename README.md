# Bark And Fly

## Installation


If you want to install the site locally, follow these instructions:

1. Download/clone the repo on your computer

		> git clone https://iKiruhin@bitbucket.org/newborn_k/craft-based-web.git

2. Set the permissions on craft-based-web/craft/storage/ to 777

		> cd craft-based-web
		> chmod 777 craft/storage

3. Set the permissions on craft-based-web/craft/config/ to 744, 774, or 777 depending on the relationship between the user that Apache/PHP is running as and the user who owns the happylager/craft/config folder. (See the [Craft installation docs](https://craftcms.com/docs/installing#step-2-set-the-permissions) for details.)

		> chmod 774 craft/config

4. Create a new MySQL database called `bark-and-fly`.

5. Fill in the proper MySQL credentials in craft-based-web/craft/config/db.php (user and password).

6. Create a new virtual host http://{yourHostForBarkAndFlyProject} that points to the `craft-based-web/public/` folder.


Now you should be able to point your web browser to http://{yourHostForBarkAndFlyProject}/admin. You should either see a Craft login screen, or a prompt telling you that some database updates need to be run. If it’s the latter, just click “Finish up”.

Now point your browser at http://{yourHostForBarkAndFlyProject}/. You should see the Happy Lager homepage.

For enable auto reload browser page, install LiveReload extension: https://chrome.google
.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei
