-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:8889
-- Время создания: Май 04 2018 г., 13:41
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `bark-and-fly`
--

-- --------------------------------------------------------

--
-- Структура таблицы `craft_assetfiles`
--

CREATE TABLE `craft_assetfiles` (
  `id` int(11) NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kind` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `width` int(11) UNSIGNED DEFAULT NULL,
  `height` int(11) UNSIGNED DEFAULT NULL,
  `size` bigint(20) UNSIGNED DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_assetfiles`
--

INSERT INTO `craft_assetfiles` (`id`, `sourceId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (7, 1, 1, 'logo_inverse.png', 'image', 384, 210, 27647, '2018-04-02 11:34:19', '2018-04-02 11:34:19', '2018-04-02 11:34:19', '6cea7814-12e0-4158-9358-dbe1e7ebf0d3'),
  (8, 1, 1, 'logo.png', 'image', 190, 103, 11941, '2018-04-02 11:34:19', '2018-04-02 11:34:20', '2018-04-02 11:34:20', 'b5832c6c-894a-42cb-ab07-3fd910a536d5'),
  (22, 1, 1, 'facebook.png', 'image', 26, 26, 254, '2018-04-03 15:07:10', '2018-04-03 15:07:10', '2018-04-03 15:07:10', '8627a3ca-fe48-464e-880c-63ad46f0be4a'),
  (23, 1, 1, 'instagram.png', 'image', 34, 34, 4726, '2018-04-03 15:07:10', '2018-04-03 15:07:11', '2018-04-03 15:07:11', '817b6f3f-d532-48f1-890b-f9cfc1959bb8'),
  (24, 1, 1, 'twitter.png', 'image', 27, 27, 596, '2018-04-03 15:07:11', '2018-04-03 15:07:11', '2018-04-03 15:07:11', '7b5e116a-ecce-46b1-a8e9-04b53e7accfa'),
  (25, 1, 1, 'youtube.png', 'image', 47, 19, 767, '2018-04-03 15:07:11', '2018-04-03 15:07:11', '2018-04-03 15:07:11', 'c41d5fc6-a393-4363-ab79-fbc5c9def53b'),
  (30, 2, 2, 'art_cover_welcome_2.jpg', 'image', 1440, 440, 438585, '2018-04-03 16:29:27', '2018-04-03 16:29:27', '2018-04-03 16:29:27', 'c5f03467-9e30-447a-9155-e5257e4a733c'),
  (31, 2, 2, 'art_cover_welcome.jpg', 'image', 1440, 440, 402540, '2018-04-03 16:29:28', '2018-04-03 16:29:28', '2018-04-03 16:29:28', 'e5e5bf29-ba61-46c3-bd99-166f7059adcf'),
  (34, 2, 2, 'art_testimonial_1.jpg', 'image', 370, 370, 95774, '2018-04-05 10:15:06', '2018-04-05 10:15:06', '2018-04-05 10:15:06', '77e83013-1a2a-4121-9d8b-719828aa7ad0'),
  (38, 1, 1, 'ad_background.png', 'image', 2880, 900, 2402933, '2018-04-05 10:32:43', '2018-04-05 10:32:44', '2018-04-05 10:33:14', '18413ea2-9023-4aa3-86af-e878088ffffd'),
  (40, 1, 1, 'card_1.png', 'image', 760, 457, 491064, '2018-04-05 11:32:26', '2018-04-05 11:32:26', '2018-04-05 11:32:26', '054a0609-c245-485f-b853-660fa3846855'),
  (41, 1, 1, 'card_3.png', 'image', 760, 458, 596224, '2018-04-05 11:32:27', '2018-04-05 11:32:27', '2018-04-05 11:32:27', '4bb9640b-2777-4c06-9bef-f6b3f17faa02'),
  (42, 1, 1, 'card_4.png', 'image', 760, 458, 523982, '2018-04-05 11:32:28', '2018-04-05 11:32:28', '2018-04-05 11:32:28', '592313f5-bf02-4915-bfa7-a7bbf8a27a4b'),
  (47, 3, 3, 'service_1.png', 'image', 1160, 772, 753721, '2018-04-06 07:41:01', '2018-04-06 07:41:01', '2018-04-06 07:41:01', '52cdb103-e123-494e-ba6c-5d6f88e96809'),
  (48, 3, 3, 'service_2.png', 'image', 1160, 772, 844342, '2018-04-06 07:41:24', '2018-04-06 07:41:24', '2018-04-06 07:41:24', '2852db68-93ca-4942-bf66-f1fb87108eba'),
  (49, 3, 3, 'service_3.png', 'image', 1160, 772, 772768, '2018-04-06 07:41:46', '2018-04-06 07:41:46', '2018-04-06 07:41:46', '28320a96-681c-42fa-a2de-842fa4881101'),
  (50, 3, 3, 'service_4.png', 'image', 1160, 772, 988617, '2018-04-06 07:42:09', '2018-04-06 07:42:09', '2018-04-06 07:42:09', '39b6d854-760b-4303-a6d1-7de33731792c'),
  (51, 3, 3, 'art_cover_service.jpg', 'image', 2160, 675, 233633, '2018-04-06 07:50:02', '2018-04-06 07:50:02', '2018-04-06 07:50:02', '93ac6b4d-3c93-4a25-a4dc-dc14d20013c2'),
  (59, 4, 4, 'art_cover_about.jpg', 'image', 1440, 390, 261139, '2018-04-06 12:21:31', '2018-04-06 12:21:32', '2018-04-06 12:21:32', '5b9dd671-9d25-4be9-a6f9-fe1d9ff32f96'),
  (60, 4, 4, 'art_cover_facilities.jpg', 'image', 2160, 675, 178365, '2018-04-06 12:21:33', '2018-04-06 12:21:33', '2018-04-06 12:21:33', '9869c3a0-cec3-4579-b12c-94bf4a82d3bb'),
  (61, 4, 4, 'art_cover_pricing.jpg', 'image', 1440, 390, 348911, '2018-04-06 12:21:33', '2018-04-06 12:21:33', '2018-04-06 12:21:33', 'cfc7f090-2b65-4955-b3fe-f19f5f0e6d06'),
  (63, 4, 4, 'art_cover_training.png', 'image', 2880, 900, 1680622, '2018-04-06 12:22:28', '2018-04-06 12:22:29', '2018-04-06 12:22:29', '727c6b5a-323c-4d22-b06a-21f381e9caa0'),
  (66, 4, 4, 'art_cover_service.jpg', 'image', 2160, 675, 233633, '2018-04-06 12:21:34', '2018-04-06 12:25:08', '2018-04-06 12:25:08', 'a8204951-bb92-4441-881c-9f1df82cb19a'),
  (67, 4, 4, 'art_cover_welcome_2.jpg', 'image', 1440, 440, 438585, '2018-04-06 12:22:30', '2018-04-06 12:25:08', '2018-04-06 12:25:08', 'ececc3d7-ce6e-47e5-9294-4af200cc5fc4'),
  (68, 4, 4, 'art_cover_welcome.jpg', 'image', 1440, 440, 402540, '2018-04-06 12:22:31', '2018-04-06 12:25:08', '2018-04-06 12:25:08', '6c58555c-47c2-4425-b5f6-09fcafce85a7'),
  (69, 5, 5, 'facility_1_1.png', 'image', 641, 531, 471987, '2018-04-06 12:35:06', '2018-04-06 12:35:06', '2018-04-06 12:35:06', 'ad859c76-567b-4ca4-9bf0-36d4c386313c'),
  (70, 5, 5, 'facility_1_2.png', 'image', 200, 158, 60335, '2018-04-06 12:35:07', '2018-04-06 12:35:07', '2018-04-06 12:35:07', '70a8433e-5f1e-4765-89bf-987e3a1eaa8b'),
  (71, 5, 5, 'facility_1_3.png', 'image', 200, 158, 64214, '2018-04-06 12:35:08', '2018-04-06 12:35:08', '2018-04-06 12:35:08', '598d9596-dfac-4569-a247-a48b6046c136'),
  (72, 5, 5, 'facility_2_1.png', 'image', 641, 531, 585884, '2018-04-06 12:35:17', '2018-04-06 12:35:17', '2018-04-06 12:35:17', '6001802b-28b4-45ec-9d96-93029eb9772c'),
  (73, 5, 5, 'facility_2_2.png', 'image', 200, 159, 71807, '2018-04-06 12:35:17', '2018-04-06 12:35:17', '2018-04-06 12:35:17', '95e60ce2-cdb1-4db3-9efa-e6eb35b84963'),
  (74, 5, 5, 'facility_2_3.png', 'image', 200, 159, 55654, '2018-04-06 12:35:18', '2018-04-06 12:35:18', '2018-04-06 12:35:18', '758ed5e0-3808-4e6f-b9aa-3a9bf08274aa'),
  (75, 5, 5, 'facility_3_1.png', 'image', 641, 531, 456971, '2018-04-06 12:35:32', '2018-04-06 12:35:33', '2018-04-06 12:35:33', 'ed0f2a04-6720-4b95-92ea-d27c4d81ea2e'),
  (76, 5, 5, 'facility_3_2.png', 'image', 198, 158, 52928, '2018-04-06 12:35:34', '2018-04-06 12:35:34', '2018-04-06 12:35:34', '0294ce5d-60a1-4d59-8ce5-a9263534c9b9'),
  (77, 5, 5, 'facility_3_3.png', 'image', 198, 158, 56595, '2018-04-06 12:35:35', '2018-04-06 12:35:35', '2018-04-06 12:35:35', '5741a1b0-6351-4b63-97a3-bd90b508211f'),
  (78, 5, 5, 'facility_4_1.png', 'image', 641, 531, 673228, '2018-04-06 12:35:42', '2018-04-06 12:35:42', '2018-04-06 12:35:42', '7a9e9301-b219-4ebe-b9d9-4707cba8e373'),
  (79, 5, 5, 'facility_4_2.png', 'image', 198, 159, 66055, '2018-04-06 12:35:43', '2018-04-06 12:35:43', '2018-04-06 12:35:43', '1636a1bd-eccb-4782-ab23-6bc653dfe790'),
  (80, 5, 5, 'facility_4_3.png', 'image', 198, 159, 58248, '2018-04-06 12:35:44', '2018-04-06 12:35:44', '2018-04-06 12:35:44', '7dafe513-394d-4b59-b82a-054066be0f76'),
  (81, 5, 5, 'facility_5_1.png', 'image', 641, 531, 588289, '2018-04-06 12:35:57', '2018-04-06 12:35:58', '2018-04-06 12:35:58', '38623b6e-29d5-4ccf-a7a1-f400d8fc9425'),
  (82, 5, 5, 'facility_5_2.png', 'image', 198, 159, 51515, '2018-04-06 12:35:58', '2018-04-06 12:35:58', '2018-04-06 12:35:58', '01c072fc-d705-44d5-816a-ef829a35c4da'),
  (83, 5, 5, 'facility_5_3.png', 'image', 198, 159, 70049, '2018-04-06 12:35:59', '2018-04-06 12:35:59', '2018-04-06 12:35:59', '1c8628d0-4fdc-4495-ad20-65b7b4eb6198'),
  (84, 5, 5, 'facility_6_1.png', 'image', 641, 531, 469913, '2018-04-06 12:36:14', '2018-04-06 12:36:14', '2018-04-06 12:36:14', 'ba253c78-8719-4bf4-a5e5-eaf99836338e'),
  (85, 5, 5, 'facility_6_2.png', 'image', 197, 158, 60714, '2018-04-06 12:36:15', '2018-04-06 12:36:15', '2018-04-06 12:36:15', 'c9935a7d-72ff-4b91-8ab0-30640bc58b75'),
  (86, 5, 5, 'facility_6_3.png', 'image', 197, 158, 53148, '2018-04-06 12:36:16', '2018-04-06 12:36:16', '2018-04-06 12:36:16', '49571af2-2ac6-4dc8-81cb-833d6cf3ba95'),
  (95, 6, 6, 'training_video.png', 'image', 2360, 1232, 2465601, '2018-04-09 14:10:27', '2018-04-09 14:10:27', '2018-04-09 14:10:27', '4fb671de-19f6-40a9-a552-17ad0ecaa187'),
  (96, 6, 6, 'training_1.png', 'image', 1142, 630, 813339, '2018-04-10 07:57:19', '2018-04-10 07:57:19', '2018-04-10 07:57:19', '87ac6643-1bdf-4b2a-af42-2898dbb67953'),
  (97, 6, 6, 'training_2.png', 'image', 1140, 630, 1007321, '2018-04-10 07:57:20', '2018-04-10 07:57:20', '2018-04-10 07:57:20', '51acd8e8-00ea-4d6e-9033-d5f885f0bf31'),
  (98, 6, 6, 'training_3.png', 'image', 1140, 630, 694292, '2018-04-10 07:57:23', '2018-04-10 07:57:23', '2018-04-10 07:57:23', 'de79bc0f-17d3-4066-9146-aedc29d69f12'),
  (99, 6, 6, 'training_4.png', 'image', 1140, 630, 1384409, '2018-04-10 07:57:25', '2018-04-10 07:57:25', '2018-04-10 07:57:25', '67a4551f-05dc-4924-bff9-5092a60f9101'),
  (110, 1, 1, 'about_video.png', 'image', 2360, 1232, 3269212, '2018-04-10 14:58:56', '2018-04-10 14:58:57', '2018-04-10 14:58:57', 'a66123cd-f096-4b4d-bc80-2a80384db5fd'),
  (121, 7, 7, 'flat_grid_view_1.jpg', 'image', 1440, 410, 278685, '2018-04-16 12:07:03', '2018-04-16 12:07:03', '2018-04-16 12:07:03', 'b6d0e0d9-76ba-4573-99d9-497aecacab3a'),
  (122, 7, 7, 'flat_grid_view_2.jpg', 'image', 720, 410, 206384, '2018-04-16 12:07:04', '2018-04-16 12:07:04', '2018-04-16 12:07:04', '34e229f7-6139-485b-8d4f-39c55a3f265e'),
  (123, 7, 7, 'flat_grid_view_3.jpg', 'image', 720, 410, 244458, '2018-04-16 12:07:04', '2018-04-16 12:07:04', '2018-04-16 12:07:04', '9d9e97ae-4509-46b6-9828-2ad035a22342'),
  (161, 4, 4, 'pupparazzi-cover.png', 'image', 1440, 450, 1224247, '2018-04-30 17:51:30', '2018-04-30 17:51:30', '2018-04-30 17:51:30', 'ff669efc-466c-4d8f-b419-c6f1055b5b99');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_assetfolders`
--

CREATE TABLE `craft_assetfolders` (
  `id` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_assetfolders`
--

INSERT INTO `craft_assetfolders` (`id`, `parentId`, `sourceId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, NULL, 1, 'Common Site Assets', '', '2018-04-02 10:00:46', '2018-04-02 10:00:46', '42685989-011d-401f-a8e4-ae15c6256c77'),
  (2, NULL, 2, 'Carouseles', '', '2018-04-03 16:27:07', '2018-04-05 10:13:43', '57c57f20-bdb0-48a6-b91d-c84310540803'),
  (3, NULL, 3, 'Services', '', '2018-04-06 07:39:29', '2018-04-06 07:39:29', '043985bb-5e61-4bda-b784-037c482c4d44'),
  (4, NULL, 4, 'Page Hovers', '', '2018-04-06 12:21:02', '2018-04-09 10:56:28', 'ba0ebee7-0f8b-48da-94e4-12180f6a3262'),
  (5, NULL, 5, 'Facilities', '', '2018-04-06 12:34:30', '2018-04-06 12:34:30', '4fdef6b2-45b3-4b33-877c-a19a09f48f64'),
  (6, NULL, 6, 'Trainings', '', '2018-04-09 13:53:36', '2018-04-09 13:53:36', '67b1b1ed-1528-4cb4-acfb-e821cd0d9316'),
  (7, NULL, 7, 'Pricing Page', '', '2018-04-16 12:06:39', '2018-04-16 12:06:39', '06150248-eba9-4afe-b820-d4b65a1ef6d6');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_assetindexdata`
--

CREATE TABLE `craft_assetindexdata` (
  `id` int(11) NOT NULL,
  `sessionId` varchar(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `sourceId` int(10) NOT NULL,
  `offset` int(10) NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recordId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_assetsources`
--

CREATE TABLE `craft_assetsources` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_assetsources`
--

INSERT INTO `craft_assetsources` (`id`, `name`, `handle`, `type`, `settings`, `sortOrder`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 'Common Site Assets', 'commonSiteAssets', 'Local', '{\"path\":\"{craftAssetsBasePath}\\/site\\/\",\"publicURLs\":\"1\",\"url\":\"{craftAssetsBaseUrl}\\/site\\/\"}', 1, 15, '2018-04-02 10:00:46', '2018-04-02 11:34:00', '143deac3-d41d-4320-8046-017946387849'),
  (2, 'Carouseles', 'carouseles', 'Local', '{\"path\":\"{craftAssetsBasePath}\\/site\\/\",\"publicURLs\":\"1\",\"url\":\"{craftAssetsBaseUrl}\\/site\\/\"}', 2, 106, '2018-04-03 16:27:07', '2018-04-06 07:40:09', 'bac1ebca-1e42-4e36-a196-5bf393d2f19a'),
  (3, 'Services', 'services', 'Local', '{\"path\":\"{craftAssetsBasePath}\\/site\\/\",\"publicURLs\":\"1\",\"url\":\"{craftAssetsBaseUrl}\\/site\\/\"}', 3, 105, '2018-04-06 07:39:28', '2018-04-06 07:39:28', 'ade0204a-4a12-4227-8a75-21e9f5da1d04'),
  (4, 'Page Hovers', 'pageHovers', 'Local', '{\"path\":\"{craftAssetsBasePath}\\/site\\/\",\"publicURLs\":\"1\",\"url\":\"{craftAssetsBaseUrl}\\/site\\/\"}', 4, 131, '2018-04-06 12:21:02', '2018-04-09 14:06:37', '357cb6ba-5fd9-4506-94f3-e4b2a19b3f26'),
  (5, 'Facilities', 'facilities', 'Local', '{\"path\":\"{craftAssetsBasePath}\\/site\\/\",\"publicURLs\":\"1\",\"url\":\"{craftAssetsBaseUrl}\\/site\\/\"}', 5, 120, '2018-04-06 12:34:30', '2018-04-06 12:34:30', 'b883808f-98f8-464d-bdf6-9d70ac47bf2e'),
  (6, 'Trainings', 'trainings', 'Local', '{\"path\":\"{craftAssetsBasePath}\\/site\\/\",\"publicURLs\":\"1\",\"url\":\"{craftAssetsBaseUrl}\\/site\\/\"}', 6, 130, '2018-04-09 13:53:36', '2018-04-09 13:53:36', '6f98ff87-1707-4f28-a4e7-2c094aeff4ec'),
  (7, 'Pricing Page', 'pricingPage', 'Local', '{\"path\":\"{craftAssetsBasePath}\\/site\\/\",\"publicURLs\":\"1\",\"url\":\"{craftAssetsBaseUrl}\\/site\\/\"}', 7, 153, '2018-04-16 12:06:39', '2018-04-16 12:06:39', '7eb7cd2c-d4d9-4c10-b095-c3b48181f5ef');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_assettransformindex`
--

CREATE TABLE `craft_assettransformindex` (
  `id` int(11) NOT NULL,
  `fileId` int(11) NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sourceId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT NULL,
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_assettransforms`
--

CREATE TABLE `craft_assettransforms` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mode` enum('stretch','fit','crop') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'center-center',
  `height` int(10) DEFAULT NULL,
  `width` int(10) DEFAULT NULL,
  `format` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quality` int(10) DEFAULT NULL,
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_categories`
--

CREATE TABLE `craft_categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_categorygroups`
--

CREATE TABLE `craft_categorygroups` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasUrls` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_categorygroups_i18n`
--

CREATE TABLE `craft_categorygroups_i18n` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_content`
--

CREATE TABLE `craft_content` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_body` text COLLATE utf8_unicode_ci,
  `field_phoneNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_foundationYear` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_addressLine1` text COLLATE utf8_unicode_ci,
  `field_addressLine2` text COLLATE utf8_unicode_ci,
  `field_addressLine3` text COLLATE utf8_unicode_ci,
  `field_companyTitle` text COLLATE utf8_unicode_ci,
  `field_logocolored` text COLLATE utf8_unicode_ci,
  `field_logogreyed` text COLLATE utf8_unicode_ci,
  `field_showInMenu` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `field_sortingOrder` tinyint(3) UNSIGNED DEFAULT '0',
  `field_showInFooter` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `field_footerSortingOrder` tinyint(3) UNSIGNED DEFAULT '0',
  `field_socialLink` text COLLATE utf8_unicode_ci,
  `field_bottomHtmlMarkup` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_content`
--

INSERT INTO `craft_content` (`id`, `elementId`, `locale`, `title`, `field_body`, `field_phoneNumber`, `field_foundationYear`, `field_addressLine1`, `field_addressLine2`, `field_addressLine3`, `field_companyTitle`, `field_logocolored`, `field_logogreyed`, `field_showInMenu`, `field_sortingOrder`, `field_showInFooter`, `field_footerSortingOrder`, `field_socialLink`, `field_bottomHtmlMarkup`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 1, 'en', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-03-28 09:59:37', '2018-03-28 14:48:51', 'f2591d2b-3295-452f-baf7-7b08738d7f71'),
  (2, 2, 'en', 'Welcome to Localhost!', '<p>It’s true, this site doesn’t have a whole lot of content yet, but don’t worry. Our web developers have just installed the CMS, and they’re setting things up for the content editors this very moment. Soon Localhost will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-03-28 09:59:41', '2018-04-02 08:22:04', '73fb41aa-b1f5-49fe-bc06-01fcd7890729'),
  (3, 3, 'en', 'We just installed Craft!', '<p>Craft is the CMS that’s powering Localhost. It’s beautiful, powerful, flexible, and easy-to-use, and it’s made by Pixel &amp; Tonic. We can’t wait to dive in and see what it’s capable of!</p><!--pagebreak--><p>This is even more captivating content, which you couldn’t see on the News index page because it was entered after a Page Break, and the News index template only likes to show the content on the first page.</p><p>Craft: a nice alternative to Word, if you’re making a website.</p>', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'a5479f8d-dc1b-448e-a7ac-9c08e8e4c9ed'),
  (4, 4, 'en', NULL, NULL, '0131  333 1222', '2017', 'Hallyards Farm House', 'Kirkliston', 'EH29 9DZ', 'Bark n’ Fly', '', '', 0, 0, 0, 0, NULL, NULL, '2018-04-02 08:37:52', '2018-04-02 09:34:36', 'bb6d9360-a1ab-4e7d-a83a-a220d215d61f'),
  (7, 7, 'en', 'Logo Inverse', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-02 11:34:19', '2018-04-02 11:34:19', 'c7bdf9ef-98f6-4d78-8ed4-ae8dcc63168c'),
  (8, 8, 'en', 'Logo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-02 11:34:19', '2018-04-02 11:34:19', 'd7ac739a-ed5d-4a72-bf1a-2bddd21cf0c5'),
  (9, 9, 'en', 'Home', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, '2018-04-02 15:22:42', '2018-04-05 11:41:20', '46bdc63d-556d-4519-b711-24b5b386c3eb'),
  (10, 11, 'en', 'Services', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, NULL, NULL, '2018-04-02 15:32:17', '2018-04-06 09:42:52', 'd6859674-a806-4fa5-8df7-3ec6649db423'),
  (11, 12, 'en', 'Pricing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2, 0, 0, NULL, NULL, '2018-04-02 15:33:05', '2018-04-16 14:10:02', '35cf2f11-4576-49d0-9507-b5b17d57eb44'),
  (12, 13, 'en', 'Facilities', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 3, 1, 2, NULL, NULL, '2018-04-02 15:33:24', '2018-04-06 12:37:17', '84c4c20b-5a2f-4a0c-9abe-d775f5846130'),
  (13, 14, 'en', 'Training', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 4, 1, 5, NULL, NULL, '2018-04-02 15:34:08', '2018-04-10 07:58:38', 'a156c025-79c1-4d38-aa15-83d10920f173'),
  (14, 15, 'en', 'About Us', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 5, 1, 3, NULL, NULL, '2018-04-02 15:34:22', '2018-04-10 14:59:28', 'f11572a6-123d-45b3-93ef-12d3f33f4de6'),
  (15, 16, 'en', 'Pupparazzi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 6, 1, 4, NULL, NULL, '2018-04-02 15:34:39', '2018-04-30 17:51:42', '06194597-e823-4990-9747-40d8072c0de9'),
  (16, 17, 'en', 'Contact Us', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 7, 1, 6, NULL, NULL, '2018-04-02 15:34:57', '2018-04-04 08:38:45', '0b613315-cae3-446e-8fed-071416fc0b94'),
  (17, 18, 'en', 'Privacy Policy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 7, NULL, NULL, '2018-04-03 12:46:49', '2018-04-04 08:38:45', '40524f66-2c3e-4e62-b5ca-46666ca14e45'),
  (18, 19, 'en', 'Terms & Conditions', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 8, NULL, NULL, '2018-04-03 12:47:28', '2018-04-04 08:38:45', 'e1cffd2f-24b3-44d9-b498-ebecce80e901'),
  (19, 20, 'en', 'Cookies', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 9, NULL, NULL, '2018-04-03 12:47:50', '2018-04-04 08:38:46', '85175129-dfc1-4564-892f-4f511c09baf0'),
  (20, 21, 'en', 'VAT registration: 264563488', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, 10, NULL, NULL, '2018-04-03 12:48:26', '2018-04-04 08:38:46', '58d5488c-afcc-4d40-88be-95c7da828638'),
  (21, 22, 'en', 'Facebook', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-03 15:07:10', '2018-04-03 15:07:10', '225803f9-6345-4292-b8fc-7d9007bb4bb0'),
  (22, 23, 'en', 'Instagram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-03 15:07:10', '2018-04-03 15:07:10', '4d9b6806-925c-465b-ad55-0eff2be16862'),
  (23, 24, 'en', 'Twitter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-03 15:07:11', '2018-04-03 15:07:11', '5c138031-648f-4a2e-b31e-1ce5d0591ae1'),
  (24, 25, 'en', 'Youtube', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-03 15:07:11', '2018-04-03 15:07:11', '1a91fe72-bc52-473d-852e-90c48eaa35f3'),
  (25, 26, 'en', 'Twitter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 'https://twitter.com/', NULL, '2018-04-03 15:08:30', '2018-04-03 15:08:30', '2e541c0d-d99f-4ab2-b01c-dcde492dfb41'),
  (26, 27, 'en', 'Facebook', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 'https://facebook.com/', NULL, '2018-04-03 15:08:59', '2018-04-03 15:08:59', 'a40c55a5-41da-4a29-98c2-35a3eaede3dd'),
  (27, 28, 'en', 'Youtube', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 'https://youtube.com/', NULL, '2018-04-03 15:09:19', '2018-04-03 15:09:19', '8653423a-284b-4a13-af04-652e442cbb94'),
  (28, 29, 'en', 'Instagram', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 'https://instagram.com/', NULL, '2018-04-03 15:09:33', '2018-04-03 15:09:33', 'fbe341e7-0258-446c-90f9-3d3425d98703'),
  (29, 30, 'en', 'Art Cover Welcome 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-03 16:29:27', '2018-04-03 16:29:27', 'e4c3c6e7-2782-4dd9-80c5-f43b7f4f9c89'),
  (30, 31, 'en', 'Art Cover Welcome', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-03 16:29:28', '2018-04-03 16:29:28', 'e08a3375-ac98-4c55-a472-aaff60bb551e'),
  (31, 34, 'en', 'Art Testimonial 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-05 10:15:06', '2018-04-05 10:15:06', '8fd07b01-680b-4223-bd1a-618d7b2abe9b'),
  (32, 38, 'en', 'Natural Instinct', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-05 10:32:44', '2018-04-05 10:33:40', 'd4fdfb8b-bc6c-46fe-b9a0-3294ff8e5775'),
  (33, 40, 'en', 'Card 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-05 11:32:26', '2018-04-05 11:32:26', '6128de9f-89ed-498d-9623-5d70f8599a26'),
  (34, 41, 'en', 'Card 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-05 11:32:27', '2018-04-05 11:32:27', '84289c5a-ed8a-4663-96c3-d74a1e3c4c53'),
  (35, 42, 'en', 'Card 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-05 11:32:28', '2018-04-05 11:32:28', 'f36a1ba8-eab2-4af6-b045-0263ec5986ca'),
  (36, 47, 'en', 'Service 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 07:41:01', '2018-04-06 07:41:01', '723436b0-1886-4105-9906-27273d553d47'),
  (37, 48, 'en', 'Service 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 07:41:24', '2018-04-06 07:41:24', '72056c6f-2666-4642-850d-bd10719d12de'),
  (38, 49, 'en', 'Service 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 07:41:46', '2018-04-06 07:41:46', '529686c4-b4f6-413e-967a-55cd8f8a0d66'),
  (39, 50, 'en', 'Service 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 07:42:09', '2018-04-06 07:42:09', 'c641e947-28ec-48e6-a474-5a0f3804fdf0'),
  (40, 51, 'en', 'Art Cover Service', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 07:50:02', '2018-04-06 07:50:02', '0b469413-c485-44ef-afc1-bbbe99d32fbb'),
  (41, 59, 'en', 'Art Cover About', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:21:32', '2018-04-06 12:21:32', '1b99a720-2de8-4e05-ba45-b8c0e1130c3d'),
  (42, 60, 'en', 'Art Cover Facilities', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:21:33', '2018-04-06 12:21:33', '70e4e878-d21c-4726-af60-046097857e05'),
  (43, 61, 'en', 'Art Cover Pricing', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:21:33', '2018-04-06 12:21:33', '11955a89-8d80-45f0-bf01-7ba94847a00f'),
  (45, 63, 'en', 'Art Cover Training', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:22:29', '2018-04-06 12:22:29', 'ed3a9748-8660-439c-845e-bbe63bb142b7'),
  (48, 66, 'en', 'Art Cover Service', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:25:07', '2018-04-06 12:25:08', '44f576f1-ea82-4ab9-82f2-ccfcbd4fb2f2'),
  (49, 67, 'en', 'Art Cover Welcome 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:25:08', '2018-04-06 12:25:08', '6de4e454-4ba1-48ed-8763-6a211845e2c6'),
  (50, 68, 'en', 'Art Cover Welcome', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:25:08', '2018-04-06 12:25:08', '9873b5f0-2471-4e67-9a0d-1a58e04a564f'),
  (51, 69, 'en', 'Facility 1 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:06', '2018-04-06 12:35:06', 'b8c03e91-c632-4053-8d95-7dd210aeb97f'),
  (52, 70, 'en', 'Facility 1 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:07', '2018-04-06 12:35:07', 'a53cad04-7ff9-4cff-8e7d-40803413bccb'),
  (53, 71, 'en', 'Facility 1 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:08', '2018-04-06 12:35:08', 'ce73197e-5822-4794-9fb1-54ac87b63968'),
  (54, 72, 'en', 'Facility 2 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:17', '2018-04-06 12:35:17', 'b122d9cf-8182-4adc-be56-f3f60cacca64'),
  (55, 73, 'en', 'Facility 2 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:17', '2018-04-06 12:35:17', 'd0eeb325-ea0b-4a84-9055-8d93f5711877'),
  (56, 74, 'en', 'Facility 2 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:18', '2018-04-06 12:35:18', '8411a377-7642-4486-8d48-f5c33f9a08ff'),
  (57, 75, 'en', 'Facility 3 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:33', '2018-04-06 12:35:33', '4f5da060-818c-46d8-8793-dc7c145bbc99'),
  (58, 76, 'en', 'Facility 3 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:34', '2018-04-06 12:35:34', '0449422f-23d4-4196-9b3d-02e69b448af4'),
  (59, 77, 'en', 'Facility 3 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:35', '2018-04-06 12:35:35', '9e505462-b1e9-4e4e-90e0-e2478bfb88bc'),
  (60, 78, 'en', 'Facility 4 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:42', '2018-04-06 12:35:42', 'ffb9dd81-a6ec-4744-a85c-f7d92e1ff2eb'),
  (61, 79, 'en', 'Facility 4 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:43', '2018-04-06 12:35:43', '41643681-87a1-4ba2-ae7c-99a74b9c47f5'),
  (62, 80, 'en', 'Facility 4 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:44', '2018-04-06 12:35:44', 'e1520099-886d-4b6e-846e-e0137feebc27'),
  (63, 81, 'en', 'Facility 5 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:58', '2018-04-06 12:35:58', 'a16f9e78-1e51-45f7-8794-fefe7835224e'),
  (64, 82, 'en', 'Facility 5 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:58', '2018-04-06 12:35:58', '02bac269-17b1-49ad-a09b-d29b616fae78'),
  (65, 83, 'en', 'Facility 5 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:35:59', '2018-04-06 12:35:59', '9658c37a-f9ca-484b-98c0-ff0a990dbf15'),
  (66, 84, 'en', 'Facility 6 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:36:14', '2018-04-06 12:36:14', '3a0aa6d1-1cbc-4576-91f7-fc95fddf814f'),
  (67, 85, 'en', 'Facility 6 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:36:15', '2018-04-06 12:36:15', 'e80e234d-4495-4dda-a41c-7de150eddd47'),
  (68, 86, 'en', 'Facility 6 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-06 12:36:16', '2018-04-06 12:36:16', '8c504a0a-b4c0-47b6-b9bb-2517e6c7fdd1'),
  (69, 95, 'en', 'Training Video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-09 14:10:27', '2018-04-09 14:10:27', '7574bc94-92fd-489b-b506-0b1bde0b6bf5'),
  (70, 96, 'en', 'Training 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-10 07:57:19', '2018-04-10 07:57:19', '2b6813d3-19e6-46db-8247-962c3c4bcabd'),
  (71, 97, 'en', 'Training 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-10 07:57:20', '2018-04-10 07:57:20', 'd34013fb-9fd4-4bfe-ac96-a671a64cc881'),
  (72, 98, 'en', 'Training 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-10 07:57:23', '2018-04-10 07:57:23', 'e69d89c1-08d4-4df9-af6c-58d198f6eda9'),
  (73, 99, 'en', 'Training 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-10 07:57:25', '2018-04-10 07:57:25', '131778e9-916c-4c42-8653-a3cdf6252bae'),
  (74, 105, 'en', 'Feedback List', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '7324d75a-f86b-4344-9625-07753768ed7a'),
  (75, 110, 'en', 'About Video', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-10 14:58:57', '2018-04-10 14:58:57', '6e3eca1f-e572-4b9d-aac7-734dd76b2300'),
  (76, 121, 'en', 'Flat Grid View 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-16 12:07:03', '2018-04-16 12:07:03', '36ffde32-ec8c-4319-854f-1094f1707a3f'),
  (77, 122, 'en', 'Flat Grid View 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-16 12:07:04', '2018-04-16 12:07:04', '84fb3752-a003-4a11-8ccc-a960ff9fdb6e'),
  (78, 123, 'en', 'Flat Grid View 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-16 12:07:04', '2018-04-16 12:07:04', '7c7e2dcb-4c3d-4012-bb94-429ec2da37d0'),
  (79, 128, 'en', 'Day Care', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, '<div class=\"pricing__lead lead\">Rates include VAT</div>\n<p>Note that day care and boarding pick up / drop off service is restricted to certain areas. Please contact us for\n                                        details.</p>\n<p><strong>Please note:</strong> Dogmore does not charge for days or periods when your dog is not using our services, including last\n                                        minute cancellations.</p>', '2018-04-16 14:02:31', '2018-04-16 16:08:23', '566c992a-fead-4517-a824-a703b2805592'),
  (80, 135, 'en', 'Boarding Kennels', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, '<p><b>Please note</b>: Regardless of the time your dog is checked in, the charge of £26 would apply from the check in time\n                            until midday the next day. Collection after midday would incur an additional charge of half a day boarding (£18.50 for one\n                            dog).</p>', '2018-04-16 14:06:18', '2018-04-16 15:26:48', '389c75da-2cd8-4deb-be2c-b38089fada97'),
  (81, 140, 'en', 'Dog Training, Behaviour Correction and Puppy Education', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, '<p><strong>Please note</strong>: We like to differentiate between performance and practical obedience. Performance obedience is suited for\n                        owners who like the idea of working with their dog and would like to engage in dog training as an activity, either\n                        competitively or not.</p>', '2018-04-16 14:07:57', '2018-04-16 16:19:43', '391821df-ddd5-460f-923b-3f15f30c0ec6'),
  (82, 161, 'en', 'Pupparazzi Cover', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 0, NULL, NULL, '2018-04-30 17:51:30', '2018-04-30 17:51:30', 'd1da7e91-1947-49c6-9a4b-36846e4360e5');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_deprecationerrors`
--

CREATE TABLE `craft_deprecationerrors` (
  `id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fingerprint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `line` smallint(6) UNSIGNED NOT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `templateLine` smallint(6) UNSIGNED DEFAULT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traces` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_elementindexsettings`
--

CREATE TABLE `craft_elementindexsettings` (
  `id` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_elementindexsettings`
--

INSERT INTO `craft_elementindexsettings` (`id`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 'Entry', '{\"sources\":{\"section:3\":{\"tableAttributes\":{\"1\":\"link\",\"2\":\"slug\",\"3\":\"field:11\",\"4\":\"field:15\",\"5\":\"field:16\",\"6\":\"field:17\",\"7\":\"type\"}},\"section:2\":{\"tableAttributes\":{\"1\":\"postDate\",\"2\":\"expiryDate\",\"3\":\"link\"}},\"singles\":{\"tableAttributes\":{\"1\":\"link\",\"2\":\"section\"}},\"*\":{\"tableAttributes\":{\"1\":\"section\",\"2\":\"postDate\",\"3\":\"expiryDate\",\"4\":\"link\"}},\"section:5\":{\"tableAttributes\":{\"1\":\"postDate\",\"2\":\"expiryDate\",\"3\":\"link\",\"4\":\"section\"}}}}', '2018-04-02 14:05:13', '2018-04-10 12:38:11', 'a9e1ec3a-6c51-4f56-bd38-e96691dd00a4'),
  (8, 'Asset', '{\"sourceOrder\":[[\"key\",\"folder:1\"],[\"heading\",\"asdf\"]],\"sources\":{\"folder:1\":{\"tableAttributes\":{\"1\":\"filename\",\"2\":\"size\",\"3\":\"dateModified\"}}}}', '2018-04-03 16:24:05', '2018-04-03 16:24:44', 'facc1d5c-d48c-4011-aa0b-2e63cf6d48bd');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_elements`
--

CREATE TABLE `craft_elements` (
  `id` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `archived` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_elements`
--

INSERT INTO `craft_elements` (`id`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 'User', 1, 0, '2018-03-28 09:59:37', '2018-03-28 14:48:51', '8df939f1-b5f6-4f1a-84d0-503a74055fa1'),
  (2, 'Entry', 1, 0, '2018-03-28 09:59:41', '2018-04-02 08:22:04', 'a5637297-e424-49ac-8709-e8c6ef4db405'),
  (3, 'Entry', 1, 0, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '65ed17b1-ad84-48b5-9ec9-9facbcf7b1a6'),
  (4, 'GlobalSet', 1, 0, '2018-04-02 08:37:52', '2018-04-02 09:34:36', 'a328626e-4f92-4ae4-b63e-21ffdd88c76e'),
  (7, 'Asset', 1, 0, '2018-04-02 11:34:19', '2018-04-02 11:34:19', 'f3b477c7-3304-4c42-8d00-4bbe26c9e8a1'),
  (8, 'Asset', 1, 0, '2018-04-02 11:34:19', '2018-04-02 11:34:19', '057f5065-4e61-462d-a8b1-285b54f1505a'),
  (9, 'Entry', 1, 0, '2018-04-02 15:22:42', '2018-04-05 11:41:20', '02c42c26-793f-40a0-9d53-c0e2e754b704'),
  (10, 'MatrixBlock', 1, 0, '2018-04-02 15:22:42', '2018-04-05 11:41:21', '9d008297-7305-4187-b065-e98be0239899'),
  (11, 'Entry', 1, 0, '2018-04-02 15:32:17', '2018-04-06 09:42:52', 'a18e01e6-270a-4259-aea7-7fa5553f58c8'),
  (12, 'Entry', 1, 0, '2018-04-02 15:33:05', '2018-04-16 14:10:02', 'fb1e5793-9493-4d97-933f-81766da355f9'),
  (13, 'Entry', 1, 0, '2018-04-02 15:33:24', '2018-04-06 12:37:17', '8e5856fe-32fd-43e4-9524-0b34dce8296c'),
  (14, 'Entry', 1, 0, '2018-04-02 15:34:08', '2018-04-10 07:58:38', 'b7ea500c-5d04-4781-ae33-6ffd17a0f6fe'),
  (15, 'Entry', 1, 0, '2018-04-02 15:34:22', '2018-04-10 14:59:28', '6f3a3816-2fad-4997-b8b9-43d21267a33b'),
  (16, 'Entry', 1, 0, '2018-04-02 15:34:39', '2018-04-30 17:51:42', 'b429bcb1-76f3-4ba1-a84e-b7f13f5afe94'),
  (17, 'Entry', 1, 0, '2018-04-02 15:34:57', '2018-04-04 08:38:45', 'f2acb3ca-2ee3-4950-93e5-7464fb20e44b'),
  (18, 'Entry', 1, 0, '2018-04-03 12:46:49', '2018-04-04 08:38:45', '5583398a-b086-4915-95e5-9afc6111b4ab'),
  (19, 'Entry', 1, 0, '2018-04-03 12:47:28', '2018-04-04 08:38:45', '9559d20b-91e9-4df4-b4a5-8766d196dbfb'),
  (20, 'Entry', 1, 0, '2018-04-03 12:47:50', '2018-04-04 08:38:46', '8545b079-5f20-4e0f-8bf0-9e1333d22fa8'),
  (21, 'Entry', 1, 0, '2018-04-03 12:48:26', '2018-04-04 08:38:46', '166febe8-de41-4630-8936-e612318b8eb4'),
  (22, 'Asset', 1, 0, '2018-04-03 15:07:10', '2018-04-03 15:07:10', '269b50ab-fc84-4477-b557-45c15b1e6a3d'),
  (23, 'Asset', 1, 0, '2018-04-03 15:07:10', '2018-04-03 15:07:10', '2ba3caf2-69f7-465a-8971-4ff09802f192'),
  (24, 'Asset', 1, 0, '2018-04-03 15:07:11', '2018-04-03 15:07:11', 'de605099-15d4-4f6e-ac67-290e9732ad10'),
  (25, 'Asset', 1, 0, '2018-04-03 15:07:11', '2018-04-03 15:07:11', 'bc6a81ba-53d7-4aa5-b1fa-d3b55a7d6a54'),
  (26, 'Entry', 1, 0, '2018-04-03 15:08:30', '2018-04-03 15:08:30', '3a4553e5-0178-4e97-8f5b-bb78f3e2311a'),
  (27, 'Entry', 1, 0, '2018-04-03 15:08:59', '2018-04-03 15:08:59', '117005e4-d394-4693-b04e-3d3f7179bbf7'),
  (28, 'Entry', 1, 0, '2018-04-03 15:09:19', '2018-04-03 15:09:19', 'ff879fe4-624f-47d7-80ef-95422db00104'),
  (29, 'Entry', 1, 0, '2018-04-03 15:09:33', '2018-04-03 15:09:33', '53f512fe-d198-4c2d-a609-984c33ca633c'),
  (30, 'Asset', 1, 0, '2018-04-03 16:29:27', '2018-04-03 16:29:27', 'f3b6f2e8-858f-4155-b240-2ab1c64c419a'),
  (31, 'Asset', 1, 0, '2018-04-03 16:29:28', '2018-04-03 16:29:28', '45e4bad1-b747-4843-88be-bdfeec46c60d'),
  (32, 'MatrixBlock', 1, 0, '2018-04-03 16:32:55', '2018-04-05 11:41:21', 'fd385cbb-ee20-4d2d-8d14-626a40e112b0'),
  (33, 'MatrixBlock', 1, 0, '2018-04-04 11:01:27', '2018-04-05 11:41:21', '10090924-f246-4ae8-8477-6fcb0eb510c2'),
  (34, 'Asset', 1, 0, '2018-04-05 10:15:06', '2018-04-05 10:15:06', '3c47d5b5-f107-470e-aefb-efcfd090e5e7'),
  (35, 'MatrixBlock', 1, 0, '2018-04-05 10:19:28', '2018-04-05 11:41:21', '8dd275b2-af8d-42dc-91c3-295eb60abc69'),
  (36, 'MatrixBlock', 1, 0, '2018-04-05 10:19:28', '2018-04-05 11:41:21', '40b3c31c-060c-47de-856e-90d2d296717c'),
  (37, 'MatrixBlock', 1, 0, '2018-04-05 10:19:28', '2018-04-05 11:41:21', 'e633453b-c98f-487b-8244-d0c481121b0a'),
  (38, 'Asset', 1, 0, '2018-04-05 10:32:44', '2018-04-05 10:33:40', '6a961ad1-1cc5-4e79-a837-90cf156133bc'),
  (39, 'MatrixBlock', 1, 0, '2018-04-05 10:37:37', '2018-04-05 11:41:21', '6d127504-fc72-4b63-b4e7-889beb724eb5'),
  (40, 'Asset', 1, 0, '2018-04-05 11:32:26', '2018-04-05 11:32:26', 'fd1c5cb6-845f-4513-b75b-403343213ee9'),
  (41, 'Asset', 1, 0, '2018-04-05 11:32:27', '2018-04-05 11:32:27', '69741cfc-5fda-44b4-a064-023eef3431c3'),
  (42, 'Asset', 1, 0, '2018-04-05 11:32:28', '2018-04-05 11:32:28', '7ee9d492-ac67-45dc-9c74-83b8d4c3ca1f'),
  (43, 'MatrixBlock', 1, 0, '2018-04-05 11:34:09', '2018-04-05 11:41:21', '60d9c928-5825-4948-bea2-ddcaddead487'),
  (44, 'MatrixBlock', 1, 0, '2018-04-05 11:35:39', '2018-04-05 11:41:21', 'd7fedc78-a8c1-4f34-b696-7ce2c997bf5e'),
  (45, 'MatrixBlock', 1, 0, '2018-04-05 11:35:39', '2018-04-05 11:41:21', 'c182ebce-1485-4cea-8fad-a39781d5521d'),
  (46, 'MatrixBlock', 1, 0, '2018-04-05 11:35:39', '2018-04-05 11:41:21', 'c42f8688-9f89-4dc0-9a38-390363cfc3d9'),
  (47, 'Asset', 1, 0, '2018-04-06 07:41:01', '2018-04-06 07:41:01', '5b1f8402-eaec-442c-acda-d5a01c6e0931'),
  (48, 'Asset', 1, 0, '2018-04-06 07:41:24', '2018-04-06 07:41:24', '226a097f-191d-4f94-8702-8588a8e04dbf'),
  (49, 'Asset', 1, 0, '2018-04-06 07:41:46', '2018-04-06 07:41:46', '5ff97bca-0aff-44b1-9f7d-fdc3566dae17'),
  (50, 'Asset', 1, 0, '2018-04-06 07:42:09', '2018-04-06 07:42:09', 'c3e3c312-8e33-4482-80a6-37cfd6a25799'),
  (51, 'Asset', 1, 0, '2018-04-06 07:50:02', '2018-04-06 07:50:02', 'e1bdc17f-07f7-47af-8b16-c37264d4ab9c'),
  (52, 'MatrixBlock', 1, 0, '2018-04-06 08:10:45', '2018-04-06 09:42:52', '129c132a-84a4-4c29-b71d-07b428df70aa'),
  (53, 'MatrixBlock', 1, 0, '2018-04-06 08:26:49', '2018-04-06 09:42:52', '468b2ecf-76c5-47c0-802c-95f50f979e23'),
  (54, 'MatrixBlock', 1, 0, '2018-04-06 08:55:23', '2018-04-06 09:42:52', 'b9d41b2b-2578-43be-beb6-395bc262423f'),
  (55, 'MatrixBlock', 1, 0, '2018-04-06 08:55:23', '2018-04-06 09:42:52', '1224049a-146c-4a54-a01d-e1b0387ca767'),
  (56, 'MatrixBlock', 1, 0, '2018-04-06 08:55:23', '2018-04-06 09:42:52', 'e514910d-fc35-4938-ba88-9e6384c67c2e'),
  (57, 'MatrixBlock', 1, 0, '2018-04-06 08:55:23', '2018-04-06 09:42:52', '80ad890a-3e90-44f1-8529-f14d3ee76238'),
  (58, 'MatrixBlock', 1, 0, '2018-04-06 09:42:52', '2018-04-06 09:42:52', '7b619007-e746-40d5-adef-9b23f4ac9591'),
  (59, 'Asset', 1, 0, '2018-04-06 12:21:32', '2018-04-06 12:21:32', '6ea3eab3-9254-4bd4-a969-5f3e12811b96'),
  (60, 'Asset', 1, 0, '2018-04-06 12:21:33', '2018-04-06 12:21:33', '47cd6ccd-b0ed-46bf-86bf-7c2e1ae586cd'),
  (61, 'Asset', 1, 0, '2018-04-06 12:21:33', '2018-04-06 12:21:33', '57b211b9-fb56-497d-ac2f-18564a097d9d'),
  (63, 'Asset', 1, 0, '2018-04-06 12:22:29', '2018-04-06 12:22:29', 'dd85c533-e401-4805-860f-a56c2707e8f4'),
  (66, 'Asset', 1, 0, '2018-04-06 12:25:07', '2018-04-06 12:25:08', '5a5b082e-0ac6-46b6-81b6-5c03c7df89c1'),
  (67, 'Asset', 1, 0, '2018-04-06 12:25:08', '2018-04-06 12:25:08', 'f8e71b53-0e4f-4d69-9322-7875bd52db65'),
  (68, 'Asset', 1, 0, '2018-04-06 12:25:08', '2018-04-06 12:25:08', '795f7062-d9ab-4f41-b00c-7794ca685771'),
  (69, 'Asset', 1, 0, '2018-04-06 12:35:06', '2018-04-06 12:35:06', '4e7fdc1e-24f0-4dc4-8e06-600d69109342'),
  (70, 'Asset', 1, 0, '2018-04-06 12:35:07', '2018-04-06 12:35:07', '8b0edd1d-29a6-4739-a4e2-531606f60fbe'),
  (71, 'Asset', 1, 0, '2018-04-06 12:35:08', '2018-04-06 12:35:08', '4fce7875-bf2b-47e5-b205-87b5e3ea176b'),
  (72, 'Asset', 1, 0, '2018-04-06 12:35:17', '2018-04-06 12:35:17', '7b3bf300-1fc4-488d-8bfa-877c6aa027be'),
  (73, 'Asset', 1, 0, '2018-04-06 12:35:17', '2018-04-06 12:35:17', 'ae8cfebe-784a-4162-b901-e58d977f4584'),
  (74, 'Asset', 1, 0, '2018-04-06 12:35:18', '2018-04-06 12:35:18', '99dbc848-e435-4248-a645-d9e0788e5ab8'),
  (75, 'Asset', 1, 0, '2018-04-06 12:35:33', '2018-04-06 12:35:33', '5b42c44a-1c01-4d08-b3b8-b103882b3d3e'),
  (76, 'Asset', 1, 0, '2018-04-06 12:35:34', '2018-04-06 12:35:34', '33bd59ab-f1eb-48c5-9339-36f671244899'),
  (77, 'Asset', 1, 0, '2018-04-06 12:35:35', '2018-04-06 12:35:35', '5164e5e1-0841-4f10-b863-921e6ea7020a'),
  (78, 'Asset', 1, 0, '2018-04-06 12:35:42', '2018-04-06 12:35:42', '58e994e9-6438-4984-b025-45bcd04229c0'),
  (79, 'Asset', 1, 0, '2018-04-06 12:35:43', '2018-04-06 12:35:43', '709528f4-19bd-4cc7-9310-6d8aad10c35d'),
  (80, 'Asset', 1, 0, '2018-04-06 12:35:44', '2018-04-06 12:35:44', 'c54f0718-c53f-43c6-8422-909e490f3ecf'),
  (81, 'Asset', 1, 0, '2018-04-06 12:35:58', '2018-04-06 12:35:58', 'e3ac732b-ac63-48b5-a48f-96e57636ee6b'),
  (82, 'Asset', 1, 0, '2018-04-06 12:35:58', '2018-04-06 12:35:58', 'd5344f8f-8b56-4aad-81ca-0fefdd4205f3'),
  (83, 'Asset', 1, 0, '2018-04-06 12:35:59', '2018-04-06 12:35:59', '75a64118-2afe-4ccc-99e3-ba27e9b3d353'),
  (84, 'Asset', 1, 0, '2018-04-06 12:36:14', '2018-04-06 12:36:14', '1fe60327-8f4c-4e66-be11-a024c9bf05b9'),
  (85, 'Asset', 1, 0, '2018-04-06 12:36:15', '2018-04-06 12:36:15', '4dfc556c-2951-4535-a6bf-c6ad584ce699'),
  (86, 'Asset', 1, 0, '2018-04-06 12:36:16', '2018-04-06 12:36:16', '08729336-4821-41ff-9145-6f95688f9b91'),
  (87, 'MatrixBlock', 1, 0, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'fdbbba72-b0de-4d29-b893-b7c622b0c29b'),
  (88, 'MatrixBlock', 1, 0, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'f8a7d42b-3201-489e-a425-dbebf757bb23'),
  (89, 'MatrixBlock', 1, 0, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '10c0b2ef-3fe2-4df3-bcf6-bcfa0e096411'),
  (90, 'MatrixBlock', 1, 0, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'cd0aa254-60ca-4829-b0cd-d37fb92bef9f'),
  (91, 'MatrixBlock', 1, 0, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '36deafb7-c6be-414a-a728-5dc35ccf6cb1'),
  (92, 'MatrixBlock', 1, 0, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '27f183a6-27e2-4ea0-86b3-9dbe0d9abb77'),
  (93, 'MatrixBlock', 1, 0, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '41085ca3-eac6-4512-be92-3f620345dbdf'),
  (94, 'MatrixBlock', 1, 0, '2018-04-09 11:04:30', '2018-04-10 07:58:38', '724c5e20-8fb2-4e2d-ac0c-c103ae533c85'),
  (95, 'Asset', 1, 0, '2018-04-09 14:10:27', '2018-04-09 14:10:27', '4d663b6c-60c1-4c09-aeb6-109a147f4516'),
  (96, 'Asset', 1, 0, '2018-04-10 07:57:19', '2018-04-10 07:57:19', 'a2da51be-035e-47bc-b33f-0533d4e617f7'),
  (97, 'Asset', 1, 0, '2018-04-10 07:57:20', '2018-04-10 07:57:20', 'e67695f4-af16-4b51-b1c8-519c7ee9f894'),
  (98, 'Asset', 1, 0, '2018-04-10 07:57:23', '2018-04-10 07:57:23', 'd5c462b3-2308-490d-92d9-eb8c054cac5f'),
  (99, 'Asset', 1, 0, '2018-04-10 07:57:25', '2018-04-10 07:57:25', '1e923756-d8dc-45fc-a653-957b17676afb'),
  (100, 'MatrixBlock', 1, 0, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'b6f79eb0-e0fc-4e5f-a5fd-83caa537fb0d'),
  (101, 'MatrixBlock', 1, 0, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '7b47b77e-d55f-4e03-84f7-ec5faa4978c3'),
  (102, 'MatrixBlock', 1, 0, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'e9eb8e4d-cfd0-4f6a-8ab4-ad4bca563a62'),
  (103, 'MatrixBlock', 1, 0, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '9541ca2e-8e43-40de-8414-238723412a06'),
  (104, 'MatrixBlock', 1, 0, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '16a73afc-e101-44e1-b35f-b24129115116'),
  (105, 'Entry', 1, 0, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '9fbee55c-992e-41df-adee-86c0680b228b'),
  (106, 'MatrixBlock', 1, 0, '2018-04-10 12:31:27', '2018-04-11 16:53:59', 'fa9a4729-9194-4083-847f-e636a8c538fc'),
  (107, 'MatrixBlock', 1, 0, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '79172cb2-6f51-4ac0-8383-bec5930e9942'),
  (108, 'MatrixBlock', 1, 0, '2018-04-10 12:31:27', '2018-04-11 16:53:59', 'd1e04e8e-6873-48bf-90dc-d6a764a3eca5'),
  (109, 'MatrixBlock', 1, 0, '2018-04-10 14:48:46', '2018-04-10 14:59:28', 'df90b659-045d-44cd-a23f-ad85b718e194'),
  (110, 'Asset', 1, 0, '2018-04-10 14:58:57', '2018-04-10 14:58:57', '857c0fe2-ea65-44a3-9c02-3bdca1a154c9'),
  (111, 'MatrixBlock', 1, 0, '2018-04-10 14:59:28', '2018-04-10 14:59:28', '031afacf-3ed5-4c5f-8cbb-dcf122aa1847'),
  (112, 'MatrixBlock', 1, 0, '2018-04-10 14:59:28', '2018-04-10 14:59:28', '1f7edef2-4069-40a8-a374-8a8ebbf4cbb1'),
  (113, 'MatrixBlock', 1, 0, '2018-04-10 14:59:28', '2018-04-10 14:59:28', 'c79b00dc-8717-45ba-8ab6-7e771245815f'),
  (114, 'MatrixBlock', 1, 0, '2018-04-16 09:14:17', '2018-04-16 14:10:02', '93a64ca2-370c-4e19-945b-35c0fe50f3f4'),
  (121, 'Asset', 1, 0, '2018-04-16 12:07:03', '2018-04-16 12:07:03', '2b13ff3e-9a0c-44a7-9cfc-6452028bc684'),
  (122, 'Asset', 1, 0, '2018-04-16 12:07:04', '2018-04-16 12:07:04', 'df7cc768-0df3-43ca-8cf6-815e680759ee'),
  (123, 'Asset', 1, 0, '2018-04-16 12:07:04', '2018-04-16 12:07:04', '36be2ecd-0cdf-432b-9083-1f9a4273f878'),
  (124, 'MatrixBlock', 1, 0, '2018-04-16 12:08:50', '2018-04-16 14:10:02', 'a27d9085-f1fe-4c7c-ba30-05c3be42d709'),
  (125, 'MatrixBlock', 1, 0, '2018-04-16 12:08:50', '2018-04-16 14:10:02', '28d42b8f-c037-459f-83c5-a2c23a11f056'),
  (127, 'MatrixBlock', 1, 0, '2018-04-16 12:13:44', '2018-04-16 14:10:02', '82c2e2db-6440-4dc4-8ddd-2d1f1fdc622f'),
  (128, 'Entry', 1, 0, '2018-04-16 14:02:31', '2018-04-16 16:08:23', '05269934-b7ca-4818-9faf-8b258c599411'),
  (135, 'Entry', 1, 0, '2018-04-16 14:06:18', '2018-04-16 15:26:48', '8c5673fa-5f5e-4d32-86c0-3d9ba721eacb'),
  (140, 'Entry', 1, 0, '2018-04-16 14:07:57', '2018-04-16 16:19:43', '0f271128-5a22-4bad-a34a-87278d9e539a'),
  (146, 'MatrixBlock', 1, 0, '2018-04-16 15:18:32', '2018-04-16 16:08:23', 'f188df79-a44f-4f3b-841f-7703a4f2599a'),
  (147, 'MatrixBlock', 1, 0, '2018-04-16 15:21:14', '2018-04-16 16:08:23', 'aedfe362-064c-4d30-9344-6198db328021'),
  (148, 'MatrixBlock', 1, 0, '2018-04-16 15:21:14', '2018-04-16 16:08:23', '2511b8a7-0aa0-4e7a-8f0d-13c85c28eb91'),
  (149, 'MatrixBlock', 1, 0, '2018-04-16 15:21:14', '2018-04-16 16:08:23', '85ba2859-6a98-4b72-8ff8-484893e85bb0'),
  (150, 'MatrixBlock', 1, 0, '2018-04-16 15:21:14', '2018-04-16 16:08:23', 'b62ad58d-dac5-44fd-8cec-3b8e7436bc7b'),
  (154, 'MatrixBlock', 1, 0, '2018-04-16 15:26:48', '2018-04-16 15:26:48', '7cbb5c82-e5a0-41ad-9833-b7f5dc402847'),
  (155, 'MatrixBlock', 1, 0, '2018-04-16 15:26:48', '2018-04-16 15:26:48', 'efe2f935-42be-48f5-aa0f-40567ecfceac'),
  (156, 'MatrixBlock', 1, 0, '2018-04-16 15:26:48', '2018-04-16 15:26:48', '970c5be4-b1af-4327-9684-b180fe72175b'),
  (157, 'MatrixBlock', 1, 0, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '9296c35b-5ec0-4a68-863c-34f7544f6eed'),
  (158, 'MatrixBlock', 1, 0, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '0dbaa3a2-c64e-48d5-a054-19d427b2b0d8'),
  (159, 'MatrixBlock', 1, 0, '2018-04-16 15:28:12', '2018-04-16 16:19:43', 'a7c177f2-3ea2-4871-9b5b-0b4c3e0fcfea'),
  (160, 'MatrixBlock', 1, 0, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '41969553-9d4c-452a-a0fd-626814db7508'),
  (161, 'Asset', 1, 0, '2018-04-30 17:51:30', '2018-04-30 17:51:30', '0e82f780-5920-46bd-b3ca-3795f8f3ddf6'),
  (162, 'MatrixBlock', 1, 0, '2018-04-30 17:51:42', '2018-04-30 17:51:42', 'ce7d49cd-ec90-47aa-b265-7a757d500616');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_elements_i18n`
--

CREATE TABLE `craft_elements_i18n` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_elements_i18n`
--

INSERT INTO `craft_elements_i18n` (`id`, `elementId`, `locale`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 1, 'en', '', NULL, 1, '2018-03-28 09:59:37', '2018-03-28 14:48:51', 'da06fe9f-d7be-411c-bd6b-0e0eefc4fef9'),
  (2, 2, 'en', 'homepage', '__home__', 1, '2018-03-28 09:59:41', '2018-04-02 08:22:04', '9946bcf2-8dc4-4fa0-aaf0-651c51783d9c'),
  (3, 3, 'en', 'we-just-installed-craft', 'news/2018/we-just-installed-craft', 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'e4b83fbc-a264-495e-9fdf-84fac1977981'),
  (4, 4, 'en', '', NULL, 1, '2018-04-02 08:37:52', '2018-04-02 09:34:36', '55e995fb-f616-4a5a-8a45-44540d90fdc2'),
  (7, 7, 'en', 'logo-inverse', NULL, 1, '2018-04-02 11:34:19', '2018-04-02 11:34:19', '54caca08-7fd7-4881-8bbc-7fd71bcc01a7'),
  (8, 8, 'en', 'logo', NULL, 1, '2018-04-02 11:34:19', '2018-04-02 11:34:19', '180dc1cd-8e79-467f-8aed-825066d4077d'),
  (9, 9, 'en', 'homepage', 'pages/homepage', 1, '2018-04-02 15:22:42', '2018-04-05 11:41:21', '3be026d8-692c-4c32-94ce-74f3537ccba5'),
  (10, 10, 'en', '', NULL, 1, '2018-04-02 15:22:42', '2018-04-05 11:41:21', '64a0cc43-439a-4111-b12c-ba38963aee2f'),
  (11, 11, 'en', 'services', 'pages/services', 1, '2018-04-02 15:32:17', '2018-04-06 09:42:52', '653aba2e-c547-48ce-8359-95bc69ae9184'),
  (12, 12, 'en', 'pricing', 'pages/pricing', 1, '2018-04-02 15:33:05', '2018-04-16 14:10:02', '1bcb62cf-a5e4-4b88-a761-cba8cb0d90a2'),
  (13, 13, 'en', 'facilities', 'pages/facilities', 1, '2018-04-02 15:33:24', '2018-04-06 12:37:18', '414e4a79-963a-4bff-81fb-75ca012c3505'),
  (14, 14, 'en', 'training', 'pages/training', 1, '2018-04-02 15:34:08', '2018-04-10 07:58:38', '107edd4b-e5a5-400d-9977-3209531576d3'),
  (15, 15, 'en', 'about-us', 'pages/about-us', 1, '2018-04-02 15:34:22', '2018-04-10 14:59:28', 'bdd09fab-53ae-4b84-b168-3123fea6c8d6'),
  (16, 16, 'en', 'pupparazzi', 'pages/pupparazzi', 1, '2018-04-02 15:34:39', '2018-04-30 17:51:42', 'a788680c-1f98-4e55-a1b4-29e91660d8d1'),
  (17, 17, 'en', 'contact-us', 'pages/contact-us', 1, '2018-04-02 15:34:57', '2018-04-04 08:38:45', '5f8c93a7-6691-4cb6-a6e0-344cbe4f028e'),
  (18, 18, 'en', 'privacy-policy', 'pages/privacy-policy', 1, '2018-04-03 12:46:49', '2018-04-04 08:38:45', '3c3f1d10-d950-4def-b25c-5ce648fa2305'),
  (19, 19, 'en', 'terms-conditions', 'pages/terms-conditions', 1, '2018-04-03 12:47:28', '2018-04-04 08:38:46', 'e1a84250-3fae-48f8-806e-e381ee0670a1'),
  (20, 20, 'en', 'cookies', 'pages/cookies', 1, '2018-04-03 12:47:50', '2018-04-04 08:38:46', '18402755-7a46-4125-9610-8b855cd862ca'),
  (21, 21, 'en', 'vat-registration-264563488', 'pages/vat-registration-264563488', 1, '2018-04-03 12:48:26', '2018-04-04 08:38:46', '15d3fcab-eabc-46c4-8bc4-bc03559bb1c6'),
  (22, 22, 'en', 'facebook', NULL, 1, '2018-04-03 15:07:10', '2018-04-03 15:07:10', 'e9782e82-587a-4602-904f-90635e0c856b'),
  (23, 23, 'en', 'instagram', NULL, 1, '2018-04-03 15:07:10', '2018-04-03 15:07:10', 'c4310fe3-fce1-46b9-b8c7-48654d0e36f4'),
  (24, 24, 'en', 'twitter', NULL, 1, '2018-04-03 15:07:11', '2018-04-03 15:07:11', '4146fb33-8564-43b4-8811-0460ee7c880a'),
  (25, 25, 'en', 'youtube', NULL, 1, '2018-04-03 15:07:11', '2018-04-03 15:07:11', 'a0fee25f-cfc9-42f5-ad6d-0f65a2fb5f5b'),
  (26, 26, 'en', 'twitter', NULL, 1, '2018-04-03 15:08:30', '2018-04-03 15:08:30', 'c10be9cb-45d7-44e6-9e50-450201c70d68'),
  (27, 27, 'en', 'facebook', NULL, 1, '2018-04-03 15:08:59', '2018-04-03 15:08:59', '10129301-1e7b-4e77-932a-8b5058880e29'),
  (28, 28, 'en', 'youtube', NULL, 1, '2018-04-03 15:09:19', '2018-04-03 15:09:19', '63b5bcad-5ae6-4f3b-b590-55c65210eca0'),
  (29, 29, 'en', 'instagram', NULL, 1, '2018-04-03 15:09:33', '2018-04-03 15:09:33', '41aa3baf-90cb-4c00-8364-4bd37d4e5863'),
  (30, 30, 'en', 'art-cover-welcome-2', NULL, 1, '2018-04-03 16:29:27', '2018-04-03 16:29:27', '1b2e63cc-61ac-4751-b7ca-568d357b2329'),
  (31, 31, 'en', 'art-cover-welcome', NULL, 1, '2018-04-03 16:29:28', '2018-04-03 16:29:28', '8bdd6944-266a-4720-9735-b0033d706e0a'),
  (32, 32, 'en', '', NULL, 1, '2018-04-03 16:32:55', '2018-04-05 11:41:21', '68cd1a42-4c54-43e2-96d0-1fad291b5ace'),
  (33, 33, 'en', '', NULL, 1, '2018-04-04 11:01:27', '2018-04-05 11:41:21', '0224e6b9-3469-4a85-985b-9a01306bde37'),
  (34, 34, 'en', 'art-testimonial-1', NULL, 1, '2018-04-05 10:15:06', '2018-04-05 10:15:06', '82d22d0d-ecfc-485e-9019-74ca6e0d02ad'),
  (35, 35, 'en', '', NULL, 1, '2018-04-05 10:19:28', '2018-04-05 11:41:21', 'cc453178-7671-42cf-9084-893fcb75b36f'),
  (36, 36, 'en', '', NULL, 1, '2018-04-05 10:19:28', '2018-04-05 11:41:21', '01bc5235-f9fe-4656-a275-2f1402bd44dc'),
  (37, 37, 'en', '', NULL, 1, '2018-04-05 10:19:28', '2018-04-05 11:41:21', 'f28eeac5-210d-4632-a196-45ce1c76023b'),
  (38, 38, 'en', 'order-background', NULL, 1, '2018-04-05 10:32:44', '2018-04-05 10:33:40', 'a6187a76-50e9-4433-af0c-65f3b96da7c9'),
  (39, 39, 'en', '', NULL, 1, '2018-04-05 10:37:37', '2018-04-05 11:41:21', '5cbcf34f-8050-4d4d-8a72-9c789af86672'),
  (40, 40, 'en', 'card-1', NULL, 1, '2018-04-05 11:32:26', '2018-04-05 11:32:26', '1b0bb0b8-0148-49df-8f3e-a0cf4795b56e'),
  (41, 41, 'en', 'card-3', NULL, 1, '2018-04-05 11:32:27', '2018-04-05 11:32:27', 'f235a0a0-5bfb-4864-8576-b79887d2d747'),
  (42, 42, 'en', 'card-4', NULL, 1, '2018-04-05 11:32:28', '2018-04-05 11:32:28', '8513501b-87ee-4e0b-88ea-aba4f7a551f6'),
  (43, 43, 'en', '', NULL, 1, '2018-04-05 11:34:09', '2018-04-05 11:41:21', '766f6232-1222-48b2-a8dc-61d649353de8'),
  (44, 44, 'en', '', NULL, 1, '2018-04-05 11:35:39', '2018-04-05 11:41:21', '98525c65-12b0-4074-8002-a78ec76b4a28'),
  (45, 45, 'en', '', NULL, 1, '2018-04-05 11:35:39', '2018-04-05 11:41:21', 'e9a217c9-db1a-4b80-9792-954371b8939c'),
  (46, 46, 'en', '', NULL, 1, '2018-04-05 11:35:39', '2018-04-05 11:41:21', 'cf45c032-96a5-415a-b217-37732ceca926'),
  (47, 47, 'en', 'service-1', NULL, 1, '2018-04-06 07:41:01', '2018-04-06 07:41:01', '1efd88c9-00b0-4f0e-a4d5-bf409cb23389'),
  (48, 48, 'en', 'service-2', NULL, 1, '2018-04-06 07:41:24', '2018-04-06 07:41:24', '5fd80c98-28aa-40ec-8b24-e852828fbbfe'),
  (49, 49, 'en', 'service-3', NULL, 1, '2018-04-06 07:41:46', '2018-04-06 07:41:46', '5a54d364-3795-4b06-ae02-628804b30728'),
  (50, 50, 'en', 'service-4', NULL, 1, '2018-04-06 07:42:09', '2018-04-06 07:42:09', '58f8e874-e83f-4258-bd4c-d3a84d48906f'),
  (51, 51, 'en', 'art-cover-service', NULL, 1, '2018-04-06 07:50:02', '2018-04-06 07:50:02', 'c10da559-a28e-483e-8fa2-612569a19bb0'),
  (52, 52, 'en', '', NULL, 1, '2018-04-06 08:10:45', '2018-04-06 09:42:52', '942da23b-4c3f-41b7-abf1-bf3349378511'),
  (53, 53, 'en', '', NULL, 1, '2018-04-06 08:26:49', '2018-04-06 09:42:52', '395a5b7b-2084-4c76-bda5-b37af208de83'),
  (54, 54, 'en', '', NULL, 1, '2018-04-06 08:55:23', '2018-04-06 09:42:52', '886794b1-526b-433d-b25c-13ba04c5806e'),
  (55, 55, 'en', '', NULL, 1, '2018-04-06 08:55:23', '2018-04-06 09:42:52', '61b8c95f-bca8-42f5-90e1-f29bf65a0dd1'),
  (56, 56, 'en', '', NULL, 1, '2018-04-06 08:55:23', '2018-04-06 09:42:52', '2c8973b3-4baf-4709-9852-8d513b998916'),
  (57, 57, 'en', '', NULL, 1, '2018-04-06 08:55:23', '2018-04-06 09:42:52', 'c7c242b8-f724-477d-ae51-78f6f166f43a'),
  (58, 58, 'en', '', NULL, 1, '2018-04-06 09:42:52', '2018-04-06 09:42:52', '98bd6df8-6c64-40ce-84e3-aa56fc80d878'),
  (59, 59, 'en', 'art-cover-about', NULL, 1, '2018-04-06 12:21:32', '2018-04-06 12:21:32', 'bb192865-d981-491a-bb3d-657a6c949868'),
  (60, 60, 'en', 'art-cover-facilities', NULL, 1, '2018-04-06 12:21:33', '2018-04-06 12:21:33', '3fad1feb-edd3-40e1-88ed-fc53d20d0059'),
  (61, 61, 'en', 'art-cover-pricing', NULL, 1, '2018-04-06 12:21:33', '2018-04-06 12:21:33', '5193631a-ac20-4412-85dd-461f417e155a'),
  (63, 63, 'en', 'art-cover-training', NULL, 1, '2018-04-06 12:22:29', '2018-04-06 12:22:29', '0407847a-fc36-484a-8ee9-8d3f86c22536'),
  (66, 66, 'en', 'art-cover-service', NULL, 1, '2018-04-06 12:25:07', '2018-04-06 12:25:08', '1de52430-a9f6-4394-a1d4-c5e7d3fcebcf'),
  (67, 67, 'en', 'art-cover-welcome-2', NULL, 1, '2018-04-06 12:25:08', '2018-04-06 12:25:08', '9020bcc1-5dfc-4ae5-8f7c-db17b8fbf5d1'),
  (68, 68, 'en', 'art-cover-welcome', NULL, 1, '2018-04-06 12:25:08', '2018-04-06 12:25:08', '705af344-01c0-4b5b-8b84-ed4229dd97ee'),
  (69, 69, 'en', 'facility-1-1', NULL, 1, '2018-04-06 12:35:06', '2018-04-06 12:35:06', '4718981b-4bc5-4463-8203-a541d97e9384'),
  (70, 70, 'en', 'facility-1-2', NULL, 1, '2018-04-06 12:35:07', '2018-04-06 12:35:07', '0ce3d6c2-b83f-431c-a9b7-49f12552e948'),
  (71, 71, 'en', 'facility-1-3', NULL, 1, '2018-04-06 12:35:08', '2018-04-06 12:35:08', '19361e84-ed68-464d-b4fa-1818206f3593'),
  (72, 72, 'en', 'facility-2-1', NULL, 1, '2018-04-06 12:35:17', '2018-04-06 12:35:17', '80c31342-da10-4faf-95d6-2686b7b0f447'),
  (73, 73, 'en', 'facility-2-2', NULL, 1, '2018-04-06 12:35:17', '2018-04-06 12:35:17', '34030f7d-51b7-4f43-a6cb-c6c76274d290'),
  (74, 74, 'en', 'facility-2-3', NULL, 1, '2018-04-06 12:35:18', '2018-04-06 12:35:18', 'e61972d5-f0be-4647-b728-529ff23835f9'),
  (75, 75, 'en', 'facility-3-1', NULL, 1, '2018-04-06 12:35:33', '2018-04-06 12:35:33', '7e696042-056c-40d8-a143-597a4f047794'),
  (76, 76, 'en', 'facility-3-2', NULL, 1, '2018-04-06 12:35:34', '2018-04-06 12:35:34', 'd0a0a7d6-1a3a-4de0-8656-203ca397f2b4'),
  (77, 77, 'en', 'facility-3-3', NULL, 1, '2018-04-06 12:35:35', '2018-04-06 12:35:35', 'd4511127-6d7f-4b97-b4eb-290041fe0f4b'),
  (78, 78, 'en', 'facility-4-1', NULL, 1, '2018-04-06 12:35:42', '2018-04-06 12:35:42', '69d7c533-db30-45c7-b7bc-59df78e4e96a'),
  (79, 79, 'en', 'facility-4-2', NULL, 1, '2018-04-06 12:35:43', '2018-04-06 12:35:43', '5928d688-ac12-4b8a-8eec-4e830049ef2e'),
  (80, 80, 'en', 'facility-4-3', NULL, 1, '2018-04-06 12:35:44', '2018-04-06 12:35:44', 'a078877b-4804-4397-8e12-324ac875cc66'),
  (81, 81, 'en', 'facility-5-1', NULL, 1, '2018-04-06 12:35:58', '2018-04-06 12:35:58', 'b86de0f4-0af7-4f7e-a3c2-9dee1910b773'),
  (82, 82, 'en', 'facility-5-2', NULL, 1, '2018-04-06 12:35:58', '2018-04-06 12:35:58', '3d7a4c57-0e96-4bb3-9a5f-14106e13ae6f'),
  (83, 83, 'en', 'facility-5-3', NULL, 1, '2018-04-06 12:35:59', '2018-04-06 12:35:59', 'ee4c8bd7-519a-442e-9c9f-b76708dda88a'),
  (84, 84, 'en', 'facility-6-1', NULL, 1, '2018-04-06 12:36:14', '2018-04-06 12:36:14', 'a4c65d41-04c3-4e19-8f5e-947cb48b7684'),
  (85, 85, 'en', 'facility-6-2', NULL, 1, '2018-04-06 12:36:15', '2018-04-06 12:36:15', '2d4c8508-277f-462b-b2d3-f4d87226343c'),
  (86, 86, 'en', 'facility-6-3', NULL, 1, '2018-04-06 12:36:16', '2018-04-06 12:36:16', '439bc99f-0c6d-46ca-be1a-3cdf433b248d'),
  (87, 87, 'en', '', NULL, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '6af5751a-c31a-48aa-9694-bfc01a41e0f2'),
  (88, 88, 'en', '', NULL, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '85f85236-bb61-4c2b-988e-df939f25d5d7'),
  (89, 89, 'en', '', NULL, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '28e83c0a-396b-4343-b61f-fee0d45f07bf'),
  (90, 90, 'en', '', NULL, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '74ba2ce5-4e08-4b1a-8222-1f2f27083b80'),
  (91, 91, 'en', '', NULL, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '9ec48156-0b45-4a9d-96dd-cddd9a3bc6f0'),
  (92, 92, 'en', '', NULL, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '9b9506a5-0a8a-4fda-bfd5-b9d4236b0b7b'),
  (93, 93, 'en', '', NULL, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '7ba2b84d-af84-4ae0-a56e-67622c34a43f'),
  (94, 94, 'en', '', NULL, 1, '2018-04-09 11:04:30', '2018-04-10 07:58:38', '8053636a-c442-4475-a876-281a13978d64'),
  (95, 95, 'en', 'training-video', NULL, 1, '2018-04-09 14:10:27', '2018-04-09 14:10:27', '033e0c69-b0ef-4b13-80e2-52c72512b82c'),
  (96, 96, 'en', 'training-1', NULL, 1, '2018-04-10 07:57:19', '2018-04-10 07:57:19', 'fedca07e-f94b-4b35-88f0-9e7f9a8bb628'),
  (97, 97, 'en', 'training-2', NULL, 1, '2018-04-10 07:57:20', '2018-04-10 07:57:20', 'c11272c3-9a65-4078-b974-a62e4d258219'),
  (98, 98, 'en', 'training-3', NULL, 1, '2018-04-10 07:57:23', '2018-04-10 07:57:23', '6947de8b-34f7-4efe-a7a5-63e4154ad83a'),
  (99, 99, 'en', 'training-4', NULL, 1, '2018-04-10 07:57:25', '2018-04-10 07:57:25', '78e70a86-4c29-44c0-ae57-5fc46bdf22fa'),
  (100, 100, 'en', '', NULL, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '86b06fc0-661d-437c-958b-eacb85e3f225'),
  (101, 101, 'en', '', NULL, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '93c5948d-bedc-479f-af00-8a4f32d7f14b'),
  (102, 102, 'en', '', NULL, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'c621da36-9f46-441f-8413-1e927f2b2527'),
  (103, 103, 'en', '', NULL, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'f8aa8524-d7a6-4798-9a4a-9040140074d0'),
  (104, 104, 'en', '', NULL, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'fbe11d5c-a404-4cd3-8a68-e734f964d277'),
  (105, 105, 'en', 'feedback-list', 'feedback-list/feedback-list', 1, '2018-04-10 12:31:27', '2018-04-11 16:53:59', 'b48bf7b6-b291-428c-961b-7ab47424955b'),
  (106, 106, 'en', '', NULL, 1, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '9deed4c5-3112-4f62-a115-d43adc59a9da'),
  (107, 107, 'en', '', NULL, 1, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '8a6ae8e5-fd6c-4dd0-bafe-27683a92b291'),
  (108, 108, 'en', '', NULL, 1, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '5992621d-20a5-4950-9471-ed8f146b6a5a'),
  (109, 109, 'en', '', NULL, 1, '2018-04-10 14:48:46', '2018-04-10 14:59:28', '5180ac17-cf3e-4b9e-bb5d-0eee03b78648'),
  (110, 110, 'en', 'about-video', NULL, 1, '2018-04-10 14:58:57', '2018-04-10 14:58:57', '57b9f700-21b4-442a-bbe3-21de16c35ec3'),
  (111, 111, 'en', '', NULL, 1, '2018-04-10 14:59:28', '2018-04-10 14:59:28', '9b5587f3-b05b-48b0-af63-e08e23efeb80'),
  (112, 112, 'en', '', NULL, 1, '2018-04-10 14:59:28', '2018-04-10 14:59:28', 'aca0a677-8a0b-4d62-9633-eff12ee54049'),
  (113, 113, 'en', '', NULL, 1, '2018-04-10 14:59:28', '2018-04-10 14:59:28', 'b713e843-ce1a-4623-8c7c-8ad66acfaf17'),
  (114, 114, 'en', '', NULL, 1, '2018-04-16 09:14:17', '2018-04-16 14:10:02', '62e43891-fa00-459d-acd5-e533f3b0cd2d'),
  (121, 121, 'en', 'flat-grid-view-1', NULL, 1, '2018-04-16 12:07:03', '2018-04-16 12:07:03', 'de7e2626-a83e-43bf-aa1a-d54ee33d2de8'),
  (122, 122, 'en', 'flat-grid-view-2', NULL, 1, '2018-04-16 12:07:04', '2018-04-16 12:07:04', '89d02730-b657-4188-ab68-00e9c087bba7'),
  (123, 123, 'en', 'flat-grid-view-3', NULL, 1, '2018-04-16 12:07:04', '2018-04-16 12:07:04', '09f85afe-c3cd-45b6-bd9a-22de036d9cdb'),
  (124, 124, 'en', '', NULL, 1, '2018-04-16 12:08:50', '2018-04-16 14:10:02', '5e72331b-8bf9-4d06-954e-1db2a2a031bb'),
  (125, 125, 'en', '', NULL, 1, '2018-04-16 12:08:50', '2018-04-16 14:10:02', '5c27cd7a-b56d-4e47-bf3f-d0989bd19d10'),
  (127, 127, 'en', '', NULL, 1, '2018-04-16 12:13:44', '2018-04-16 14:10:02', 'fd657c0b-2206-46d8-89f7-38d9406e5c87'),
  (128, 128, 'en', 'day-care', 'price-lists/day-care', 1, '2018-04-16 14:02:31', '2018-04-16 16:08:23', '17f518b8-9841-420f-a0ed-3b794a051f9d'),
  (135, 135, 'en', 'boarding-kennels', 'price-lists/boarding-kennels', 1, '2018-04-16 14:06:18', '2018-04-16 15:26:48', '34206dd8-8ac7-45ef-b960-8dd918f0629c'),
  (140, 140, 'en', 'dog-training-behaviour-correction-and-puppy-education', 'price-lists/dog-training-behaviour-correction-and-puppy-education', 1, '2018-04-16 14:07:57', '2018-04-16 16:19:43', 'bc5fc29e-29e8-4ba3-ae53-0839298afc27'),
  (146, 146, 'en', '', NULL, 1, '2018-04-16 15:18:32', '2018-04-16 16:08:23', '4c3cbc15-8c00-4300-ab9a-7fc9afe54934'),
  (147, 147, 'en', '', NULL, 1, '2018-04-16 15:21:14', '2018-04-16 16:08:23', '1b31588e-4014-472f-a3ea-23f96cda4f1a'),
  (148, 148, 'en', '', NULL, 1, '2018-04-16 15:21:14', '2018-04-16 16:08:23', '0b6088fa-00b7-4515-a981-55ca916f73e8'),
  (149, 149, 'en', '', NULL, 1, '2018-04-16 15:21:14', '2018-04-16 16:08:23', '9a36839f-5f12-419c-8e53-6f458ef89709'),
  (150, 150, 'en', '', NULL, 1, '2018-04-16 15:21:14', '2018-04-16 16:08:23', 'd34dd713-e3bd-40d9-ad79-e5ecd9124c0c'),
  (154, 154, 'en', '', NULL, 1, '2018-04-16 15:26:48', '2018-04-16 15:26:48', '108c5d7b-f156-484d-b355-e8e1a3d66e3d'),
  (155, 155, 'en', '', NULL, 1, '2018-04-16 15:26:48', '2018-04-16 15:26:48', '64269bf4-2720-416b-b2fd-888af866d71d'),
  (156, 156, 'en', '', NULL, 1, '2018-04-16 15:26:48', '2018-04-16 15:26:48', '11202403-c414-4dd1-a7f3-f0ed4ce89ad1'),
  (157, 157, 'en', '', NULL, 1, '2018-04-16 15:28:12', '2018-04-16 16:19:43', 'fa016e41-6d17-4951-9654-575d3cb15d2c'),
  (158, 158, 'en', '', NULL, 1, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '9d5fbeed-7cd4-43ac-8cca-b00f6bcbc95a'),
  (159, 159, 'en', '', NULL, 1, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '2662cacf-c2fb-43f3-a606-82fde83debd5'),
  (160, 160, 'en', '', NULL, 1, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '200b9fcf-b6bd-4ae4-979c-2912bd30dbe9'),
  (161, 161, 'en', 'pupparazzi-cover', NULL, 1, '2018-04-30 17:51:30', '2018-04-30 17:51:30', '6da3e85a-49ea-4a5e-b1e4-7ef6f1ca66f4'),
  (162, 162, 'en', '', NULL, 1, '2018-04-30 17:51:42', '2018-04-30 17:51:42', '59793869-9c22-49e2-8223-dfa6c1028895');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_emailmessages`
--

CREATE TABLE `craft_emailmessages` (
  `id` int(11) NOT NULL,
  `key` char(150) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_entries`
--

CREATE TABLE `craft_entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_entries`
--

INSERT INTO `craft_entries` (`id`, `sectionId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (2, 1, 1, NULL, '2018-04-02 08:22:03', NULL, '2018-03-28 09:59:41', '2018-04-02 08:22:03', '6e905ee2-8e5b-45b5-a202-41dff1a3aeb7'),
  (3, 2, 2, 1, '2018-03-28 09:59:41', NULL, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '3ffaf3c5-bd3f-4f5c-837d-4f673b65c9a2'),
  (9, 3, 3, 1, '2018-04-02 15:22:00', NULL, '2018-04-02 15:22:42', '2018-04-05 11:41:21', '486094ed-cf30-4a8a-a193-447f6ddd0eb6'),
  (11, 3, 4, 1, '2018-04-02 15:32:00', NULL, '2018-04-02 15:32:17', '2018-04-06 09:42:52', 'ee4a35ef-3924-4bd8-b772-dc24f1cbccc8'),
  (12, 3, 5, 1, '2018-04-02 15:33:00', NULL, '2018-04-02 15:33:05', '2018-04-16 14:10:02', 'b4472a92-7cc2-41be-81d9-bb9f6829b547'),
  (13, 3, 6, 1, '2018-04-02 15:33:00', NULL, '2018-04-02 15:33:24', '2018-04-06 12:37:18', 'c0d89e5d-881b-417c-8e2e-27f27ea866cf'),
  (14, 3, 7, 1, '2018-04-02 15:34:00', NULL, '2018-04-02 15:34:08', '2018-04-10 07:58:38', '5aa3bed3-bbcc-4792-9853-93b8ddc0ca11'),
  (15, 3, 8, 1, '2018-04-02 15:34:00', NULL, '2018-04-02 15:34:22', '2018-04-10 14:59:28', '858ad111-6777-4dc9-94ed-8634bfdf66b9'),
  (16, 3, 9, 1, '2018-04-02 15:34:00', NULL, '2018-04-02 15:34:39', '2018-04-30 17:51:42', '3a3bde7c-6392-41a8-99aa-5a6091af25c7'),
  (17, 3, 10, 1, '2018-04-02 15:34:00', NULL, '2018-04-02 15:34:57', '2018-04-03 12:42:25', '3a4a99e4-cafc-4b08-8590-d7790dbe95d3'),
  (18, 3, 11, 1, '2018-04-03 12:46:49', NULL, '2018-04-03 12:46:49', '2018-04-03 12:46:49', '9d0946f8-dc28-4d47-96e2-563b838c7cf8'),
  (19, 3, 11, 1, '2018-04-03 12:47:28', NULL, '2018-04-03 12:47:28', '2018-04-03 12:47:28', '44c363c0-3590-4e98-80b7-0bdef97a9cf6'),
  (20, 3, 11, 1, '2018-04-03 12:47:50', NULL, '2018-04-03 12:47:50', '2018-04-03 12:47:50', '4448818c-6d58-46c4-980f-3862cdcb5bcf'),
  (21, 3, 11, 1, '2018-04-03 12:48:26', NULL, '2018-04-03 12:48:26', '2018-04-03 12:48:26', '3845d1a6-7586-45df-972e-a848cdfc35d2'),
  (26, 4, 12, 1, '2018-04-03 15:07:00', NULL, '2018-04-03 15:08:30', '2018-04-03 15:08:30', '87c3b2b6-cd08-4901-a7b2-d963318c9cbb'),
  (27, 4, 12, 1, '2018-04-03 15:08:59', NULL, '2018-04-03 15:08:59', '2018-04-03 15:08:59', '795656e0-68fb-4473-9edd-1d024d25d1ea'),
  (28, 4, 12, 1, '2018-04-03 15:09:19', NULL, '2018-04-03 15:09:19', '2018-04-03 15:09:19', '51b81bec-11f2-45e0-bd05-1e0a5aa1efb1'),
  (29, 4, 12, 1, '2018-04-03 15:09:33', NULL, '2018-04-03 15:09:34', '2018-04-03 15:09:34', 'c30da4d9-72d6-4ef3-83cb-a0c164617e5e'),
  (105, 5, 13, 1, '2018-04-10 12:31:00', NULL, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '58b279f8-fb2c-4baa-9903-b53ed8c63beb'),
  (128, 6, 14, 1, '2018-04-16 14:02:00', NULL, '2018-04-16 14:02:31', '2018-04-16 16:08:23', '20f5a795-261c-48a6-bbb6-4a6ba357c08d'),
  (135, 6, 14, 1, '2018-04-16 14:06:00', NULL, '2018-04-16 14:06:18', '2018-04-16 15:26:48', '3a7eec09-a9b9-4ffa-8456-77949c21fbb5'),
  (140, 6, 14, 1, '2018-04-16 14:07:00', NULL, '2018-04-16 14:07:57', '2018-04-16 16:19:43', '77aca068-6386-4f30-bb3a-fa49c7ea476f');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_entrydrafts`
--

CREATE TABLE `craft_entrydrafts` (
  `id` int(11) NOT NULL,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_entrytypes`
--

CREATE TABLE `craft_entrytypes` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hasTitleField` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'Title',
  `titleFormat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_entrytypes`
--

INSERT INTO `craft_entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 1, 3, 'Homepage', 'homepage', 1, 'Title', NULL, 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'ad51f669-f2e8-4113-870c-fbf060ee9763'),
  (2, 2, 5, 'News', 'news', 1, 'Title', NULL, 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '8fef13a0-8955-4928-aa6c-7dd67b012300'),
  (3, 3, 171, 'Home Page', 'homePage', 1, 'Title', NULL, 1, '2018-04-02 13:55:56', '2018-05-03 11:11:32', 'ef393d08-9c89-4382-bfe7-4e061b737000'),
  (4, 3, 115, 'Services Page', 'servicesPage', 1, 'Title', NULL, 2, '2018-04-02 15:01:27', '2018-04-06 09:41:46', '34910f26-d3c7-488d-9193-aee2ece6843c'),
  (5, 3, 160, 'Pricing Page', 'pricingPage', 1, 'Title', NULL, 3, '2018-04-02 15:17:20', '2018-04-16 14:09:33', 'd971b4cb-060c-4ef9-9f01-f4fc9fceab8a'),
  (6, 3, 119, 'Facilities Page', 'facilitiesPage', 1, 'Title', NULL, 4, '2018-04-02 15:17:39', '2018-04-06 12:30:40', '725c8b10-6fa6-4fa2-8195-9de7ba3ddf7b'),
  (7, 3, 129, 'Training Page', 'trainingPage', 1, 'Title', NULL, 5, '2018-04-02 15:19:06', '2018-04-09 11:03:43', '41d83102-ad68-41a9-8960-c6da10cc6dbd'),
  (8, 3, 138, 'About Us Page', 'aboutUsPage', 1, 'Title', NULL, 6, '2018-04-02 15:19:28', '2018-04-10 14:43:13', '010720ac-4849-4330-a65d-6f13dab2963d'),
  (9, 3, 170, 'Pupparazzi Page', 'pupparazziPage', 1, 'Title', NULL, 7, '2018-04-02 15:19:53', '2018-05-03 11:08:36', 'd4570480-d255-464c-bbaa-c4baf06c247b'),
  (10, 3, 66, 'Contact Us Page', 'contactUsPage', 1, 'Title', NULL, 8, '2018-04-02 15:20:12', '2018-04-03 12:40:48', 'fff601b4-b7f1-4e4a-adf3-f8b01e4e3ecf'),
  (11, 3, 68, 'Mere Page', 'merePage', 1, 'Title', NULL, 9, '2018-04-03 12:46:06', '2018-04-03 12:48:40', '80fc7d5c-c3c8-4905-a4c0-97f363a472d1'),
  (12, 4, 70, 'Social Links', 'socialLinks', 1, 'Title', NULL, 1, '2018-04-03 14:57:48', '2018-04-03 15:02:07', '89cac5b8-3b86-43eb-b271-713b1e753518'),
  (13, 5, 133, 'Feedback List', 'feedbackList', 1, 'Title', NULL, 1, '2018-04-10 12:10:46', '2018-04-10 12:25:20', '0f35341a-c36a-4da4-b087-9d6c09d34979'),
  (14, 6, 167, 'Price Lists', 'priceLists', 1, 'Title', NULL, 1, '2018-04-16 13:52:02', '2018-04-16 15:23:03', 'aa5ad5b8-0c8f-466b-8c95-46983523a48f'),
  (15, 7, 176, 'Blog', 'blog', 1, 'Title', NULL, 1, '2018-05-03 11:13:44', '2018-05-03 11:29:03', 'cb6eb94e-67b8-46e0-8e96-6cf3058168d2');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_entryversions`
--

CREATE TABLE `craft_entryversions` (
  `id` int(11) NOT NULL,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `num` smallint(6) UNSIGNED NOT NULL,
  `notes` tinytext COLLATE utf8_unicode_ci,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_entryversions`
--

INSERT INTO `craft_entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 2, 1, 1, 'en', 1, NULL, '{\"typeId\":\"1\",\"authorId\":null,\"title\":\"Homepage\",\"slug\":\"homepage\",\"postDate\":1522231181,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}', '2018-03-28 09:59:41', '2018-03-28 09:59:41', '7c5f78c9-4ede-4a78-9c45-e4bbde331c07'),
  (2, 2, 1, 1, 'en', 2, NULL, '{\"typeId\":null,\"authorId\":null,\"title\":\"Welcome to Localhost!\",\"slug\":\"homepage\",\"postDate\":1522231181,\"expiryDate\":null,\"enabled\":\"1\",\"parentId\":null,\"fields\":{\"1\":\"<p>It\\u2019s true, this site doesn\\u2019t have a whole lot of content yet, but don\\u2019t worry. Our web developers have just installed the CMS, and they\\u2019re setting things up for the content editors this very moment. Soon Localhost will be an oasis of fresh perspectives, sharp analyses, and astute opinions that will keep you coming back again and again.<\\/p>\"}}', '2018-03-28 09:59:41', '2018-03-28 09:59:41', '01a0e689-434d-49f7-b478-67602e4bfdab'),
  (3, 3, 2, 1, 'en', 1, NULL, '{\"typeId\":\"2\",\"authorId\":\"1\",\"title\":\"We just installed Craft!\",\"slug\":\"we-just-installed-craft\",\"postDate\":1522231181,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":[]}', '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'dd9e6475-1481-4498-831f-a4ec81e81f57'),
  (4, 9, 3, 1, 'en', 1, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"home\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"12\":{\"10\":{\"type\":\"topCarousel\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog as if it were our own\",\"coverpPcture\":[\"7\"]}}}}}', '2018-04-02 15:22:42', '2018-04-02 15:22:42', '1fb50fbd-66c4-427d-8aea-9efc38de6820'),
  (5, 9, 3, 1, 'en', 2, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"home\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"12\":{\"10\":{\"type\":\"topCarousel\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverpPcture\":[\"7\"]}}}}}', '2018-04-02 15:23:29', '2018-04-02 15:23:29', '2f246482-3735-453d-b436-c34b4271c8a9'),
  (6, 11, 3, 1, 'en', 1, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683137,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:32:17', '2018-04-02 15:32:17', 'e3a004b3-20bc-46c7-9523-cdfaaf801976'),
  (7, 12, 3, 1, 'en', 1, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683185,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:33:05', '2018-04-02 15:33:05', 'bcf35870-934e-4aba-bd87-449d5f929c66'),
  (8, 13, 3, 1, 'en', 1, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Facilities\",\"slug\":\"facilities\",\"postDate\":1522683204,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"12\":[]}}', '2018-04-02 15:33:24', '2018-04-02 15:33:24', 'fb7d4ace-bbdb-440f-90ed-ca844d701a08'),
  (9, 13, 3, 1, 'en', 2, '', '{\"typeId\":\"6\",\"authorId\":\"1\",\"title\":\"Facilities\",\"slug\":\"facilities\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:33:34', '2018-04-02 15:33:34', '569f4a94-1b72-4110-8035-0de126496e01'),
  (10, 14, 3, 1, 'en', 1, '', '{\"typeId\":\"7\",\"authorId\":\"1\",\"title\":\"Training\",\"slug\":\"training\",\"postDate\":1522683248,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:34:08', '2018-04-02 15:34:08', 'cb10288b-1c77-43a0-bab1-5ff7a80a9547'),
  (11, 15, 3, 1, 'en', 1, '', '{\"typeId\":\"8\",\"authorId\":\"1\",\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1522683262,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:34:22', '2018-04-02 15:34:22', 'e66ca676-0cde-4e44-8d88-caed3f33ffc7'),
  (12, 16, 3, 1, 'en', 1, '', '{\"typeId\":\"9\",\"authorId\":\"1\",\"title\":\"Pupparazzi\",\"slug\":\"pupparazzi\",\"postDate\":1522683279,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:34:39', '2018-04-02 15:34:39', '984f3e74-ea3e-445e-ad5c-1879eda6ca7c'),
  (13, 17, 3, 1, 'en', 1, '', '{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1522683297,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:34:57', '2018-04-02 15:34:57', 'a7057c73-58a5-4998-896f-fac577e5990a'),
  (14, 15, 3, 1, 'en', 2, '', '{\"typeId\":\"8\",\"authorId\":\"1\",\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"\"}}', '2018-04-02 15:53:07', '2018-04-02 15:53:07', 'b3fa4050-bb7d-4090-81a3-4aeabd199b45'),
  (15, 17, 3, 1, 'en', 2, '', '{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":0,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:53:30', '2018-04-02 15:53:30', 'b8ae9b1a-75be-4230-855f-fa738da9fa28'),
  (16, 17, 3, 1, 'en', 3, '', '{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:53:53', '2018-04-02 15:53:53', 'db484f95-cf9f-4774-b3da-8d5007dd75c7'),
  (17, 15, 3, 1, 'en', 3, '', '{\"typeId\":\"8\",\"authorId\":\"1\",\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\"}}', '2018-04-02 15:54:02', '2018-04-02 15:54:02', 'bd0e1073-4019-419e-8567-fff06bd55d0f'),
  (18, 11, 3, 1, 'en', 2, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"1\"}}', '2018-04-02 15:59:28', '2018-04-02 15:59:28', '425239f0-0050-4398-970e-08f985dbec5a'),
  (19, 12, 3, 1, 'en', 2, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"2\"}}', '2018-04-02 15:59:36', '2018-04-02 15:59:36', 'fcde5da8-8263-45c1-9292-846f3d8a96b3'),
  (20, 13, 3, 1, 'en', 3, '', '{\"typeId\":\"6\",\"authorId\":\"1\",\"title\":\"Facilities\",\"slug\":\"facilities\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"3\"}}', '2018-04-02 16:01:13', '2018-04-02 16:01:13', 'd97f7a86-8732-4b75-a421-76249848bf50'),
  (21, 14, 3, 1, 'en', 2, '', '{\"typeId\":\"7\",\"authorId\":\"1\",\"title\":\"Training\",\"slug\":\"training\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"4\"}}', '2018-04-02 16:01:26', '2018-04-02 16:01:26', '02065b09-381d-47bb-a252-26b2062b8c66'),
  (22, 15, 3, 1, 'en', 4, '', '{\"typeId\":\"8\",\"authorId\":\"1\",\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"5\"}}', '2018-04-02 16:01:33', '2018-04-02 16:01:33', '3d16bcfd-38b2-4ae2-99d9-ebc2ee3de1e8'),
  (23, 16, 3, 1, 'en', 2, '', '{\"typeId\":\"9\",\"authorId\":\"1\",\"title\":\"Pupparazzi\",\"slug\":\"pupparazzi\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"6\"}}', '2018-04-02 16:01:39', '2018-04-02 16:01:39', '20d89dcc-3314-4f60-9eea-5250538729ab'),
  (24, 17, 3, 1, 'en', 4, '', '{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"7\"}}', '2018-04-02 16:01:49', '2018-04-02 16:01:49', '9cc5b97a-1f7c-4206-bc98-b15cf054e75e'),
  (25, 11, 3, 1, 'en', 3, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"service\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"1\"}}', '2018-04-03 08:37:52', '2018-04-03 08:37:52', 'ad30afa9-fccb-4771-881b-41f74d16d368'),
  (26, 9, 3, 1, 'en', 3, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"0\",\"12\":{\"10\":{\"type\":\"topCarousel\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverpPcture\":[\"7\"]}}}}}', '2018-04-03 08:38:13', '2018-04-03 08:38:13', '27c47eb0-1318-4fa7-bc50-2e92cb100c6d'),
  (27, 11, 3, 1, 'en', 4, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"11\":\"1\",\"15\":\"1\"}}', '2018-04-03 09:38:24', '2018-04-03 09:38:24', 'c557ea7f-2097-4783-b5ec-ac39e24ae99e'),
  (28, 14, 3, 1, 'en', 3, '', '{\"typeId\":\"7\",\"authorId\":\"1\",\"title\":\"Training\",\"slug\":\"training\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"16\":\"1\",\"11\":\"1\",\"15\":\"4\"}}', '2018-04-03 11:34:23', '2018-04-03 11:34:23', '8db2dc06-04ef-4a8a-b146-f2da0410d529'),
  (29, 11, 3, 1, 'en', 5, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"16\":\"1\",\"11\":\"1\",\"15\":\"1\"}}', '2018-04-03 11:34:29', '2018-04-03 11:34:29', '0d44aff3-0f68-4c24-8734-8af03b96119d'),
  (30, 16, 3, 1, 'en', 3, '', '{\"typeId\":\"9\",\"authorId\":\"1\",\"title\":\"Pupparazzi\",\"slug\":\"pupparazzi\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"16\":\"1\",\"11\":\"1\",\"15\":\"6\"}}', '2018-04-03 11:34:34', '2018-04-03 11:34:34', '71154874-9375-4a34-a2cf-e1107c6396ee'),
  (31, 13, 3, 1, 'en', 4, '', '{\"typeId\":\"6\",\"authorId\":\"1\",\"title\":\"Facilities\",\"slug\":\"facilities\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"16\":\"1\",\"11\":\"1\",\"15\":\"3\"}}', '2018-04-03 11:34:42', '2018-04-03 11:34:42', 'c8053e89-11ab-4bdc-a583-58623431e476'),
  (32, 15, 3, 1, 'en', 5, '', '{\"typeId\":\"8\",\"authorId\":\"1\",\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"16\":\"1\",\"11\":\"1\",\"15\":\"5\"}}', '2018-04-03 11:34:48', '2018-04-03 11:34:48', '80e9f620-c860-45c9-abe5-59d9dd7ed38d'),
  (33, 17, 3, 1, 'en', 5, '', '{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"16\":\"1\",\"11\":\"1\",\"15\":\"7\"}}', '2018-04-03 11:35:04', '2018-04-03 11:35:04', 'c71d83f4-8d0d-4127-b2ab-c6d60aa90d69'),
  (34, 11, 3, 1, 'en', 6, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"1\",\"15\":\"1\",\"16\":\"1\",\"11\":\"1\"}}', '2018-04-03 12:41:34', '2018-04-03 12:41:34', 'c4925fd4-b775-4f7b-ba54-abbf368ba267'),
  (35, 13, 3, 1, 'en', 5, '', '{\"typeId\":\"6\",\"authorId\":\"1\",\"title\":\"Facilities\",\"slug\":\"facilities\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"2\",\"15\":\"3\",\"16\":\"1\",\"11\":\"1\"}}', '2018-04-03 12:41:45', '2018-04-03 12:41:45', 'ea05baa0-6b4f-4de6-bb24-0f12c0485469'),
  (36, 15, 3, 1, 'en', 6, '', '{\"typeId\":\"8\",\"authorId\":\"1\",\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"3\",\"15\":\"5\",\"16\":\"1\",\"11\":\"1\"}}', '2018-04-03 12:41:53', '2018-04-03 12:41:53', '372c9903-c721-4d00-a745-d1d1d1789e24'),
  (37, 16, 3, 1, 'en', 4, '', '{\"typeId\":\"9\",\"authorId\":\"1\",\"title\":\"Pupparazzi\",\"slug\":\"pupparazzi\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"4\",\"15\":\"6\",\"16\":\"1\",\"11\":\"1\"}}', '2018-04-03 12:42:01', '2018-04-03 12:42:01', '3070ddc3-a4d3-42fc-82ab-b47f7de53884'),
  (38, 14, 3, 1, 'en', 4, '', '{\"typeId\":\"7\",\"authorId\":\"1\",\"title\":\"Training\",\"slug\":\"training\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"5\",\"15\":\"4\",\"16\":\"1\",\"11\":\"1\"}}', '2018-04-03 12:42:10', '2018-04-03 12:42:10', '273ff863-cd5c-4148-903c-e7372c3abdb5'),
  (39, 17, 3, 1, 'en', 6, '', '{\"typeId\":\"10\",\"authorId\":\"1\",\"title\":\"Contact Us\",\"slug\":\"contact-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"6\",\"15\":\"7\",\"16\":\"1\",\"11\":\"1\"}}', '2018-04-03 12:42:25', '2018-04-03 12:42:25', 'f2ba4d0f-8aad-4068-8654-86ab30fcba41'),
  (40, 18, 3, 1, 'en', 1, '', '{\"typeId\":\"11\",\"authorId\":\"1\",\"title\":\"Privacy Policy\",\"slug\":\"privacy-policy\",\"postDate\":1522759609,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"7\",\"15\":\"0\",\"16\":\"1\",\"11\":\"\"}}', '2018-04-03 12:46:49', '2018-04-03 12:46:49', '218cde1e-6f6b-45f1-8594-cbfa65b100ca'),
  (41, 19, 3, 1, 'en', 1, '', '{\"typeId\":\"11\",\"authorId\":\"1\",\"title\":\"Terms & Conditions\",\"slug\":\"terms-conditions\",\"postDate\":1522759648,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"8\",\"15\":\"0\",\"16\":\"1\",\"11\":\"\"}}', '2018-04-03 12:47:28', '2018-04-03 12:47:28', '831a8a0a-efd5-4d45-a40a-5fafbdce1a4f'),
  (42, 20, 3, 1, 'en', 1, '', '{\"typeId\":\"11\",\"authorId\":\"1\",\"title\":\"Cookies\",\"slug\":\"cookies\",\"postDate\":1522759670,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"9\",\"15\":\"0\",\"16\":\"\",\"11\":\"\"}}', '2018-04-03 12:47:50', '2018-04-03 12:47:50', '619d8905-4514-4848-9bd5-1565cc55ab4b'),
  (43, 21, 3, 1, 'en', 1, '', '{\"typeId\":\"11\",\"authorId\":\"1\",\"title\":\"VAT registration: 264563488\",\"slug\":\"vat-registration-264563488\",\"postDate\":1522759706,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"10\",\"15\":\"0\",\"16\":\"1\",\"11\":\"\"}}', '2018-04-03 12:48:26', '2018-04-03 12:48:26', '7a2f9628-ed14-4af0-8816-0f6bcdcf9354'),
  (44, 26, 4, 1, 'en', 1, '', '{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Twitter\",\"slug\":\"twitter\",\"postDate\":1522768020,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"19\":[\"24\"],\"18\":\"https:\\/\\/twitter.com\\/\"}}', '2018-04-03 15:08:30', '2018-04-03 15:08:30', '07953874-8e1e-4b8b-97cf-abeee6699ac8'),
  (45, 27, 4, 1, 'en', 1, '', '{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Facebook\",\"slug\":\"facebook\",\"postDate\":1522768139,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"19\":[\"22\"],\"18\":\"https:\\/\\/facebook.com\\/\"}}', '2018-04-03 15:08:59', '2018-04-03 15:08:59', '8ddd9b76-176b-4607-8da2-b318702a2266'),
  (46, 28, 4, 1, 'en', 1, '', '{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Youtube\",\"slug\":\"youtube\",\"postDate\":1522768159,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"19\":[\"25\"],\"18\":\"https:\\/\\/youtube.com\\/\"}}', '2018-04-03 15:09:19', '2018-04-03 15:09:19', 'd75c7c4f-68e9-45d2-a7b0-fbba6ec0ffe2'),
  (47, 29, 4, 1, 'en', 1, '', '{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Instagram\",\"slug\":\"instagram\",\"postDate\":1522768173,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"19\":[\"23\"],\"18\":\"https:\\/\\/instagram.com\\/\"}}', '2018-04-03 15:09:34', '2018-04-03 15:09:34', 'a69424d4-578a-4094-ac94-f70b3f1b20d2'),
  (48, 9, 3, 1, 'en', 4, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"topCarousel\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverpPcture\":[\"31\"]}},\"32\":{\"type\":\"topCarousel\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverpPcture\":[\"30\"]}}}}}', '2018-04-03 16:32:55', '2018-04-03 16:32:55', '359279a8-9962-40b1-94f5-2fa5acaa8a12'),
  (49, 9, 3, 1, 'en', 5, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-04 08:48:32', '2018-04-04 08:48:32', '7ae2aed6-c304-494a-9099-98fedb9939c8'),
  (50, 9, 3, 1, 'en', 6, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-04 08:49:07', '2018-04-04 08:49:07', '5398a0f5-308f-4622-9aa2-3b60ee02e5e1'),
  (51, 9, 3, 1, 'en', 7, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-04 11:01:27', '2018-04-04 11:01:27', '6fc1f244-199c-46c9-ac97-1b40ce0190e7'),
  (52, 9, 3, 1, 'en', 8, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-04 11:01:59', '2018-04-04 11:01:59', 'c0835b32-9ea0-471c-a9e6-650f664a3dd7'),
  (53, 9, 3, 1, 'en', 9, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-04 11:27:09', '2018-04-04 11:27:09', '55865b76-f3bb-48f9-9bac-d8fa8411330c'),
  (54, 9, 3, 1, 'en', 10, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-04 11:28:18', '2018-04-04 11:28:18', 'b77d43bf-7e85-439d-8fac-2e03fc21a090'),
  (55, 9, 3, 1, 'en', 11, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 10:19:28', '2018-04-05 10:19:28', '0dcb0732-1e51-4ed5-b520-93d136a80e48'),
  (56, 9, 3, 1, 'en', 12, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 10:37:37', '2018-04-05 10:37:37', '5d0c69e1-6abb-46fa-9244-49c0b5cf33ed'),
  (57, 9, 3, 1, 'en', 13, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 10:40:02', '2018-04-05 10:40:02', 'c2274098-5d7c-4ca1-b7bf-61b2e7ad3257'),
  (58, 9, 3, 1, 'en', 14, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form asdf asd f\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 11:09:25', '2018-04-05 11:09:25', '8ccd79c7-e0a5-477c-89a5-a33bc10d9b70'),
  (59, 9, 3, 1, 'en', 15, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 11:09:46', '2018-04-05 11:09:46', 'f81d04e0-fd3c-4ed8-99e5-057a98ad464d'),
  (60, 9, 3, 1, 'en', 16, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 11:12:19', '2018-04-05 11:12:19', 'd14a0bea-cb9a-42fe-a0fc-90b73b8de1e0'),
  (61, 9, 3, 1, 'en', 17, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 11:12:35', '2018-04-05 11:12:35', 'c763d5d0-ad9e-46b7-86af-06c537e7fde4'),
  (62, 9, 3, 1, 'en', 18, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"37\":{\"43\":{\"type\":\"servicesBlock\",\"enabled\":\"1\",\"fields\":{\"blockTitle\":\"Your best friend deserves the best care.\",\"caption\":\"At Bark n\\u2019 Fly we specialise in giving dogs more. Established in 2009 we provide premium dog boarding and dog day care throughout Edinburgh & the surrounding area.\",\"backgroundColor\":\"#6cb55a\"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 11:34:09', '2018-04-05 11:34:09', '1bd525a1-49a8-47e8-a986-5cd6dec2dbf5');
INSERT INTO `craft_entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (63, 9, 3, 1, 'en', 19, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"37\":{\"43\":{\"type\":\"servicesBlock\",\"enabled\":\"1\",\"fields\":{\"blockTitle\":\"Your best friend deserves the best care.\",\"caption\":\"At Bark n\\u2019 Fly we specialise in giving dogs more. Established in 2009 we provide premium dog boarding and dog day care throughout Edinburgh & the surrounding area.\",\"backgroundColor\":\"#6cb55a\"}},\"44\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"40\"],\"caption\":\"Pick ups and Drop Offs\",\"text\":\"We can safely and securely collect or return your furry friend. We even have a handy SMS service, notifying you directly to your mobile phone about pick ups or drop offs if you\\u2019re away from home.\"}},\"45\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":\"\",\"caption\":\"Spacious & Safe Playareas\",\"text\":\"Under the watchful eye of our experienced, fully insured and police vetted staff, your dog will love running around in our two outdoor exercise areas and dry indoor arena, keeping paws busy and tails wagging!\"}},\"46\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"41\"],\"caption\":\"Dog Training\",\"text\":\"Our dog training service is there to help with any problems you might be experiencing with your dog. From every day obedience to behaviour issues, all the way to competition training and other specialised types of work.\"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 11:35:39', '2018-04-05 11:35:39', '24fe553d-f784-467e-991c-badcc233008e'),
  (64, 9, 3, 1, 'en', 20, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"37\":{\"43\":{\"type\":\"servicesBlock\",\"enabled\":\"1\",\"fields\":{\"blockTitle\":\"Your best friend deserves the best care\",\"caption\":\"At Bark n\\u2019 Fly we specialise in giving dogs more. Established in 2009 we provide premium dog boarding and dog day care throughout Edinburgh & the surrounding area.\",\"backgroundColor\":\"#6cb55a\"}},\"44\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"40\"],\"caption\":\"Pick ups and Drop Offs\",\"text\":\"We can safely and securely collect or return your furry friend. We even have a handy SMS service, notifying you directly to your mobile phone about pick ups or drop offs if you\\u2019re away from home.\"}},\"45\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":\"\",\"caption\":\"Spacious & Safe Playareas\",\"text\":\"Under the watchful eye of our experienced, fully insured and police vetted staff, your dog will love running around in our two outdoor exercise areas and dry indoor arena, keeping paws busy and tails wagging!\"}},\"46\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"41\"],\"caption\":\"Dog Training\",\"text\":\"Our dog training service is there to help with any problems you might be experiencing with your dog. From every day obedience to behaviour issues, all the way to competition training and other specialised types of work.\"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 11:40:56', '2018-04-05 11:40:56', 'f8db661c-d3c8-4ed2-a5bf-1fe500be789f'),
  (65, 9, 3, 1, 'en', 21, '', '{\"typeId\":\"3\",\"authorId\":\"1\",\"title\":\"Home\",\"slug\":\"homepage\",\"postDate\":1522682520,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"39\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"20\":{\"33\":{\"type\":\"panel\",\"enabled\":\"1\",\"fields\":{\"topCaption\":\"Start your booking\",\"tab1Title\":\"Dog Boarding\",\"tab1Input1Caption\":\"Start\",\"tab1Input2Caption\":\"End\",\"tab1SubmitButtonCaption\":\"Book now\",\"tab2Title\":\"Dog Day Care \"}}},\"37\":{\"43\":{\"type\":\"servicesBlock\",\"enabled\":\"1\",\"fields\":{\"blockTitle\":\"Your best friend deserves the best care.\",\"caption\":\"At Bark n\\u2019 Fly we specialise in giving dogs more. Established in 2009 we provide premium dog boarding and dog day care throughout Edinburgh & the surrounding area.\",\"backgroundColor\":\"#6cb55a\"}},\"44\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"40\"],\"caption\":\"Pick ups and Drop Offs\",\"text\":\"We can safely and securely collect or return your furry friend. We even have a handy SMS service, notifying you directly to your mobile phone about pick ups or drop offs if you\\u2019re away from home.\"}},\"45\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":\"\",\"caption\":\"Spacious & Safe Playareas\",\"text\":\"Under the watchful eye of our experienced, fully insured and police vetted staff, your dog will love running around in our two outdoor exercise areas and dry indoor arena, keeping paws busy and tails wagging!\"}},\"46\":{\"type\":\"serviceItem\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"41\"],\"caption\":\"Dog Training\",\"text\":\"Our dog training service is there to help with any problems you might be experiencing with your dog. From every day obedience to behaviour issues, all the way to competition training and other specialised types of work.\"}}},\"27\":{\"35\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\\r\\n\\r\\n                            \",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"36\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"37\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}},\"17\":\"0\",\"15\":\"0\",\"16\":\"\",\"11\":\"1\",\"12\":{\"10\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"31\"]}},\"32\":{\"type\":\"carouselBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"At Bark n\\u2019 Fly, we treat your dog\\r\\nas if it were our own\",\"coverPicture\":[\"30\"]}}}}}', '2018-04-05 11:41:21', '2018-04-05 11:41:21', 'a73cd2af-02cf-411e-a8e8-94a321d58beb'),
  (66, 11, 3, 1, 'en', 7, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"44\":[\"51\"],\"17\":\"1\",\"15\":\"1\",\"45\":{\"52\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"callUsTodayCaption\":\"Call us today: 01234 567 890\"}}},\"16\":\"1\",\"11\":\"1\"}}', '2018-04-06 08:10:45', '2018-04-06 08:10:45', '6e0c3783-a9a3-44f7-8110-3781ec7591dc'),
  (67, 11, 3, 1, 'en', 8, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"1\",\"15\":\"1\",\"44\":{\"53\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Our Services\",\"picture\":[\"51\"]}}},\"45\":{\"52\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"callUsTodayCaption\":\"Call us today: 01234 567 890\"}}},\"16\":\"1\",\"11\":\"1\"}}', '2018-04-06 08:26:49', '2018-04-06 08:26:49', 'b2151442-3378-411a-965f-2db702ab3446'),
  (68, 11, 3, 1, 'en', 9, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"1\",\"15\":\"1\",\"44\":{\"53\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Our Services\",\"picture\":[\"51\"]}}},\"45\":{\"52\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"callUsTodayCaption\":\"Call us today: 01234 567 890\"}},\"54\":{\"type\":\"serviceBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"47\"],\"caption\":\"Dog Day Care\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.\"}},\"55\":{\"type\":\"serviceBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"48\"],\"caption\":\"Dog Boarding\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.\"}},\"56\":{\"type\":\"serviceBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"49\"],\"caption\":\"Pick up & Drop off\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.\"}},\"57\":{\"type\":\"serviceBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"50\"],\"caption\":\"Dog Training\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.\"}}},\"16\":\"1\",\"11\":\"1\"}}', '2018-04-06 08:55:23', '2018-04-06 08:55:23', '043a8054-ae8f-44ec-8b65-85b0573d22a4'),
  (69, 11, 3, 1, 'en', 10, '', '{\"typeId\":\"4\",\"authorId\":\"1\",\"title\":\"Services\",\"slug\":\"services\",\"postDate\":1522683120,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"32\":{\"58\":{\"type\":\"adBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"38\"],\"caption\":\"Call us today: 01234 567 890\",\"buttonText\":\"Order form\",\"targetUrl\":\"https:\\/\\/google.com\"}}},\"17\":\"1\",\"15\":\"1\",\"44\":{\"53\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Our Services\",\"picture\":[\"51\"]}}},\"45\":{\"52\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"callUsTodayCaption\":\"Call us today: 01234 567 890\"}},\"54\":{\"type\":\"serviceBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"47\"],\"caption\":\"Dog Day Care\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.\"}},\"55\":{\"type\":\"serviceBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"48\"],\"caption\":\"Dog Boarding\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.\"}},\"56\":{\"type\":\"serviceBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"49\"],\"caption\":\"Pick up & Drop off\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.\"}},\"57\":{\"type\":\"serviceBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"50\"],\"caption\":\"Dog Training\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.\"}}},\"16\":\"1\",\"11\":\"1\"}}', '2018-04-06 09:42:52', '2018-04-06 09:42:52', '1ff4d722-e319-420c-bc79-416a3276963f'),
  (70, 13, 3, 1, 'en', 6, '', '{\"typeId\":\"6\",\"authorId\":\"1\",\"title\":\"Facilities\",\"slug\":\"facilities\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"52\":{\"88\":{\"type\":\"facilityBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Little Dog Pen\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.\",\"images\":[\"69\",\"70\",\"71\"]}},\"89\":{\"type\":\"facilityBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Big Dog Pen\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.\",\"images\":[\"72\",\"73\",\"74\"]}},\"90\":{\"type\":\"facilityBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Indoor Arena\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.\",\"images\":[\"75\",\"76\",\"77\"]}},\"91\":{\"type\":\"facilityBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Little Dog Field\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.\",\"images\":[\"78\",\"79\",\"80\"]}},\"92\":{\"type\":\"facilityBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Big Dog Field\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.\",\"images\":[\"81\",\"82\",\"83\"]}},\"93\":{\"type\":\"facilityBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Boarding Kennel\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\\r\\n\\r\\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.\",\"images\":[\"84\",\"85\",\"86\"]}}},\"17\":\"2\",\"15\":\"3\",\"44\":{\"87\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Our Facilities\",\"picture\":[\"60\"]}}},\"16\":\"1\",\"11\":\"1\"}}', '2018-04-06 12:37:18', '2018-04-06 12:37:18', '6fcaefc9-6a87-4267-9294-fcd7b1bf2eae'),
  (71, 14, 3, 1, 'en', 5, '', '{\"typeId\":\"7\",\"authorId\":\"1\",\"title\":\"Training\",\"slug\":\"training\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"5\",\"15\":\"4\",\"44\":{\"94\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Training\",\"picture\":[\"63\"]}}},\"16\":\"1\",\"11\":\"1\",\"56\":[]}}', '2018-04-09 11:04:30', '2018-04-09 11:04:30', '4532e923-d374-4fe9-8b89-184c36922bae'),
  (72, 14, 3, 1, 'en', 6, '', '{\"typeId\":\"7\",\"authorId\":\"1\",\"title\":\"Training\",\"slug\":\"training\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"5\",\"15\":\"4\",\"44\":{\"94\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Training\",\"picture\":[\"63\"]}}},\"16\":\"1\",\"11\":\"1\",\"56\":{\"100\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Dog Training, Behaviour Correction and Puppy Education\",\"text\":\"Fusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta leo sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex tincidunt nibhtempor at leo.\",\"video\":[\"95\"]},\"collapsed\":\"1\"},\"101\":{\"type\":\"trainingsblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Practical Obedience\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor ateget mi.\",\"picture\":[\"96\"]}},\"102\":{\"type\":\"trainingsblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Puppy Education\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor ateget mi.\",\"picture\":[\"97\"]}},\"103\":{\"type\":\"trainingsblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Behaviour Correction\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor ateget mi.\",\"picture\":[\"98\"]}},\"104\":{\"type\":\"trainingsblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Activity Based\",\"text\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor ateget mi.\",\"picture\":[\"99\"]}}}}}', '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'd02543a4-6b25-4308-aac3-d960583aab95'),
  (73, 105, 5, 1, 'en', 1, '', '{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Feedback List\",\"slug\":\"feedback-list\",\"postDate\":1523363487,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"27\":{\"106\":{\"type\":\"feedbackBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"107\":{\"type\":\"feedbackBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Dexter\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"108\":{\"type\":\"feedbackBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Robin\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}}}}', '2018-04-10 12:31:27', '2018-04-10 12:31:27', '7fb7db44-b92c-45eb-bf0e-a360eaa8a375'),
  (74, 15, 3, 1, 'en', 7, '', '{\"typeId\":\"8\",\"authorId\":\"1\",\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"3\",\"67\":[],\"15\":\"5\",\"44\":{\"109\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"About Us\",\"picture\":[\"59\"]}}},\"16\":\"1\",\"11\":\"1\",\"63\":[]}}', '2018-04-10 14:48:46', '2018-04-10 14:48:46', '44a54d7d-8b07-47a5-a28e-156afa50f403'),
  (75, 15, 3, 1, 'en', 8, '', '{\"typeId\":\"8\",\"authorId\":\"1\",\"title\":\"About Us\",\"slug\":\"about-us\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"3\",\"67\":{\"112\":{\"type\":\"mapblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Servicing the Nation\"}},\"113\":{\"type\":\"mapblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pick up & Drop off Locations\"}}},\"15\":\"5\",\"44\":{\"109\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"About Us\",\"picture\":[\"59\"]}}},\"16\":\"1\",\"11\":\"1\",\"63\":{\"111\":{\"type\":\"videoBlock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Bark n\\u2019 Fly\",\"text\":\"Fusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta leo sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex tincidunt nibhtempor at leo.\",\"video\":[\"110\"]}}}}}', '2018-04-10 14:59:28', '2018-04-10 14:59:28', '8e580acf-fe6a-4a5f-af37-4f6dc3df9055'),
  (76, 105, 5, 1, 'en', 2, '', '{\"typeId\":\"13\",\"authorId\":\"1\",\"title\":\"Feedback List\",\"slug\":\"feedback-list\",\"postDate\":1523363460,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"27\":{\"106\":{\"type\":\"feedbackBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Eddie\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"107\":{\"type\":\"feedbackBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"Dexter\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}},\"108\":{\"type\":\"feedbackBlock\",\"enabled\":\"1\",\"fields\":{\"picture\":[\"34\"],\"text\":\"I love coming to Bark n\\u2019 Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I\\u2019m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I\\u2019m ready for cuddles and belly rubs!\",\"author\":\"George\",\"signature\":\"Border Collie & Day Care Guest.\\r\\nHuman friend: Frances\"}}}}}', '2018-04-11 16:53:59', '2018-04-11 16:53:59', 'e376194b-d924-4b70-bc37-20d58cc20ac8'),
  (77, 12, 3, 1, 'en', 3, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":[],\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"69\":[],\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 09:14:17', '2018-04-16 09:14:17', '30cf2512-7a24-465f-a37c-d2edae5bac28'),
  (78, 12, 3, 1, 'en', 4, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":[],\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"69\":{\"115\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Day Care\",\"endingText\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n                                    <p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for details.<\\/p>\\r\\n\\r\\n                                    <p><b>Please note:<\\/b> Dogmore does not charge for days or periods when your dog is not using our services, including last minute cancellations.<\\/p>\"}},\"116\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"priceCommentText\":\"\"}},\"117\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day for two dogs (same household)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}}},\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 09:20:08', '2018-04-16 09:20:08', '61e0f08e-17d9-4e8c-8f3a-86feabf41035'),
  (79, 12, 3, 1, 'en', 5, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":[],\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"69\":{\"115\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Day Care\",\"endingText\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n<p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for details.<\\/p>\\r\\n<p><strong>Please note:<\\/strong> Dogmore does not charge for days or periods when your dog is not using our services, including last minute cancellations.<\\/p>\"}},\"116\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"priceCommentText\":\"\"}},\"117\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"118\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"priceCommentText\":\"\"}},\"119\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"120\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"doubleSizedBlock\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"priceCommentText\":\"(\\u00a31.50 for each additional dog)\"}}},\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 09:24:34', '2018-04-16 09:24:34', 'e14657c8-7107-47aa-9ac3-c1c490275146'),
  (80, 12, 3, 1, 'en', 6, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":{\"124\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":\"\",\"caption\":\"Download our Pricing\",\"subcaption\":\"\",\"targetLink\":\"http:\\/\\/www.pdf995.com\\/samples\\/pdf.pdf\"}},\"125\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":\"\",\"caption\":\"\",\"subcaption\":\"\",\"targetLink\":\"\"}}},\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"69\":{\"115\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Day Care\",\"endingText\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n<p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for details.<\\/p>\\r\\n<p><strong>Please note:<\\/strong> Dogmore does not charge for days or periods when your dog is not using our services, including last minute cancellations.<\\/p>\"}},\"116\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"priceCommentText\":\"\"}},\"117\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"118\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"priceCommentText\":\"\"}},\"119\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"120\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"doubleSizedBlock\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"priceCommentText\":\"(\\u00a31.50 for each additional dog)\"}}},\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 12:08:50', '2018-04-16 12:08:50', '8e23723b-301f-4e50-9913-6aa90b8b53a4'),
  (81, 12, 3, 1, 'en', 7, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":{\"124\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"121\"],\"caption\":\"Download our Pricing\",\"subcaption\":\"\",\"targetLink\":\"http:\\/\\/www.pdf995.com\\/samples\\/pdf.pdf\",\"buttonCaption\":\"Download\"}},\"125\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":\"\",\"caption\":\"\",\"subcaption\":\"\",\"targetLink\":\"\",\"buttonCaption\":\"\"}}},\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"69\":{\"115\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Day Care\",\"endingText\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n<p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for details.<\\/p>\\r\\n<p><strong>Please note:<\\/strong> Dogmore does not charge for days or periods when your dog is not using our services, including last minute cancellations.<\\/p>\"}},\"116\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"priceCommentText\":\"\"}},\"117\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"118\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"priceCommentText\":\"\"}},\"119\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"120\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"doubleSizedBlock\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"priceCommentText\":\"(\\u00a31.50 for each additional dog)\"}}},\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 12:12:01', '2018-04-16 12:12:01', '0f77226b-f365-4dc3-adab-9551cfa305e7');
INSERT INTO `craft_entryversions` (`id`, `entryId`, `sectionId`, `creatorId`, `locale`, `num`, `notes`, `data`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (82, 12, 3, 1, 'en', 8, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":{\"124\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"121\"],\"caption\":\"Download our Pricing\",\"subcaption\":\"\",\"targetLink\":\"http:\\/\\/www.pdf995.com\\/samples\\/pdf.pdf\",\"buttonCaption\":\"Download\",\"size\":\"fullSize\"}},\"125\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"122\"],\"caption\":\"Dog Boarding\",\"subcaption\":\"01234 567 890\",\"targetLink\":\"https:\\/\\/google.com\",\"buttonCaption\":\"Learn more\",\"size\":\"halfSize\"}},\"126\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":\"\",\"caption\":\"Dog Taxi\",\"subcaption\":\"01234 567 890\",\"targetLink\":\"https:\\/\\/google.com\",\"buttonCaption\":\"Learn more\",\"size\":\"halfSize\"}}},\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"69\":{\"115\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Day Care\",\"endingText\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n<p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for details.<\\/p>\\r\\n<p><strong>Please note:<\\/strong> Dogmore does not charge for days or periods when your dog is not using our services, including last minute cancellations.<\\/p>\"}},\"116\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"priceCommentText\":\"\"}},\"117\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"118\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"priceCommentText\":\"\"}},\"119\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"120\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"doubleSizedBlock\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"priceCommentText\":\"(\\u00a31.50 for each additional dog)\"}}},\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 12:13:43', '2018-04-16 12:13:43', '94d66bec-d79b-48e5-9863-cdca25dae120'),
  (83, 12, 3, 1, 'en', 9, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":{\"124\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"121\"],\"caption\":\"Download our Pricing\",\"subcaption\":\"\",\"targetLink\":\"http:\\/\\/www.pdf995.com\\/samples\\/pdf.pdf\",\"buttonCaption\":\"Download\",\"size\":\"fullSize\"}},\"125\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"122\"],\"caption\":\"Dog Boarding\",\"subcaption\":\"01234 567 890\",\"targetLink\":\"https:\\/\\/google.com\",\"buttonCaption\":\"Learn more\",\"size\":\"halfSize\"}},\"127\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":\"\",\"caption\":\"Dog Taxi\",\"subcaption\":\"01234 567 890\",\"targetLink\":\"https:\\/\\/google.com\",\"buttonCaption\":\"Learn more\",\"size\":\"halfSize\"}}},\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"69\":{\"115\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Day Care\",\"endingText\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n<p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for details.<\\/p>\\r\\n<p><strong>Please note:<\\/strong> Dogmore does not charge for days or periods when your dog is not using our services, including last minute cancellations.<\\/p>\"}},\"116\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"priceCommentText\":\"\"}},\"117\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"118\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"priceCommentText\":\"\"}},\"119\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"120\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"doubleSizedBlock\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"priceCommentText\":\"(\\u00a31.50 for each additional dog)\"}}},\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 12:13:44', '2018-04-16 12:13:44', 'efd27583-8386-4eb7-93fa-8c3371ba11f4'),
  (84, 12, 3, 1, 'en', 10, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":{\"124\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"121\"],\"caption\":\"Download our Pricing\",\"subcaption\":\"\",\"targetLink\":\"http:\\/\\/www.pdf995.com\\/samples\\/pdf.pdf\",\"buttonCaption\":\"Download\",\"size\":\"fullSize\"}},\"125\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"122\"],\"caption\":\"Dog Boarding\",\"subcaption\":\"01234 567 890\",\"targetLink\":\"https:\\/\\/google.com\",\"buttonCaption\":\"Learn more\",\"size\":\"halfSize\"}},\"127\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"123\"],\"caption\":\"Dog Taxi\",\"subcaption\":\"01234 567 890\",\"targetLink\":\"https:\\/\\/google.com\",\"buttonCaption\":\"Learn more\",\"size\":\"halfSize\"}}},\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"69\":{\"115\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Day Care\",\"endingText\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n<p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for details.<\\/p>\\r\\n<p><strong>Please note:<\\/strong> Dogmore does not charge for days or periods when your dog is not using our services, including last minute cancellations.<\\/p>\"}},\"116\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"priceCommentText\":\"\"}},\"117\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"118\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"priceCommentText\":\"\"}},\"119\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"120\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"doubleSizedBlock\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"priceCommentText\":\"(\\u00a31.50 for each additional dog)\"}}},\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 12:52:10', '2018-04-16 12:52:10', '900eefbd-8541-4f6c-980d-1a93fcae8898'),
  (85, 128, 6, 1, 'en', 1, '', '{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Day Care\",\"slug\":\"day-care\",\"postDate\":1523887351,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"69\":{\"129\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Day Care\",\"endingText\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n<p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for details.<\\/p>\\r\\n<p><strong>Please note:<\\/strong> Dogmore does not charge for days or periods when your dog is not using our services, including last minute cancellations.<\\/p>\"}},\"130\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"priceCommentText\":\"\"}},\"131\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"132\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"priceCommentText\":\"\"}},\"133\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a322.30\",\"priceCommentText\":\"\"}},\"134\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"doubleSizedBlock\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"priceCommentText\":\"(\\u00a31.50 for each additional dog)\"}}}}}', '2018-04-16 14:02:31', '2018-04-16 14:02:31', '76bfeb31-19d1-4756-9808-5537baee60dd'),
  (86, 135, 6, 1, 'en', 1, '', '{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Boarding Kennels\",\"slug\":\"boarding-kennels\",\"postDate\":1523887578,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"69\":{\"136\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Boarding Kennels\",\"endingText\":\"<p><b>Please note<\\/b>: Regardless of the time your dog is checked in, the charge of \\u00a326 would apply from the check in time until midday the next day. Collection after midday would incur an additional charge of half a day boarding (\\u00a318.50 for one dog).<\\/p>\"}},\"137\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Per night\",\"priceText\":\"\\u00a326.00\",\"priceCommentText\":\"\"}},\"138\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a337.00\",\"priceCommentText\":\"\"}},\"139\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"After mid-day collection\",\"priceText\":\"\\u00a318.50\",\"priceCommentText\":\"\"}}}}}', '2018-04-16 14:06:18', '2018-04-16 14:06:18', '43f9ae3a-b153-4ac0-858c-efc59fecd260'),
  (87, 140, 6, 1, 'en', 1, '', '{\"typeId\":null,\"authorId\":\"1\",\"title\":\"Dog Training, Behaviour Correction and Puppy Education\",\"slug\":\"dog-training-behaviour-correction-and-puppy-education\",\"postDate\":1523887677,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"69\":{\"141\":{\"type\":\"generalBlock\",\"enabled\":\"1\",\"fields\":{\"beginningCaption\":\"Dog Training, Behaviour Correction and Puppy Education\",\"endingText\":\"<p><b>Please note<\\/b>: We like to differentiate between performance and practical obedience. Performance obedience is suited for owners who like the idea of working with their dog and would like to engage in dog training as an activity, either competitively or not.<\\/p>\"}},\"142\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"One-on-one session\\r\\n(at centre)\",\"priceText\":\"\\u00a395.00\",\"priceCommentText\":\"\"}},\"143\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Subsequent session\\r\\n(client residence)\",\"priceText\":\"\\u00a365.00\",\"priceCommentText\":\"\"}},\"144\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"One-on-one session\\r\\n(client residence)\",\"priceText\":\"\\u00a3120.00\",\"priceCommentText\":\"\"}},\"145\":{\"type\":\"serviceItemBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"itemLayout\":\"singleSizedBlock\",\"caption\":\"Subsequent session\\r\\n(at centre)\",\"priceText\":\"\\u00a350.00\",\"priceCommentText\":\"(per 30 minutes)\"}}}}}', '2018-04-16 14:07:57', '2018-04-16 14:07:57', 'b5ee81d0-1b53-4285-a86e-62b1b636584b'),
  (88, 12, 3, 1, 'en', 11, '', '{\"typeId\":\"5\",\"authorId\":\"1\",\"title\":\"Pricing\",\"slug\":\"pricing\",\"postDate\":1522683180,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"76\":{\"124\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"121\"],\"size\":\"fullSize\",\"caption\":\"Download our Pricing\",\"subcaption\":\"\",\"targetLink\":\"http:\\/\\/www.pdf995.com\\/samples\\/pdf.pdf\",\"buttonCaption\":\"Download\"}},\"125\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"122\"],\"size\":\"halfSize\",\"caption\":\"Dog Boarding\",\"subcaption\":\"01234 567 890\",\"targetLink\":\"https:\\/\\/google.com\",\"buttonCaption\":\"Learn more\"}},\"127\":{\"type\":\"itemBlock\",\"enabled\":\"1\",\"fields\":{\"coverPicture\":[\"123\"],\"size\":\"halfSize\",\"caption\":\"Dog Taxi\",\"subcaption\":\"01234 567 890\",\"targetLink\":\"https:\\/\\/google.com\",\"buttonCaption\":\"Learn more\"}}},\"17\":\"0\",\"15\":\"2\",\"44\":{\"114\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pricing\",\"picture\":[\"61\"]}}},\"84\":[\"128\",\"135\",\"140\"],\"16\":\"\",\"11\":\"1\"}}', '2018-04-16 14:10:02', '2018-04-16 14:10:02', '33a83a9e-7c9f-4b2e-b11d-9f33415189ee'),
  (89, 128, 6, 1, 'en', 2, '', '{\"typeId\":\"14\",\"authorId\":\"1\",\"title\":\"Day Care\",\"slug\":\"day-care\",\"postDate\":1523887320,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"85\":{\"146\":{\"type\":\"mainBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}}}}}', '2018-04-16 15:18:32', '2018-04-16 15:18:32', 'a0af4a21-fa6d-41b5-b435-e785e12a9d51'),
  (90, 128, 6, 1, 'en', 3, '', '{\"typeId\":\"14\",\"authorId\":\"1\",\"title\":\"Day Care\",\"slug\":\"day-care\",\"postDate\":1523887320,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"85\":{\"146\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"147\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a320.30\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"148\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"149\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"150\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"subcaption\":\"(\\u00a31.50 for each additional dog)\",\"size\":\"doubleSized\"}}}}}', '2018-04-16 15:21:14', '2018-04-16 15:21:14', 'f0f976e8-8a1d-4a91-acc7-67b89b5c9119'),
  (91, 128, 6, 1, 'en', 4, '', '{\"typeId\":\"14\",\"authorId\":\"1\",\"title\":\"Day Care\",\"slug\":\"day-care\",\"postDate\":1523887320,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"91\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n                                    <p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for\\r\\n                                        details.<\\/p>\\r\\n\\r\\n                                    <p><b>Please note:<\\/b> Dogmore does not charge for days or periods when your dog is not using our services, including last\\r\\n                                        minute cancellations.<\\/p>\",\"85\":{\"146\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"147\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a320.30\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"148\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"149\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"150\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"subcaption\":\"(\\u00a31.50 for each additional dog)\",\"size\":\"doubleSized\"}}}}}', '2018-04-16 15:24:42', '2018-04-16 15:24:42', '3413fbfe-f266-403f-b4ea-e686c652c531'),
  (92, 135, 6, 1, 'en', 2, '', '{\"typeId\":\"14\",\"authorId\":\"1\",\"title\":\"Boarding Kennels\",\"slug\":\"boarding-kennels\",\"postDate\":1523887560,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"91\":\"<p><b>Please note<\\/b>: Regardless of the time your dog is checked in, the charge of \\u00a326 would apply from the check in time\\r\\n                            until midday the next day. Collection after midday would incur an additional charge of half a day boarding (\\u00a318.50 for one\\r\\n                            dog).<\\/p>\",\"85\":{\"151\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Per night\",\"priceText\":\"\\u00a326.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"152\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"caption\":\"Two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a337.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"153\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"After mid-day collection\",\"priceText\":\"\\u00a318.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}}}}}', '2018-04-16 15:26:48', '2018-04-16 15:26:48', 'c96c8c9b-6657-4426-8446-1766774ad0fb'),
  (93, 135, 6, 1, 'en', 3, '', '{\"typeId\":\"14\",\"authorId\":\"1\",\"title\":\"Boarding Kennels\",\"slug\":\"boarding-kennels\",\"postDate\":1523887560,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"91\":\"<p><b>Please note<\\/b>: Regardless of the time your dog is checked in, the charge of \\u00a326 would apply from the check in time\\r\\n                            until midday the next day. Collection after midday would incur an additional charge of half a day boarding (\\u00a318.50 for one\\r\\n                            dog).<\\/p>\",\"85\":{\"154\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Per night\",\"priceText\":\"\\u00a326.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"155\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"caption\":\"Two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a337.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"156\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"After mid-day collection\",\"priceText\":\"\\u00a318.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}}}}}', '2018-04-16 15:26:48', '2018-04-16 15:26:48', '2211018e-ef00-42d6-b5ce-b81246b9438c'),
  (94, 140, 6, 1, 'en', 2, '', '{\"typeId\":\"14\",\"authorId\":\"1\",\"title\":\"Dog Training, Behaviour Correction and Puppy Education\",\"slug\":\"dog-training-behaviour-correction-and-puppy-education\",\"postDate\":1523887620,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"91\":\"<p><b>Please note<\\/b>: We like to differentiate between performance and practical obedience. Performance obedience is suited for\\r\\n                        owners who like the idea of working with their dog and would like to engage in dog training as an activity, either\\r\\n                        competitively or not.<\\/p>\",\"85\":{\"157\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"One-on-one session\\r\\n(at centre).\",\"priceText\":\"\\u00a395.00\",\"subcaption\":\"\",\"size\":\"singleSized\"},\"collapsed\":\"1\"},\"158\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Subsequent session\\r\\n(client residence)\",\"priceText\":\"\\u00a365.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"159\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"One-on-one session\\r\\n(client residence)\",\"priceText\":\"\\u00a3120.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"160\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Subsequent session\\r\\n(at centre)\",\"priceText\":\"\\u00a350.00\",\"subcaption\":\"(per 30 minutes)\",\"size\":\"singleSized\"}}}}}', '2018-04-16 15:28:12', '2018-04-16 15:28:12', 'cbc3b556-17e4-45ba-b7d7-2b9a9432021d'),
  (95, 128, 6, 1, 'en', 5, '', '{\"typeId\":\"14\",\"authorId\":\"1\",\"title\":\"Day Care\",\"slug\":\"day-care\",\"postDate\":1523887320,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"91\":\"<div class=\\\"pricing__lead lead\\\">Rates include VAT<\\/div>\\r\\n<p>Note that day care and boarding pick up \\/ drop off service is restricted to certain areas. Please contact us for\\r\\n                                        details.<\\/p>\\r\\n<p><strong>Please note:<\\/strong> Dogmore does not charge for days or periods when your dog is not using our services, including last\\r\\n                                        minute cancellations.<\\/p>\",\"85\":{\"146\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day\",\"priceText\":\"\\u00a322.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"148\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day for two dogs\\r\\n(same household)\",\"priceText\":\"\\u00a322.30\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"149\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Full day\\r\\n(more than 15 days per month)\",\"priceText\":\"\\u00a337.50\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"147\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"caption\":\"Half day\\r\\n(5 hours or less)\",\"priceText\":\"\\u00a320.30\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"150\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Pick up & drop off\",\"priceText\":\"\\u00a33.00 per dog\",\"subcaption\":\"(\\u00a31.50 for each additional dog)\",\"size\":\"doubleSized\"}}}}}', '2018-04-16 16:08:23', '2018-04-16 16:08:23', 'fa13fa70-1783-4dda-9c5a-ac8229140ae1'),
  (96, 140, 6, 1, 'en', 3, NULL, '{\"typeId\":\"14\",\"authorId\":\"1\",\"title\":\"Dog Training, Behaviour Correction and Puppy Education\",\"slug\":\"dog-training-behaviour-correction-and-puppy-education\",\"postDate\":1523887620,\"expiryDate\":null,\"enabled\":\"1\",\"parentId\":null,\"fields\":{\"91\":\"<p><strong>Please note<\\/strong>: We like to differentiate between performance and practical obedience. Performance obedience is suited for\\r\\n                        owners who like the idea of working with their dog and would like to engage in dog training as an activity, either\\r\\n                        competitively or not.<\\/p>\",\"85\":{\"157\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"One-on-one session\\r\\n(at centre).\",\"priceText\":\"\\u00a395.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"158\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"1\",\"caption\":\"Subsequent session\\r\\n(client residence)\",\"priceText\":\"\\u00a365.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"159\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"One-on-one session\\r\\n(client residence)\",\"priceText\":\"\\u00a3120.00\",\"subcaption\":\"\",\"size\":\"singleSized\"}},\"160\":{\"type\":\"priceBlock\",\"enabled\":\"1\",\"fields\":{\"isMostPopular\":\"\",\"caption\":\"Subsequent session\\r\\n(at centre)\",\"priceText\":\"\\u00a350.00\",\"subcaption\":\"(per 30 minutes)\",\"size\":\"singleSized\"}}}}}', '2018-04-16 16:19:43', '2018-04-16 16:19:43', '284ad9a6-ce9a-4e51-b20f-c9aa1fb0a11f'),
  (97, 16, 3, 1, 'en', 5, '', '{\"typeId\":\"9\",\"authorId\":\"1\",\"title\":\"Pupparazzi\",\"slug\":\"pupparazzi\",\"postDate\":1522683240,\"expiryDate\":null,\"enabled\":1,\"parentId\":null,\"fields\":{\"17\":\"4\",\"15\":\"6\",\"44\":{\"162\":{\"type\":\"coverblock\",\"enabled\":\"1\",\"fields\":{\"caption\":\"Pupparazzi\",\"picture\":[\"161\"]}}},\"16\":\"1\",\"11\":\"1\"}}', '2018-04-30 17:51:42', '2018-04-30 17:51:42', 'c83bd763-2a6e-4415-a5f2-af618c553d4b');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_fieldgroups`
--

CREATE TABLE `craft_fieldgroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_fieldgroups`
--

INSERT INTO `craft_fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 'Default', '2018-03-28 09:59:41', '2018-03-28 09:59:41', '8053dfdf-2e1d-42cc-8959-c3bc07892dd0'),
  (2, 'Company Info', '2018-04-02 08:51:35', '2018-04-02 08:51:40', '8b0fa1a8-33ef-44d2-86ce-fbe010e2a195'),
  (3, 'HomePage', '2018-04-02 13:59:16', '2018-04-05 08:25:06', '8ec5bc95-7fce-4669-bce7-ca1c846fbff4'),
  (4, 'Social Links', '2018-04-03 14:59:27', '2018-04-03 14:59:27', '52e66305-4f52-447f-a0e8-62f5f105ff94'),
  (5, 'Layout', '2018-04-05 08:25:21', '2018-04-05 08:25:21', 'fed08f29-7f0d-4d6c-8498-c76e00359935'),
  (6, 'AboutUs Page', '2018-04-10 12:55:40', '2018-04-10 12:55:40', '4d817433-e2a3-4cd6-b2b8-cda18edd21ad'),
  (7, 'Pricing Page', '2018-04-16 08:49:31', '2018-04-16 08:49:31', '7f10b3b7-4a53-474d-9c43-07bbec4879e9'),
  (8, 'Pupparazzi', '2018-04-30 17:29:46', '2018-04-30 17:29:46', '535fa22e-6f20-4db5-aea2-52fc7777a317'),
  (9, 'Blog', '2018-05-03 11:16:56', '2018-05-03 11:16:56', '700b4257-3a48-482c-9cf4-185bbf7dd9bc');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_fieldlayoutfields`
--

CREATE TABLE `craft_fieldlayoutfields` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_fieldlayoutfields`
--

INSERT INTO `craft_fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 3, 1, 1, 1, 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'c6266157-bebb-499b-a26f-e143010a3097'),
  (2, 5, 2, 1, 1, 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '6db552f4-167a-4286-b316-8f772a01e58b'),
  (3, 5, 2, 2, 0, 2, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '380bc603-34a4-4384-a0c0-8268c090899c'),
  (7, 11, 6, 8, 0, 1, '2018-04-02 09:15:42', '2018-04-02 09:15:42', '9d307a05-2b4c-4c29-a129-e5b5491f756f'),
  (8, 11, 6, 4, 0, 2, '2018-04-02 09:15:42', '2018-04-02 09:15:42', 'ea3a147d-358a-4d20-b9b6-e299a63c129a'),
  (9, 11, 6, 3, 1, 3, '2018-04-02 09:15:42', '2018-04-02 09:15:42', 'e51510e1-727b-49f8-bcc5-c42a9901f465'),
  (10, 11, 6, 5, 0, 4, '2018-04-02 09:15:42', '2018-04-02 09:15:42', '071568da-6952-49a6-b50e-f34024ec987d'),
  (11, 11, 6, 6, 0, 5, '2018-04-02 09:15:42', '2018-04-02 09:15:42', 'f3bcf009-9be2-4ed9-b310-f29fe13c6563'),
  (12, 11, 6, 7, 0, 6, '2018-04-02 09:15:42', '2018-04-02 09:15:42', 'f4a58a05-c973-4119-95e8-c6b85f1373ad'),
  (13, 11, 6, 9, 0, 7, '2018-04-02 09:15:42', '2018-04-02 09:15:42', '87b3fac0-d511-4ac0-9a31-c396b523d2d1'),
  (14, 11, 6, 10, 0, 8, '2018-04-02 09:15:42', '2018-04-02 09:15:42', 'd38c7e78-3ed1-4a5f-bbe9-fea490d1bc1d'),
  (134, 66, 55, 11, 0, 1, '2018-04-03 12:40:48', '2018-04-03 12:40:48', '38f2be04-a23a-4281-aca5-cdb94a5a5aae'),
  (135, 66, 55, 15, 1, 2, '2018-04-03 12:40:48', '2018-04-03 12:40:48', 'f3dc7bb0-c16d-4a2c-8123-613a8931b2b9'),
  (136, 66, 55, 16, 0, 3, '2018-04-03 12:40:48', '2018-04-03 12:40:48', '18cedd04-3e29-410c-97e7-a7644272e771'),
  (137, 66, 55, 17, 1, 4, '2018-04-03 12:40:48', '2018-04-03 12:40:48', '9b51146e-ad9b-4250-a5dd-5ba0a420b55e'),
  (142, 68, 57, 11, 0, 1, '2018-04-03 12:48:40', '2018-04-03 12:48:40', '6faa9f59-aaa5-4446-be1a-87d360d11033'),
  (143, 68, 57, 15, 1, 2, '2018-04-03 12:48:40', '2018-04-03 12:48:40', 'bce9a0cd-755c-41ec-94e8-5e60ce6d7e99'),
  (144, 68, 57, 16, 0, 3, '2018-04-03 12:48:40', '2018-04-03 12:48:40', '927d4db5-af2a-4398-ae0e-ae1b81448809'),
  (145, 68, 57, 17, 1, 4, '2018-04-03 12:48:40', '2018-04-03 12:48:40', '677ffa11-bfae-49d4-a6ac-c70366638089'),
  (146, 70, 58, 19, 1, 1, '2018-04-03 15:02:07', '2018-04-03 15:02:07', 'c5a3519e-5e29-475a-ab06-29e8fadff460'),
  (147, 70, 58, 18, 1, 2, '2018-04-03 15:02:07', '2018-04-03 15:02:07', '3a5abe56-9a07-41e0-8387-b92cf659f2f0'),
  (152, 74, 61, 13, 1, 1, '2018-04-04 08:20:04', '2018-04-04 08:20:04', '9d4ad93d-0bce-4135-8ef7-4aa1abf3ab52'),
  (153, 74, 61, 14, 1, 2, '2018-04-04 08:20:04', '2018-04-04 08:20:04', '1d18d971-b112-4eb6-96a9-3f8064c21275'),
  (218, 90, 74, 33, 1, 1, '2018-04-05 11:14:48', '2018-04-05 11:14:48', '4546e1bf-e49d-4359-81eb-9773b1068f0e'),
  (219, 90, 74, 34, 0, 2, '2018-04-05 11:14:48', '2018-04-05 11:14:48', '57b2f8e4-fc72-4d85-9896-91ae65dff2a2'),
  (220, 90, 74, 35, 0, 3, '2018-04-05 11:14:48', '2018-04-05 11:14:48', 'e746a02c-3100-4f99-ad6a-46e3491f8e2e'),
  (221, 90, 74, 36, 0, 4, '2018-04-05 11:14:48', '2018-04-05 11:14:48', '3664c3a6-825f-4041-bf40-7879c42f45fb'),
  (222, 91, 75, 21, 0, 1, '2018-04-05 11:16:58', '2018-04-05 11:16:58', '982122a3-122f-4d53-b21b-f39b33b97d3a'),
  (223, 91, 75, 22, 0, 2, '2018-04-05 11:16:58', '2018-04-05 11:16:58', '40575fb8-89d8-4573-a0c3-3fce67589bbd'),
  (224, 91, 75, 23, 0, 3, '2018-04-05 11:16:58', '2018-04-05 11:16:58', 'c00d7261-f7be-437a-8887-07504c364b2b'),
  (225, 91, 75, 24, 0, 4, '2018-04-05 11:16:58', '2018-04-05 11:16:58', '8aedfae9-a4d6-4633-ac1a-7dc8288e46c5'),
  (226, 91, 75, 25, 0, 5, '2018-04-05 11:16:58', '2018-04-05 11:16:58', '8d1e6701-9997-49dd-8752-b851677b7dde'),
  (227, 91, 75, 26, 0, 6, '2018-04-05 11:16:58', '2018-04-05 11:16:58', '0da21241-7f65-4a4d-809d-25fe71e83f9b'),
  (267, 103, 87, 38, 0, 1, '2018-04-05 11:45:24', '2018-04-05 11:45:24', '21e40a99-f55d-435f-9f9a-0fd8ef1a3778'),
  (268, 103, 87, 39, 0, 2, '2018-04-05 11:45:24', '2018-04-05 11:45:24', '1e9d43a0-2ef4-42b0-b6f8-3570920c7bc1'),
  (269, 103, 87, 40, 0, 3, '2018-04-05 11:45:24', '2018-04-05 11:45:24', '4cb72589-b7b0-4a79-a4b2-eef698435e8b'),
  (270, 104, 88, 41, 0, 1, '2018-04-05 11:45:24', '2018-04-05 11:45:24', '6a6228f3-be31-43f5-af14-14251c1aa562'),
  (271, 104, 88, 42, 0, 2, '2018-04-05 11:45:24', '2018-04-05 11:45:24', '781fe63f-f051-4dc9-9109-97a2d807ab54'),
  (272, 104, 88, 43, 0, 3, '2018-04-05 11:45:24', '2018-04-05 11:45:24', 'b1525a64-92af-4aa0-890b-b0d0a38169b6'),
  (287, 112, 94, 46, 0, 1, '2018-04-06 08:11:19', '2018-04-06 08:11:19', '7e274770-12b7-4c61-b769-fe5efabd50b5'),
  (288, 113, 95, 47, 0, 1, '2018-04-06 08:11:19', '2018-04-06 08:11:19', '88895e23-3185-4503-8b1d-13370e4d542f'),
  (289, 113, 95, 48, 0, 2, '2018-04-06 08:11:19', '2018-04-06 08:11:19', '449f3cc8-156e-4f54-a472-425407a54c7b'),
  (290, 113, 95, 49, 0, 3, '2018-04-06 08:11:19', '2018-04-06 08:11:19', 'a0a7e398-5591-4bdf-bf6f-4635b3f30eaf'),
  (291, 114, 96, 50, 0, 1, '2018-04-06 08:26:23', '2018-04-06 08:26:23', '0c49f43c-febf-41c4-965c-50caaa5120ee'),
  (292, 114, 96, 51, 0, 2, '2018-04-06 08:26:23', '2018-04-06 08:26:23', 'b180ff26-a8bc-4df6-82cb-4cf1434bda61'),
  (293, 115, 97, 11, 0, 1, '2018-04-06 09:41:46', '2018-04-06 09:41:46', '5617d1cb-b939-44ce-8029-4962f80ea312'),
  (294, 115, 97, 15, 1, 2, '2018-04-06 09:41:46', '2018-04-06 09:41:46', '4273ce80-44dc-4ef7-88f5-b86813b40223'),
  (295, 115, 97, 16, 0, 3, '2018-04-06 09:41:46', '2018-04-06 09:41:46', 'e23ba28c-ffbe-401b-8cb7-84c83469402f'),
  (296, 115, 97, 17, 1, 4, '2018-04-06 09:41:46', '2018-04-06 09:41:46', 'e2b0907c-19c0-4bc0-801d-ef0a8be0ae72'),
  (297, 115, 97, 44, 0, 5, '2018-04-06 09:41:46', '2018-04-06 09:41:46', '00765e53-79b6-4310-91f5-2d05421021b3'),
  (298, 115, 97, 45, 0, 6, '2018-04-06 09:41:46', '2018-04-06 09:41:46', '26a10348-216d-475c-816b-bb7cf80bb74d'),
  (299, 115, 97, 32, 0, 7, '2018-04-06 09:41:46', '2018-04-06 09:41:46', '122c8263-3207-4fb0-abb6-b641f7b43598'),
  (305, 118, 99, 53, 0, 1, '2018-04-06 12:30:25', '2018-04-06 12:30:25', 'eb247565-7e55-4302-ba33-31370f6164e4'),
  (306, 118, 99, 54, 0, 2, '2018-04-06 12:30:25', '2018-04-06 12:30:25', '4f90e377-baf9-43b4-aea1-390f973a150b'),
  (307, 118, 99, 55, 0, 3, '2018-04-06 12:30:25', '2018-04-06 12:30:25', '7e1fc71a-8cab-4643-b937-625827d26ab1'),
  (308, 119, 100, 11, 0, 1, '2018-04-06 12:30:40', '2018-04-06 12:30:40', '91ad4f9c-3de2-4314-b1af-679839fbe7aa'),
  (309, 119, 100, 15, 1, 2, '2018-04-06 12:30:40', '2018-04-06 12:30:40', '319bb3ff-cf24-48b1-b5ec-da5c5dba7ac6'),
  (310, 119, 100, 16, 0, 3, '2018-04-06 12:30:40', '2018-04-06 12:30:40', '0443617b-3d1d-4d0a-9760-fd3394a5cd6c'),
  (311, 119, 100, 17, 1, 4, '2018-04-06 12:30:40', '2018-04-06 12:30:40', '06049c5c-466c-45f7-aee2-b47a700bb454'),
  (312, 119, 100, 44, 0, 5, '2018-04-06 12:30:40', '2018-04-06 12:30:40', '814d4033-633d-4d2d-b13f-ff12e0059aab'),
  (313, 119, 100, 52, 0, 6, '2018-04-06 12:30:40', '2018-04-06 12:30:40', '61f3424d-c6d2-413a-83a5-a9a8198cf94e'),
  (325, 125, 104, 57, 0, 1, '2018-04-09 11:03:02', '2018-04-09 11:03:02', 'd8677b4f-70fb-4244-8a55-77058b0a60d6'),
  (326, 125, 104, 58, 0, 2, '2018-04-09 11:03:02', '2018-04-09 11:03:02', '5937d742-72b7-4677-b712-0274a063536a'),
  (327, 125, 104, 59, 0, 3, '2018-04-09 11:03:02', '2018-04-09 11:03:02', 'ac8c3c2f-1fe9-419a-abdb-dd73624076a2'),
  (328, 126, 105, 60, 0, 1, '2018-04-09 11:03:02', '2018-04-09 11:03:02', '5f50ef75-dee8-42d4-9a78-a5e70874c59d'),
  (329, 126, 105, 61, 0, 2, '2018-04-09 11:03:02', '2018-04-09 11:03:02', '2f780f3b-b591-4880-b395-cf6a162429b8'),
  (330, 126, 105, 62, 0, 3, '2018-04-09 11:03:02', '2018-04-09 11:03:02', 'a0d7ae40-c9c4-44b9-b6eb-14160fa913a7'),
  (343, 129, 108, 11, 0, 1, '2018-04-09 11:03:43', '2018-04-09 11:03:43', '2689b3da-1df4-4bdb-88ed-9dd2ce55e246'),
  (344, 129, 108, 15, 1, 2, '2018-04-09 11:03:43', '2018-04-09 11:03:43', '9b5239a2-0cf2-4093-81c4-253cd1da716f'),
  (345, 129, 108, 16, 0, 3, '2018-04-09 11:03:43', '2018-04-09 11:03:43', '1890a3f5-2aa9-4e16-abd8-309d4fe75bb0'),
  (346, 129, 108, 17, 1, 4, '2018-04-09 11:03:43', '2018-04-09 11:03:43', 'e29deab9-deea-49fd-b9c2-dccc5c0137c4'),
  (347, 129, 108, 44, 0, 5, '2018-04-09 11:03:43', '2018-04-09 11:03:43', '0ef3eade-8d49-49e6-a313-f448dce82625'),
  (348, 129, 108, 56, 0, 6, '2018-04-09 11:03:43', '2018-04-09 11:03:43', '3fcb58a3-3223-4b5a-af9d-ebf283245af7'),
  (349, 133, 109, 27, 0, 1, '2018-04-10 12:25:20', '2018-04-10 12:25:20', '2912644b-ab84-4814-a573-24318acb3c17'),
  (350, 134, 110, 28, 0, 1, '2018-04-10 12:27:34', '2018-04-10 12:27:34', '78d8684a-58e7-4db2-9e99-bb8dfb449ac9'),
  (351, 134, 110, 29, 0, 2, '2018-04-10 12:27:34', '2018-04-10 12:27:34', 'aed29136-a1ab-4d90-858c-fe138babf968'),
  (352, 134, 110, 30, 0, 3, '2018-04-10 12:27:34', '2018-04-10 12:27:34', 'fd2b1429-eba8-4737-a787-80b1ef701382'),
  (353, 134, 110, 31, 0, 4, '2018-04-10 12:27:34', '2018-04-10 12:27:34', '4c40768b-b1a6-4438-a61a-6bfced412557'),
  (362, 136, 112, 64, 0, 1, '2018-04-10 13:13:12', '2018-04-10 13:13:12', '9bf87530-d521-4fc6-a148-538860cc112b'),
  (363, 136, 112, 65, 0, 2, '2018-04-10 13:13:12', '2018-04-10 13:13:12', '9bd79420-81d7-4ffb-8f9a-a1575500c3de'),
  (364, 136, 112, 66, 0, 3, '2018-04-10 13:13:12', '2018-04-10 13:13:12', '91ee5ae3-b2e0-4506-85dc-bff63686676c'),
  (365, 137, 113, 68, 0, 1, '2018-04-10 14:41:45', '2018-04-10 14:41:45', 'd1df6267-a7a6-420e-8ecd-98150b657673'),
  (366, 138, 114, 11, 0, 1, '2018-04-10 14:43:13', '2018-04-10 14:43:13', '612b7fff-1142-40b8-9e96-07cf39bdebcd'),
  (367, 138, 114, 15, 1, 2, '2018-04-10 14:43:13', '2018-04-10 14:43:13', 'f3161089-d774-448b-b7e5-277cbcd4ff3f'),
  (368, 138, 114, 16, 0, 3, '2018-04-10 14:43:13', '2018-04-10 14:43:13', 'ecbdd3b5-201f-4ab0-ab43-64c688a1a8e4'),
  (369, 138, 114, 17, 1, 4, '2018-04-10 14:43:13', '2018-04-10 14:43:13', '4e8eecc0-3ae1-452f-9788-4b64dc8f1081'),
  (370, 138, 114, 44, 0, 5, '2018-04-10 14:43:13', '2018-04-10 14:43:13', '7161f111-5ae6-4198-87de-e70237898b15'),
  (371, 138, 114, 63, 0, 6, '2018-04-10 14:43:13', '2018-04-10 14:43:13', '3da114c7-cb7f-496e-bd97-5d918d5ee99f'),
  (372, 138, 114, 67, 0, 7, '2018-04-10 14:43:13', '2018-04-10 14:43:13', '1760a044-5f6a-4756-af51-b8253e9ac3c0'),
  (434, 156, 131, 77, 0, 1, '2018-04-16 12:41:53', '2018-04-16 12:41:53', 'f1ab6ca6-034b-452c-8ca2-d8107172d3d2'),
  (435, 156, 131, 83, 0, 2, '2018-04-16 12:41:53', '2018-04-16 12:41:53', 'bba80ed9-d0ec-4a8a-b0c0-9c993a88f0eb'),
  (436, 156, 131, 78, 0, 3, '2018-04-16 12:41:53', '2018-04-16 12:41:53', 'fd3cbb13-3251-4eee-871f-a69fe7f9271c'),
  (437, 156, 131, 79, 0, 4, '2018-04-16 12:41:53', '2018-04-16 12:41:53', 'b5d84648-dd61-428c-89ca-38fb5cf67b3f'),
  (438, 156, 131, 80, 0, 5, '2018-04-16 12:41:53', '2018-04-16 12:41:53', '67b9f6f4-6690-4505-b526-16da411c24d9'),
  (439, 156, 131, 82, 0, 6, '2018-04-16 12:41:53', '2018-04-16 12:41:53', '48e0cf42-2dfb-4d1d-936b-e439fcc5258e'),
  (449, 160, 134, 11, 0, 1, '2018-04-16 14:09:33', '2018-04-16 14:09:33', 'b1c13991-9fef-4632-a1f9-c8289f2c2985'),
  (450, 160, 134, 15, 1, 2, '2018-04-16 14:09:33', '2018-04-16 14:09:33', 'aca57ac8-b688-4f21-95c1-abc6ca6ab2ab'),
  (451, 160, 134, 16, 0, 3, '2018-04-16 14:09:33', '2018-04-16 14:09:33', 'cb6ca6b6-07be-4ff2-bcaa-0d6e43fbc627'),
  (452, 160, 134, 17, 1, 4, '2018-04-16 14:09:33', '2018-04-16 14:09:33', 'ff020c50-c78a-45e7-b166-bc782f2488d1'),
  (453, 160, 134, 44, 0, 5, '2018-04-16 14:09:33', '2018-04-16 14:09:33', '728f0340-fae8-4872-80f6-746c46c712b8'),
  (454, 160, 134, 76, 0, 6, '2018-04-16 14:09:33', '2018-04-16 14:09:33', '2f1c3f99-4513-4e81-b01a-a9e81eb9e875'),
  (455, 160, 134, 84, 0, 7, '2018-04-16 14:09:33', '2018-04-16 14:09:33', 'c7e1ea77-e38e-49dc-9a43-c756d565b042'),
  (464, 166, 139, 86, 0, 1, '2018-04-16 15:19:57', '2018-04-16 15:19:57', 'a27965b4-b048-441f-bc48-550d4373fc7b'),
  (465, 166, 139, 87, 0, 2, '2018-04-16 15:19:57', '2018-04-16 15:19:57', '9c1fa7aa-51ee-422c-9a12-3f4dcd0501b6'),
  (466, 166, 139, 88, 0, 3, '2018-04-16 15:19:57', '2018-04-16 15:19:57', 'c237be05-4851-443e-aa93-3cccbf016605'),
  (467, 166, 139, 89, 0, 4, '2018-04-16 15:19:57', '2018-04-16 15:19:57', '41a8016e-56fa-408b-aa7a-00a20c1e0e40'),
  (468, 166, 139, 90, 0, 5, '2018-04-16 15:19:57', '2018-04-16 15:19:57', 'aeaad992-af8c-4172-8e2d-0c58eeb97738'),
  (469, 167, 140, 85, 0, 1, '2018-04-16 15:23:03', '2018-04-16 15:23:03', '130fa7d2-0b40-4435-9308-d5ea29887759'),
  (470, 167, 140, 91, 0, 2, '2018-04-16 15:23:03', '2018-04-16 15:23:03', '69ded020-e3c3-4866-8724-6586106c3e6c'),
  (478, 170, 143, 11, 0, 1, '2018-05-03 11:08:36', '2018-05-03 11:08:36', 'c1e3ba98-fb21-42f2-9b28-6e491773f818'),
  (479, 170, 143, 15, 1, 2, '2018-05-03 11:08:36', '2018-05-03 11:08:36', '44d29d7e-991c-4e86-a5a4-44d26795ee4f'),
  (480, 170, 143, 16, 0, 3, '2018-05-03 11:08:36', '2018-05-03 11:08:36', '0fcbc65e-3d0e-4b9b-81cb-15427ea8b95b'),
  (481, 170, 143, 17, 1, 4, '2018-05-03 11:08:36', '2018-05-03 11:08:36', '6c9d616a-802d-4048-bbf4-d29aa9fbabd5'),
  (482, 170, 143, 44, 0, 5, '2018-05-03 11:08:36', '2018-05-03 11:08:36', '57b04953-c888-4a60-8f11-5432a0a74b2a'),
  (483, 170, 143, 92, 0, 6, '2018-05-03 11:08:36', '2018-05-03 11:08:36', 'afee340b-5822-468f-a667-e6bf00ac9942'),
  (484, 171, 144, 11, 0, 1, '2018-05-03 11:11:32', '2018-05-03 11:11:32', '684276ba-f2ac-4a94-8b2c-01ddb4639b94'),
  (485, 171, 144, 15, 1, 2, '2018-05-03 11:11:32', '2018-05-03 11:11:32', '8d5fb8c9-c9e4-46ae-9be3-a9ed32c69a47'),
  (486, 171, 144, 16, 0, 3, '2018-05-03 11:11:32', '2018-05-03 11:11:32', '82c32f76-8451-483a-a15d-bf33b6f8fe83'),
  (487, 171, 144, 17, 1, 4, '2018-05-03 11:11:32', '2018-05-03 11:11:32', '2e088ab9-4e03-4f9f-84a4-23e93bc55aec'),
  (488, 171, 144, 12, 0, 5, '2018-05-03 11:11:32', '2018-05-03 11:11:32', '660f84a5-5d21-4bce-ac13-69a58c8f0c08'),
  (489, 171, 144, 20, 0, 6, '2018-05-03 11:11:32', '2018-05-03 11:11:32', 'b480d1ea-4add-4784-8f3c-20e0b74532dc'),
  (490, 171, 144, 32, 0, 7, '2018-05-03 11:11:32', '2018-05-03 11:11:32', 'ed007b86-aa1a-4575-8443-96de7d7b2151'),
  (491, 171, 144, 37, 0, 8, '2018-05-03 11:11:32', '2018-05-03 11:11:32', '13267b4a-6e28-4b5a-80a8-9f2555917ca7'),
  (492, 171, 144, 92, 0, 9, '2018-05-03 11:11:32', '2018-05-03 11:11:32', 'e68fec3a-5f17-419f-8c18-7ab860c6f484'),
  (495, 174, 146, 93, 0, 1, '2018-05-03 11:15:07', '2018-05-03 11:15:07', 'a729c8b1-63ad-4681-b656-e7f1ff13cf92'),
  (496, 174, 146, 94, 0, 2, '2018-05-03 11:15:07', '2018-05-03 11:15:07', '98965c49-3b4a-44ba-b340-2a702b5ffdef'),
  (497, 175, 147, 96, 0, 1, '2018-05-03 11:22:37', '2018-05-03 11:22:37', 'd6352b60-de64-457e-a229-209e58f58fba'),
  (498, 175, 147, 97, 0, 2, '2018-05-03 11:22:37', '2018-05-03 11:22:37', '4d39fff6-f6f9-4a45-b39b-7639c67d8bb2'),
  (499, 175, 147, 98, 0, 3, '2018-05-03 11:22:37', '2018-05-03 11:22:37', 'd20f5af1-85cd-4374-8908-4675f94b26af'),
  (500, 175, 147, 99, 0, 4, '2018-05-03 11:22:37', '2018-05-03 11:22:37', 'aeb7f1fe-7303-4484-b8c1-df4595b694d0'),
  (501, 175, 147, 100, 0, 5, '2018-05-03 11:22:37', '2018-05-03 11:22:37', '331239c4-6f3c-44dd-ba9e-d18753bf937e'),
  (502, 175, 147, 101, 0, 6, '2018-05-03 11:22:37', '2018-05-03 11:22:37', 'c033c97a-5267-43f7-ad51-903148fb6900'),
  (503, 175, 147, 102, 0, 7, '2018-05-03 11:22:37', '2018-05-03 11:22:37', 'f57d9185-d777-4d05-a8eb-5253f8a05a92'),
  (504, 175, 147, 103, 0, 8, '2018-05-03 11:22:37', '2018-05-03 11:22:37', '981837d0-f351-4ceb-9958-187a87a5e411'),
  (505, 176, 148, 95, 0, 1, '2018-05-03 11:29:03', '2018-05-03 11:29:03', '77066821-3b30-4a11-af05-666c0bf84fc3');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_fieldlayouts`
--

CREATE TABLE `craft_fieldlayouts` (
  `id` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_fieldlayouts`
--

INSERT INTO `craft_fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 'Tag', '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'a5a46eca-3c1e-40c3-9c3a-a72ddeb9b2bf'),
  (3, 'Entry', '2018-03-28 09:59:41', '2018-03-28 09:59:41', '665e42dd-eaa1-44e1-9a29-a0bfa0fe92aa'),
  (5, 'Entry', '2018-03-28 09:59:41', '2018-03-28 09:59:41', '7ade4c51-3e19-42bf-9c43-cbc8f58baa6e'),
  (11, 'GlobalSet', '2018-04-02 09:15:42', '2018-04-02 09:15:42', 'f81c1dff-d318-4448-9e4f-7ee7bc221208'),
  (15, 'Asset', '2018-04-02 11:34:00', '2018-04-02 11:34:00', '98374c94-13ab-4bf9-86a0-1f9bc2173381'),
  (66, 'Entry', '2018-04-03 12:40:48', '2018-04-03 12:40:48', '9d1e5e69-223d-4857-a909-11de94939349'),
  (68, 'Entry', '2018-04-03 12:48:40', '2018-04-03 12:48:40', 'b344f5c1-f898-4fac-809d-469b050ca83a'),
  (70, 'Entry', '2018-04-03 15:02:07', '2018-04-03 15:02:07', '4b029768-22aa-4279-b176-3dd885f1fa0a'),
  (74, 'MatrixBlock', '2018-04-04 08:20:04', '2018-04-04 08:20:04', '39c49c80-da19-4b52-8f3d-681f1077df22'),
  (90, 'MatrixBlock', '2018-04-05 11:14:48', '2018-04-05 11:14:48', '32896349-d58f-439b-b0f4-26cac99fd302'),
  (91, 'MatrixBlock', '2018-04-05 11:16:58', '2018-04-05 11:16:58', '983cb8ba-9655-4f60-b8f9-3a90a5cf7cf6'),
  (103, 'MatrixBlock', '2018-04-05 11:45:24', '2018-04-05 11:45:24', 'a2f2e380-252b-42e2-a9ba-3cc5359bcc49'),
  (104, 'MatrixBlock', '2018-04-05 11:45:24', '2018-04-05 11:45:24', '0a270821-faa5-4e8d-8dd5-d3e4711e7649'),
  (105, 'Asset', '2018-04-06 07:39:28', '2018-04-06 07:39:28', '6bd63c8b-2119-4a96-95db-aba643ca0b7f'),
  (106, 'Asset', '2018-04-06 07:40:09', '2018-04-06 07:40:09', '165d5787-7a6c-4325-853e-75f820e1da48'),
  (112, 'MatrixBlock', '2018-04-06 08:11:19', '2018-04-06 08:11:19', 'ea32e6ef-fa58-456d-9b05-bdbcfe27b18f'),
  (113, 'MatrixBlock', '2018-04-06 08:11:19', '2018-04-06 08:11:19', '0ffd479b-62b9-4327-a432-f4bfbcbb5107'),
  (114, 'MatrixBlock', '2018-04-06 08:26:23', '2018-04-06 08:26:23', 'd3b1d3c6-d1e2-46e1-9d07-482e67576caa'),
  (115, 'Entry', '2018-04-06 09:41:46', '2018-04-06 09:41:46', 'a681657a-5a37-411e-b411-a2a90cb96d11'),
  (118, 'MatrixBlock', '2018-04-06 12:30:25', '2018-04-06 12:30:25', '283db487-19f4-4468-bca9-182a3ae6ba60'),
  (119, 'Entry', '2018-04-06 12:30:40', '2018-04-06 12:30:40', 'd34832c3-ee45-4c88-92bd-cf0a09468b5e'),
  (120, 'Asset', '2018-04-06 12:34:30', '2018-04-06 12:34:30', '50a4d971-c815-44a9-b845-d78d6360ebb7'),
  (125, 'MatrixBlock', '2018-04-09 11:03:02', '2018-04-09 11:03:02', '91b0ee77-b63e-48d7-aa88-dab1a600491b'),
  (126, 'MatrixBlock', '2018-04-09 11:03:02', '2018-04-09 11:03:02', '9681518b-eb0a-4881-9eeb-45f2e522350b'),
  (129, 'Entry', '2018-04-09 11:03:43', '2018-04-09 11:03:43', '4f942dbf-541a-4815-afc4-6ee56cd34407'),
  (130, 'Asset', '2018-04-09 13:53:36', '2018-04-09 13:53:36', 'c1f19ea7-2f2d-466b-a8b6-67aa4521c6df'),
  (131, 'Asset', '2018-04-09 14:06:37', '2018-04-09 14:06:37', '771a0c4d-095d-4e1f-b377-df1eeccc8d0d'),
  (133, 'Entry', '2018-04-10 12:25:20', '2018-04-10 12:25:20', '4e85186d-cd45-42ba-b85c-39018943bc19'),
  (134, 'MatrixBlock', '2018-04-10 12:27:34', '2018-04-10 12:27:34', '5f8eb940-4448-4752-9f89-6e31f468a3c5'),
  (136, 'MatrixBlock', '2018-04-10 13:13:12', '2018-04-10 13:13:12', 'cab78e12-7a3d-4a7a-a9fd-024c6d7b49fe'),
  (137, 'MatrixBlock', '2018-04-10 14:41:45', '2018-04-10 14:41:45', 'c2e13d67-c69d-4531-856b-d801a5d9d9e6'),
  (138, 'Entry', '2018-04-10 14:43:13', '2018-04-10 14:43:13', 'd47e4afe-d364-47eb-837e-9e8acb2217a5'),
  (153, 'Asset', '2018-04-16 12:06:39', '2018-04-16 12:06:39', '88da6eb1-fe9c-40f9-8c8a-e5324ddb490e'),
  (156, 'MatrixBlock', '2018-04-16 12:41:53', '2018-04-16 12:41:53', '8380f561-09cd-4168-9cac-52a5c94e01ed'),
  (160, 'Entry', '2018-04-16 14:09:33', '2018-04-16 14:09:33', '9c187597-69b8-4a0b-a307-01062b438693'),
  (166, 'MatrixBlock', '2018-04-16 15:19:57', '2018-04-16 15:19:57', '5681f0c2-899a-4ff8-801e-2962c2b1262f'),
  (167, 'Entry', '2018-04-16 15:23:03', '2018-04-16 15:23:03', 'cef3c172-3cf4-4360-9e51-c2cd1f8dff9a'),
  (170, 'Entry', '2018-05-03 11:08:36', '2018-05-03 11:08:36', 'aea9e7cd-d486-4201-82ec-158010ea505a'),
  (171, 'Entry', '2018-05-03 11:11:32', '2018-05-03 11:11:32', '49271eee-e601-41fe-8eef-435e396fff29'),
  (174, 'MatrixBlock', '2018-05-03 11:15:07', '2018-05-03 11:15:07', '062b28c3-7c1a-471d-be1a-0fdb57d2c372'),
  (175, 'MatrixBlock', '2018-05-03 11:22:37', '2018-05-03 11:22:37', '2fe5e302-53cd-456b-95b2-0879d937e173'),
  (176, 'Entry', '2018-05-03 11:29:03', '2018-05-03 11:29:03', '87938316-b2b6-4060-aceb-6e247420bc29');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_fieldlayouttabs`
--

CREATE TABLE `craft_fieldlayouttabs` (
  `id` int(11) NOT NULL,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_fieldlayouttabs`
--

INSERT INTO `craft_fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 3, 'Content', 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '019d205a-67eb-4f94-86f1-96daed977ac7'),
  (2, 5, 'Content', 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '42a2a584-1c4c-4fb0-ac81-1a625a8ee549'),
  (6, 11, 'Content', 1, '2018-04-02 09:15:42', '2018-04-02 09:15:42', '0a10fdc6-a45a-42ab-8643-cc69c69687f4'),
  (55, 66, 'Page', 1, '2018-04-03 12:40:48', '2018-04-03 12:40:48', '88f9c53a-6803-42ca-a0f1-e7b31ef51086'),
  (57, 68, 'Page', 1, '2018-04-03 12:48:40', '2018-04-03 12:48:40', '5c50150e-7a3b-42e9-8bf0-0ad172853331'),
  (58, 70, 'Social Links', 1, '2018-04-03 15:02:07', '2018-04-03 15:02:07', 'ade6d5fd-daf4-4a60-8567-adf36d676253'),
  (61, 74, 'Content', 1, '2018-04-04 08:20:04', '2018-04-04 08:20:04', 'fb4c6309-ef50-4815-8016-9e4bf3eff313'),
  (74, 90, 'Content', 1, '2018-04-05 11:14:48', '2018-04-05 11:14:48', 'b4f6ea9e-fdb3-49f0-a689-53afcdd5f967'),
  (75, 91, 'Content', 1, '2018-04-05 11:16:58', '2018-04-05 11:16:58', 'ba747634-6264-4c7c-a426-043e509444e3'),
  (87, 103, 'Content', 1, '2018-04-05 11:45:24', '2018-04-05 11:45:24', 'a92b8d85-4384-4066-94cb-568a819f758f'),
  (88, 104, 'Content', 1, '2018-04-05 11:45:24', '2018-04-05 11:45:24', '936c440e-2c5a-44f1-8934-280d37cc93a0'),
  (94, 112, 'Content', 1, '2018-04-06 08:11:19', '2018-04-06 08:11:19', '80b2a063-669b-4f12-9930-f6d15de256a8'),
  (95, 113, 'Content', 1, '2018-04-06 08:11:19', '2018-04-06 08:11:19', '7dc34dc5-3bdf-4fd8-8475-9075bdd5bbf5'),
  (96, 114, 'Content', 1, '2018-04-06 08:26:23', '2018-04-06 08:26:23', 'fed72504-eb91-4e82-a9b1-ff8d406c1a70'),
  (97, 115, 'Services', 1, '2018-04-06 09:41:46', '2018-04-06 09:41:46', '1c915e13-7507-46a7-acca-33e68f962f45'),
  (99, 118, 'Content', 1, '2018-04-06 12:30:25', '2018-04-06 12:30:25', '5b1e56ff-4c26-4d89-8a1e-4446a16c320b'),
  (100, 119, 'Page', 1, '2018-04-06 12:30:40', '2018-04-06 12:30:40', 'a8cb6528-f90e-4d85-abfd-b86686757c59'),
  (104, 125, 'Content', 1, '2018-04-09 11:03:02', '2018-04-09 11:03:02', '8f3cdaef-cb1b-4039-b83c-0f749dece868'),
  (105, 126, 'Content', 1, '2018-04-09 11:03:02', '2018-04-09 11:03:02', 'afc294aa-7c61-4e73-b17f-7d0b6c430a98'),
  (108, 129, 'Page', 1, '2018-04-09 11:03:43', '2018-04-09 11:03:43', '7a1fd491-499d-414f-9cd6-96ad3a7efbc2'),
  (109, 133, 'Feedback List', 1, '2018-04-10 12:25:20', '2018-04-10 12:25:20', '12b1c7c2-c537-4eaf-b106-6cfcf9f05c07'),
  (110, 134, 'Content', 1, '2018-04-10 12:27:34', '2018-04-10 12:27:34', '7f5f9588-7ec6-49b0-9788-4a08eb5bd5cb'),
  (112, 136, 'Content', 1, '2018-04-10 13:13:12', '2018-04-10 13:13:12', '596e56db-0b97-45ec-9849-ae737bbce570'),
  (113, 137, 'Content', 1, '2018-04-10 14:41:45', '2018-04-10 14:41:45', '2d5da60b-9e1f-45ef-bc27-c3fd3c0ff5fe'),
  (114, 138, 'Page', 1, '2018-04-10 14:43:13', '2018-04-10 14:43:13', '8a71e36a-8d66-4ea3-926f-410a328f7803'),
  (131, 156, 'Content', 1, '2018-04-16 12:41:53', '2018-04-16 12:41:53', '9d6a9bda-57cc-4a78-8595-1f27b21ce008'),
  (134, 160, 'Page', 1, '2018-04-16 14:09:33', '2018-04-16 14:09:33', 'c16e2086-0a9f-4892-8ec8-e9ee3b2c38dd'),
  (139, 166, 'Content', 1, '2018-04-16 15:19:57', '2018-04-16 15:19:57', 'f8085499-ea29-47f0-ac81-74ba26d46749'),
  (140, 167, 'Price List', 1, '2018-04-16 15:23:03', '2018-04-16 15:23:03', 'c5c2dad7-f388-475a-80a5-cf6dabca5458'),
  (143, 170, 'Page', 1, '2018-05-03 11:08:36', '2018-05-03 11:08:36', 'fa20b7d3-a8a7-4572-b08d-52d20a13d215'),
  (144, 171, 'Page', 1, '2018-05-03 11:11:32', '2018-05-03 11:11:32', '4943b5f0-3646-4886-a723-8b7cccf143d7'),
  (146, 174, 'Content', 1, '2018-05-03 11:15:07', '2018-05-03 11:15:07', 'a41749bc-2d67-4f4e-bede-80256fc4ece9'),
  (147, 175, 'Content', 1, '2018-05-03 11:22:37', '2018-05-03 11:22:37', '0763da47-5700-4e9a-892b-ba1ac6ed43f9'),
  (148, 176, 'Blog', 1, '2018-05-03 11:29:03', '2018-05-03 11:29:03', '9b6d571a-34aa-4b43-9798-55077ec9391d');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_fields`
--

CREATE TABLE `craft_fields` (
  `id` int(11) NOT NULL,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(58) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'global',
  `instructions` text COLLATE utf8_unicode_ci,
  `translatable` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_fields`
--

INSERT INTO `craft_fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `translatable`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 1, 'Body', 'body', 'global', NULL, 1, 'RichText', '{\"configFile\":\"Standard.json\",\"columnType\":\"text\"}', '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'a41a66b9-640b-46bf-8c12-80e63d7bee8c'),
  (2, 1, 'Tags', 'tags', 'global', NULL, 0, 'Tags', '{\"source\":\"taggroup:1\"}', '2018-03-28 09:59:41', '2018-03-28 09:59:41', '7ed3468d-f027-4e0c-ba9c-6dd588c552e2'),
  (3, 2, 'Phone Number', 'phoneNumber', 'global', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"20\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-02 08:52:54', '2018-04-02 08:52:54', '5f7dacf3-ba0d-4458-8bcb-fb42b506291b'),
  (4, 2, 'Foundation Year', 'foundationYear', 'global', 'Year which is displayed in footer', 0, 'PlainText', '{\"placeholder\":\"2017\",\"maxLength\":\"4\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-02 09:07:39', '2018-04-02 09:07:39', '47f43a50-f7bf-4d56-b715-dec95325026e'),
  (5, 2, 'Address Line 1', 'addressLine1', 'global', '', 0, 'PlainText', '{\"placeholder\":\"Hallyards Farm House\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-02 09:11:07', '2018-04-02 09:11:55', 'd3661d62-93ec-4889-aa6b-97fcf259db43'),
  (6, 2, 'Address Line 2', 'addressLine2', 'global', '', 0, 'PlainText', '{\"placeholder\":\"Kirkliston\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-02 09:11:41', '2018-04-02 09:11:41', 'f7bd6431-6622-482a-a440-9c1f5d2af57d'),
  (7, 2, 'Address Line 3', 'addressLine3', 'global', '', 0, 'PlainText', '{\"placeholder\":\"EH29 9DZ\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-02 09:12:15', '2018-04-02 09:12:15', 'ea6298e4-11d1-41bc-8820-352f88df9e19'),
  (8, 2, 'Company Title', 'companyTitle', 'global', '', 0, 'PlainText', '{\"placeholder\":\"Bark n\\u2019 Fly \",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-02 09:13:40', '2018-04-02 09:13:40', '36a31050-ed2b-476d-8dad-288e2198ccf2'),
  (9, 2, 'LogoColored', 'logocolored', 'global', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-02 09:14:35', '2018-04-02 09:14:35', 'ef6df268-ddf4-4f46-97e7-cab6b7dbd4dd'),
  (10, 2, 'LogoGreyed', 'logogreyed', 'global', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-02 09:14:44', '2018-04-02 09:14:44', '179e21ef-58d3-4d46-bb2a-2020a939ae83'),
  (11, 5, 'Show In Menu', 'showInMenu', 'global', 'Indicates whether page should be presented in site header menu', 0, 'Lightswitch', '{\"default\":\"\"}', '2018-04-02 14:01:43', '2018-04-05 08:26:27', '3a606e4e-27dd-4051-9bc9-05d5ae43777a'),
  (12, 3, 'Top Carousel', 'topCarousel', 'global', 'that one is located right under the main menu', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-02 14:51:43', '2018-04-04 08:20:04', 'a2913f5b-dba0-447f-99c9-4db4d59e0a7c'),
  (13, NULL, 'Caption', 'caption', 'matrixBlockType:1', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-02 14:51:43', '2018-04-04 08:20:04', '8e57fbc3-0b84-4732-b42b-eea4d4b6ccd1'),
  (14, NULL, 'Cover Picture', 'coverPicture', 'matrixBlockType:1', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-02 14:51:43', '2018-04-04 08:20:04', '0f486a15-b64d-4587-aade-f659d3b893c1'),
  (15, 5, 'Menu Sorting Order', 'sortingOrder', 'global', '', 0, 'Number', '{\"min\":\"0\",\"max\":\"100\",\"decimals\":\"0\"}', '2018-04-02 15:57:21', '2018-04-05 08:26:12', '501ce91c-92f3-41c8-9592-bbc8731446f3'),
  (16, 5, 'Show In Footer', 'showInFooter', 'global', '', 0, 'Lightswitch', '{\"default\":\"\"}', '2018-04-03 11:29:22', '2018-04-05 08:26:19', '5053982d-b6ae-48ba-afa1-24f40b8e74fa'),
  (17, 5, 'Footer Sorting Order', 'footerSortingOrder', 'global', '', 0, 'Number', '{\"min\":\"0\",\"max\":\"100\",\"decimals\":\"0\"}', '2018-04-03 12:38:36', '2018-04-05 08:26:02', 'e82306e7-586c-4e50-b9ee-963e49aff9f1'),
  (18, 4, 'Social Link', 'socialLink', 'global', 'URL to social network page', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-03 15:00:51', '2018-04-03 15:00:51', '73c160cd-5b60-40ec-91d6-4bc889dd1d9c'),
  (19, 4, 'Icon', 'icon', 'global', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"large\",\"selectionLabel\":\"\"}', '2018-04-03 15:01:41', '2018-04-03 15:01:41', 'bd18d24b-6682-437c-8b67-776b907d1445'),
  (20, 3, 'Booking Panel', 'bookingPanel', 'global', 'Originally supposed to be placed at homepage', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-04 09:17:09', '2018-04-05 11:16:58', 'b6a85192-9f35-4030-b674-0b7110a8397a'),
  (21, NULL, 'Top Caption', 'topCaption', 'matrixBlockType:2', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"100\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-04 09:17:09', '2018-04-05 11:16:58', '9a3907bc-54e7-45a7-9d7f-27a6a241c24e'),
  (22, NULL, 'Tab-1 Title', 'tab1Title', 'matrixBlockType:2', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"50\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-04 09:17:09', '2018-04-05 11:16:58', 'd4129f14-59c0-475a-a16f-595c4d8e4028'),
  (23, NULL, 'Tab-1 Input-1 Caption', 'tab1Input1Caption', 'matrixBlockType:2', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"50\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-04 09:17:09', '2018-04-05 11:16:58', 'ade307a3-4575-477c-98ca-2e23870dbabc'),
  (24, NULL, 'Tab-1 Input-2 Caption', 'tab1Input2Caption', 'matrixBlockType:2', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"50\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-04 09:17:09', '2018-04-05 11:16:58', 'c4b5ce47-34c0-4020-9bef-c8dafdeae1b4'),
  (25, NULL, 'Tab-1 Submit Button Caption', 'tab1SubmitButtonCaption', 'matrixBlockType:2', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"50\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-04 09:17:09', '2018-04-05 11:16:58', '8d5f890f-b022-498e-ba50-e9374348f880'),
  (26, NULL, 'Tab-2 Title', 'tab2Title', 'matrixBlockType:2', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"50\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-04 09:17:09', '2018-04-05 11:16:58', 'd27ce76f-ae54-4e87-ae16-6dd31051d0d0'),
  (27, 3, 'Feedback List', 'feedbackList', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-05 08:29:56', '2018-04-10 12:27:34', '780d0d2e-7185-4cd5-90bc-d80decd301a2'),
  (28, NULL, 'Picture', 'picture', 'matrixBlockType:3', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-05 08:29:57', '2018-04-10 12:27:34', '85032468-d74b-497e-87f9-748bb2c3c2a7'),
  (29, NULL, 'Text', 'text', 'matrixBlockType:3', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-05 08:29:57', '2018-04-10 12:27:34', '4de4c1b9-5733-4ed7-86e2-d9e70c173897'),
  (30, NULL, 'Author', 'author', 'matrixBlockType:3', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"50\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-05 08:29:57', '2018-04-10 12:27:34', 'f96a60d0-84e1-403b-afda-05abc2e12e12'),
  (31, NULL, 'Signature', 'signature', 'matrixBlockType:3', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"200\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-05 08:29:57', '2018-04-10 12:27:34', '9e838a63-faea-470d-826c-b19c9ebe1f4d'),
  (32, 3, 'Advertisement', 'advertisement', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-05 10:36:40', '2018-04-05 11:14:48', '9997f63b-a469-497a-a1d4-2fceb8d34403'),
  (33, NULL, 'CoverPicture', 'coverPicture', 'matrixBlockType:4', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-05 10:36:40', '2018-04-05 11:14:48', '7e5e68ae-0232-42ae-9cc8-9ee60578fcc4'),
  (34, NULL, 'Caption', 'caption', 'matrixBlockType:4', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-05 10:36:40', '2018-04-05 11:14:48', 'abc4db75-85af-4d55-950d-2d99a1871279'),
  (35, NULL, 'Button Text', 'buttonText', 'matrixBlockType:4', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"50\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-05 10:36:40', '2018-04-05 11:14:48', 'a46495b1-fc46-4e6e-889c-a39848f03d6a'),
  (36, NULL, 'Target Url', 'targetUrl', 'matrixBlockType:4', '', 0, 'PlainText', '{\"placeholder\":\"place target URL here\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-05 10:39:40', '2018-04-05 11:14:48', '6a7af816-2378-4645-83c3-04c0be22e328'),
  (37, 3, 'Featured Services', 'featuredServices', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-05 11:28:04', '2018-04-05 11:45:24', '20513222-7887-4b26-ac6f-aa27b48108aa'),
  (38, NULL, 'Block Title', 'blockTitle', 'matrixBlockType:5', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-05 11:28:04', '2018-04-05 11:45:24', 'eba48ecf-0f33-4254-810c-3e5be58f15d0'),
  (39, NULL, 'Caption', 'caption', 'matrixBlockType:5', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-05 11:28:04', '2018-04-05 11:45:24', '8e899cbe-4cb1-4eae-b975-3a1eafb3d59a'),
  (40, NULL, 'Background Color', 'backgroundColor', 'matrixBlockType:5', '', 0, 'Color', NULL, '2018-04-05 11:28:04', '2018-04-05 11:45:24', '806dffa5-feb4-4f7f-9fb7-79f95cc9a8ca'),
  (41, NULL, 'Picture', 'picture', 'matrixBlockType:6', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-05 11:28:04', '2018-04-05 11:45:24', '08566784-7a54-4bce-8cb3-74406fb0826c'),
  (42, NULL, 'Caption', 'caption', 'matrixBlockType:6', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-05 11:28:04', '2018-04-05 11:45:24', '1f6c09a0-e934-41ae-9e06-d3965b8cec92'),
  (43, NULL, 'Text', 'text', 'matrixBlockType:6', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-05 11:28:04', '2018-04-05 11:45:24', '9b5e5942-4e4c-4874-b46d-c47bbd57cd77'),
  (44, 1, 'Page Cover', 'pageCover', 'global', '', 0, 'Matrix', '{\"maxBlocks\":\"1\"}', '2018-04-06 07:57:50', '2018-04-06 08:26:23', '408aa992-3b0e-44a4-a337-a150fa574d80'),
  (45, 1, 'Services List', 'servicesList', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-06 08:08:45', '2018-04-06 08:11:19', 'aaab03b1-4c93-4889-801c-611ab890d263'),
  (46, NULL, 'Call Us Today Caption', 'callUsTodayCaption', 'matrixBlockType:7', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-06 08:08:45', '2018-04-06 08:11:19', '5f892eeb-42de-4eb7-8130-7a965ec2501e'),
  (47, NULL, 'Picture', 'picture', 'matrixBlockType:8', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-06 08:08:45', '2018-04-06 08:11:19', 'b86b3699-26b4-4bc2-b019-145f2fe8277a'),
  (48, NULL, 'Caption', 'caption', 'matrixBlockType:8', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"100\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-06 08:08:45', '2018-04-06 08:11:19', '1fb7344f-5df3-4a40-896f-f2efb705715b'),
  (49, NULL, 'Text', 'text', 'matrixBlockType:8', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-06 08:08:45', '2018-04-06 08:11:19', 'c0301fb8-5903-4c08-bb97-9594244f0e5f'),
  (50, NULL, 'Caption', 'caption', 'matrixBlockType:9', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-06 08:26:23', '2018-04-06 08:26:23', 'c2576b5e-2808-4264-be82-6c506a3c90e8'),
  (51, NULL, 'Picture', 'picture', 'matrixBlockType:9', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-06 08:26:23', '2018-04-06 08:26:23', '8f3f218d-2191-40cb-8691-c1029ca6e138'),
  (52, 1, 'Facilities List', 'facilitiesList', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-06 12:30:25', '2018-04-06 12:30:25', '5ca671f7-0f06-493f-b864-13099e2960f2'),
  (53, NULL, 'Caption', 'caption', 'matrixBlockType:10', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-06 12:30:25', '2018-04-06 12:30:25', 'b01eff74-2a4d-4b10-8b55-14e4e553d621'),
  (54, NULL, 'Text', 'text', 'matrixBlockType:10', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-06 12:30:25', '2018-04-06 12:30:25', '8a76979d-55a2-4c33-a90b-72a648c6861a'),
  (55, NULL, 'Images', 'images', 'matrixBlockType:10', 'originally supposed to be 3 images', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-06 12:30:25', '2018-04-06 12:30:25', '57768ce7-186c-44f0-8afb-1d43d7686f17'),
  (56, 1, 'Trainings List', 'trainingsList', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-09 11:02:39', '2018-04-09 11:03:02', 'b5665124-411c-4c2f-8c5f-d1d2a83f8ed6'),
  (57, NULL, 'Caption', 'caption', 'matrixBlockType:11', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-09 11:02:39', '2018-04-09 11:03:02', '1c33b18a-db4e-4551-8fec-3afe736c214e'),
  (58, NULL, 'Text', 'text', 'matrixBlockType:11', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-09 11:02:39', '2018-04-09 11:03:02', 'ea79aeca-f490-441a-b1f9-e796cf3367ec'),
  (59, NULL, 'Video', 'video', 'matrixBlockType:11', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-09 11:02:39', '2018-04-09 11:03:02', 'a8269f06-5491-40dc-a260-40a801c1ff14'),
  (60, NULL, 'Caption', 'caption', 'matrixBlockType:12', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"100\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-09 11:02:39', '2018-04-09 11:03:02', '50bd047a-3519-4f8e-b232-7f00018b3be3'),
  (61, NULL, 'Text', 'text', 'matrixBlockType:12', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-09 11:02:39', '2018-04-09 11:03:02', '3a963f40-7095-456d-8d43-83916764a2c4'),
  (62, NULL, 'Picture', 'picture', 'matrixBlockType:12', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-09 11:02:39', '2018-04-09 11:03:02', '71a0e19a-77a9-4c51-8b7c-6e914406281b'),
  (63, 6, 'Video Panel', 'videoPanel', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-10 13:13:12', '2018-04-10 13:13:12', 'f445dc5e-f000-40f5-8c22-b8f165d2ab4a'),
  (64, NULL, 'Caption', 'caption', 'matrixBlockType:13', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-10 13:13:12', '2018-04-10 13:13:12', 'b24cea11-cc23-486d-b3d0-4e29e450d898'),
  (65, NULL, 'Text', 'text', 'matrixBlockType:13', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-04-10 13:13:12', '2018-04-10 13:13:12', '136ef44a-d53e-42e5-b04a-04b0d3acc441'),
  (66, NULL, 'Video', 'video', 'matrixBlockType:13', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-10 13:13:12', '2018-04-10 13:13:12', '6138ae6d-cc86-49c2-9c1c-e4204b5c2dd1'),
  (67, 6, 'Maps Field', 'mapsField', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-10 14:41:45', '2018-04-10 14:41:45', '7d03711d-5e6b-4fca-b721-e31e38b9ef38'),
  (68, NULL, 'Caption', 'caption', 'matrixBlockType:14', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-10 14:41:45', '2018-04-10 14:41:45', 'ac01c59f-a25e-485f-8f9b-75f148637c65'),
  (76, 7, 'Cover-Caption-LinkButton', 'coverCaptionLinkbutton', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-16 09:13:21', '2018-04-16 12:41:53', '56c2aa99-a899-42af-8459-1374ec8a18f0'),
  (77, NULL, 'Cover Picture', 'coverPicture', 'matrixBlockType:17', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-04-16 09:13:21', '2018-04-16 12:41:53', '41f7c3ca-f147-41a3-8145-cac0c6b5bef7'),
  (78, NULL, 'Caption', 'caption', 'matrixBlockType:17', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-16 09:13:21', '2018-04-16 12:41:53', '13a84aab-1bbd-41d0-a42b-0a28a7bc6260'),
  (79, NULL, 'SubCaption', 'subcaption', 'matrixBlockType:17', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-16 09:13:21', '2018-04-16 12:41:53', '10901d58-6144-484b-acff-e77d06b66164'),
  (80, NULL, 'Target Link', 'targetLink', 'matrixBlockType:17', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-16 09:13:21', '2018-04-16 12:41:53', 'bae961e9-c7c7-46fc-88d6-6b471ebfb2b6'),
  (82, NULL, 'Button Caption', 'buttonCaption', 'matrixBlockType:17', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-16 12:09:57', '2018-04-16 12:41:53', 'c4a23e5d-c5b3-493e-a349-bc3f5f238579'),
  (83, NULL, 'Size', 'size', 'matrixBlockType:17', '', 0, 'Dropdown', '{\"options\":[{\"label\":\"Full Size\",\"value\":\"fullSize\",\"default\":\"1\"},{\"label\":\"Half Size\",\"value\":\"halfSize\",\"default\":\"\"}]}', '2018-04-16 12:11:52', '2018-04-16 12:41:53', 'd0db4d01-946d-4c19-bc17-27491f77edee'),
  (84, 1, 'Price List', 'priceList', 'global', '', 0, 'Entries', '{\"sources\":[\"section:6\"],\"limit\":\"\",\"selectionLabel\":\"\"}', '2018-04-16 13:19:45', '2018-04-16 13:53:21', 'f5943a84-1d4f-4972-8401-198572e9f98c'),
  (85, 7, 'Priced Items', 'pricedItems', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-04-16 15:17:04', '2018-04-16 15:19:57', '0007e266-5f15-4fdb-b92a-5d998ccc9f81'),
  (86, NULL, 'Is Most Popular', 'isMostPopular', 'matrixBlockType:18', '', 0, 'Lightswitch', '{\"default\":\"\"}', '2018-04-16 15:17:04', '2018-04-16 15:19:57', '33f8a620-4bd3-4131-b1e6-da8c6ef3c599'),
  (87, NULL, 'Caption', 'caption', 'matrixBlockType:18', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"2\"}', '2018-04-16 15:17:04', '2018-04-16 15:19:57', 'fc4687bf-cab6-488d-9408-0efbe3b55ae8'),
  (88, NULL, 'Price Text', 'priceText', 'matrixBlockType:18', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-16 15:17:04', '2018-04-16 15:19:57', '15613e9f-33b8-4cdb-882a-eed95177c917'),
  (89, NULL, 'SubCaption', 'subcaption', 'matrixBlockType:18', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-04-16 15:17:04', '2018-04-16 15:19:57', 'bf81c083-8756-496e-b390-8df8662fba83'),
  (90, NULL, 'Size', 'size', 'matrixBlockType:18', '', 0, 'Dropdown', '{\"options\":[{\"label\":\"Single Sized\",\"value\":\"singleSized\",\"default\":\"1\"},{\"label\":\"Double Sized\",\"value\":\"doubleSized\",\"default\":\"\"}]}', '2018-04-16 15:17:04', '2018-04-16 15:19:57', 'f3203d11-5a54-4c32-9ca8-9e5465204fd2'),
  (91, 7, 'Bottom HTML Markup', 'bottomHtmlMarkup', 'global', '', 0, 'RichText', '{\"configFile\":\"Standard.json\",\"availableAssetSources\":\"*\",\"availableTransforms\":\"*\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"purifierConfig\":\"\",\"columnType\":\"text\"}', '2018-04-16 15:22:53', '2018-04-16 16:15:50', '631a6193-627e-4126-908c-707edbde5e74'),
  (92, 8, 'Storyboard Gallery', 'storyboardGallery', 'global', '', 0, 'Matrix', '{\"maxBlocks\":null}', '2018-05-03 11:07:54', '2018-05-03 11:15:07', 'f2435bb4-3d58-423e-aad8-411ead3bcfdb'),
  (93, NULL, 'Post', 'post', 'matrixBlockType:19', '', 0, 'Entries', '{\"sources\":[\"section:7\"],\"limit\":\"1\",\"selectionLabel\":\"\"}', '2018-05-03 11:07:54', '2018-05-03 11:15:07', '5b478627-db5a-4278-95ec-f36f67a5e3b1'),
  (94, NULL, 'Size', 'size', 'matrixBlockType:19', '', 0, 'Dropdown', '{\"options\":[{\"label\":\"1x1\",\"value\":\"one-one\",\"default\":\"1\"},{\"label\":\"1x2\",\"value\":\"one-two\",\"default\":\"\"},{\"label\":\"2x1\",\"value\":\"two-one\",\"default\":\"\"},{\"label\":\"2x2\",\"value\":\"two-two\",\"default\":\"\"}]}', '2018-05-03 11:07:54', '2018-05-03 11:15:07', 'ef0463b8-aaba-4d0d-add4-11650e54b96c'),
  (95, 9, 'Post', 'post', 'global', '', 0, 'Matrix', '{\"maxBlocks\":\"1\"}', '2018-05-03 11:22:36', '2018-05-03 11:22:36', 'aa97e383-5126-4de6-8e24-a1465b79c562'),
  (96, NULL, 'Caption', 'caption', 'matrixBlockType:20', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-05-03 11:22:36', '2018-05-03 11:22:36', '7b5cb640-4d3c-4be0-9eb6-28aa45bde327'),
  (97, NULL, 'Is Featured', 'isFeatured', 'matrixBlockType:20', '', 0, 'Lightswitch', '{\"default\":\"\"}', '2018-05-03 11:22:36', '2018-05-03 11:22:36', '51a9f338-0f65-4664-b475-e0d1858f2d53'),
  (98, NULL, 'Author', 'author', 'matrixBlockType:20', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"\",\"initialRows\":\"4\"}', '2018-05-03 11:22:37', '2018-05-03 11:22:37', 'fcc87797-0f4c-4e82-b065-f4f87547695b'),
  (99, NULL, 'Publish Date', 'publishDate', 'matrixBlockType:20', '', 0, 'Date', '{\"minuteIncrement\":\"30\",\"showDate\":1,\"showTime\":0}', '2018-05-03 11:22:37', '2018-05-03 11:22:37', '55ac5bf1-40b3-4d01-830d-dee9687ee318'),
  (100, NULL, 'Brief', 'brief', 'matrixBlockType:20', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-05-03 11:22:37', '2018-05-03 11:22:37', '94196a73-a792-4108-8a84-343e3858ce43'),
  (101, NULL, 'Body Content', 'bodyContent', 'matrixBlockType:20', '', 0, 'PlainText', '{\"placeholder\":\"\",\"maxLength\":\"\",\"multiline\":\"1\",\"initialRows\":\"4\"}', '2018-05-03 11:22:37', '2018-05-03 11:22:37', '062f5018-e481-4e3d-9bc3-349e689b1e1d'),
  (102, NULL, 'Picture', 'picture', 'matrixBlockType:20', '', 0, 'Assets', '{\"useSingleFolder\":\"\",\"sources\":\"*\",\"defaultUploadLocationSource\":\"1\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"1\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"limit\":\"1\",\"viewMode\":\"list\",\"selectionLabel\":\"\"}', '2018-05-03 11:22:37', '2018-05-03 11:22:37', 'ba63c543-6ca1-4321-baf6-a65c9ffd3c2e'),
  (103, NULL, 'Post Type', 'postType', 'matrixBlockType:20', '', 0, 'Dropdown', '{\"options\":[{\"label\":\"Blog Post\",\"value\":\"blogPost\",\"default\":\"1\"},{\"label\":\"Press\",\"value\":\"press\",\"default\":\"\"}]}', '2018-05-03 11:22:37', '2018-05-03 11:22:37', 'e91910d5-842e-4270-837f-6d443b9cf660');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_globalsets`
--

CREATE TABLE `craft_globalsets` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_globalsets`
--

INSERT INTO `craft_globalsets` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (4, 'Company Info', 'companyInfo', 11, '2018-04-02 08:37:52', '2018-04-02 09:15:42', '45f326aa-5ec0-4bfe-adf4-bddc2db32159');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_info`
--

CREATE TABLE `craft_info` (
  `id` int(11) NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `edition` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `siteName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `siteUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `timezone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `on` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_info`
--

INSERT INTO `craft_info` (`id`, `version`, `schemaVersion`, `edition`, `siteName`, `siteUrl`, `timezone`, `on`, `maintenance`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, '2.6.3015', '2.6.14', 0, 'Bark And Fly', 'http://localhost:8011', 'UTC', 1, 0, '2018-03-28 09:59:34', '2018-04-09 10:52:30', '7896b4e6-0c05-4e84-bed2-b1d6daba0b41');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_locales`
--

CREATE TABLE `craft_locales` (
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_locales`
--

INSERT INTO `craft_locales` (`locale`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  ('en', 1, '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'f16bf76f-8512-4930-80a9-c1824103d6a8');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixblocks`
--

CREATE TABLE `craft_matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) DEFAULT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `ownerLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixblocks`
--

INSERT INTO `craft_matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `ownerLocale`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (10, 9, 12, 1, 1, NULL, '2018-04-02 15:22:42', '2018-04-05 11:41:21', '36300851-1e92-4d1d-a71b-cc66df3cd3ae'),
  (32, 9, 12, 1, 2, NULL, '2018-04-03 16:32:55', '2018-04-05 11:41:21', '9af35b80-6cbb-4552-b0f3-1c755c83469b'),
  (33, 9, 20, 2, 1, NULL, '2018-04-04 11:01:27', '2018-04-05 11:41:21', '8c19f997-4c94-4a6c-aec2-46b3cefbadbe'),
  (35, 9, 27, 3, 1, NULL, '2018-04-05 10:19:28', '2018-04-05 11:41:21', 'ec648f1e-56da-48e9-861f-bdb6cc1da6dc'),
  (36, 9, 27, 3, 2, NULL, '2018-04-05 10:19:28', '2018-04-05 11:41:21', '0be655c5-ceed-4ea0-9708-913afa82053c'),
  (37, 9, 27, 3, 3, NULL, '2018-04-05 10:19:28', '2018-04-05 11:41:21', '1f4c4096-0b66-4add-aba4-57b019b4b9f1'),
  (39, 9, 32, 4, 1, NULL, '2018-04-05 10:37:37', '2018-04-05 11:41:21', '6dd1b513-e245-419f-9f56-90b5f379e896'),
  (43, 9, 37, 5, 1, NULL, '2018-04-05 11:34:09', '2018-04-05 11:41:21', '8d1eed93-06ac-4e4b-a8eb-1e594917e5a1'),
  (44, 9, 37, 6, 2, NULL, '2018-04-05 11:35:39', '2018-04-05 11:41:21', '1841277e-edcb-430c-a38d-a87746072b8d'),
  (45, 9, 37, 6, 3, NULL, '2018-04-05 11:35:39', '2018-04-05 11:41:21', 'b87bda3b-b277-476b-a110-522f34e7ea5e'),
  (46, 9, 37, 6, 4, NULL, '2018-04-05 11:35:39', '2018-04-05 11:41:21', '68bce06e-df58-4933-a5b3-4fb85c0f10a3'),
  (52, 11, 45, 7, 1, NULL, '2018-04-06 08:10:45', '2018-04-06 09:42:52', '325c46c0-def0-434f-b99a-f9d3ff5de1a5'),
  (53, 11, 44, 9, 1, NULL, '2018-04-06 08:26:49', '2018-04-06 09:42:52', '710edc62-3fa6-47b1-8ec8-e31a730dae8e'),
  (54, 11, 45, 8, 2, NULL, '2018-04-06 08:55:23', '2018-04-06 09:42:52', 'e65b815b-aa83-4b01-96d2-d61f408f6d79'),
  (55, 11, 45, 8, 3, NULL, '2018-04-06 08:55:23', '2018-04-06 09:42:52', 'c289363a-ff57-41f6-b185-e4ad4ed6125f'),
  (56, 11, 45, 8, 4, NULL, '2018-04-06 08:55:23', '2018-04-06 09:42:52', '9682afa0-9999-4153-94d9-dccd927b63b6'),
  (57, 11, 45, 8, 5, NULL, '2018-04-06 08:55:23', '2018-04-06 09:42:52', '81f5d1ed-7ed2-4bf6-acdc-4778be4e1296'),
  (58, 11, 32, 4, 1, NULL, '2018-04-06 09:42:52', '2018-04-06 09:42:52', '09d3015f-3e2d-4f5c-849e-54a0482294b3'),
  (87, 13, 44, 9, 1, NULL, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'ff652485-297f-4b09-8f9d-4fb9e832e47f'),
  (88, 13, 52, 10, 1, NULL, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'a654b125-6b3e-4ac7-9552-cb55dc802263'),
  (89, 13, 52, 10, 2, NULL, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '4d35f471-2167-4e94-97e1-7251a520a1be'),
  (90, 13, 52, 10, 3, NULL, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'bd97467e-7378-4cc3-b0b9-d399b1c708fa'),
  (91, 13, 52, 10, 4, NULL, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '706e0236-808c-4f78-9c61-7b8d9e29545a'),
  (92, 13, 52, 10, 5, NULL, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '12607c32-c9d3-42ba-ac4a-bd1c15ae6290'),
  (93, 13, 52, 10, 6, NULL, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '9bd40c19-c01e-496a-a98c-97c3fc72ebe2'),
  (94, 14, 44, 9, 1, NULL, '2018-04-09 11:04:30', '2018-04-10 07:58:38', 'd18cafd5-7def-4641-b739-4560340c21fc'),
  (100, 14, 56, 11, 1, NULL, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '1209c4db-eac1-4934-a7dd-26e628a63244'),
  (101, 14, 56, 12, 2, NULL, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'ec22cd3b-979e-4fb3-9f02-185e128351af'),
  (102, 14, 56, 12, 3, NULL, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'c0c10c3b-b496-40c5-98ec-0df87e56e750'),
  (103, 14, 56, 12, 4, NULL, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'a375a193-70a4-4afe-8e38-a7374758dbad'),
  (104, 14, 56, 12, 5, NULL, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '2c692e1b-586b-4101-af5e-962189e0e469'),
  (106, 105, 27, 3, 1, NULL, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '7b9657e6-7c57-4dce-899d-9ccd57c5c65c'),
  (107, 105, 27, 3, 2, NULL, '2018-04-10 12:31:27', '2018-04-11 16:53:59', '170caa84-cc05-4c67-9c7a-7ce6aa5fd787'),
  (108, 105, 27, 3, 3, NULL, '2018-04-10 12:31:27', '2018-04-11 16:53:59', 'a2ba681b-7a48-4d44-aad5-6757fa6a2408'),
  (109, 15, 44, 9, 1, NULL, '2018-04-10 14:48:46', '2018-04-10 14:59:28', '03eaf0a5-09e1-41e0-99aa-039065364c98'),
  (111, 15, 63, 13, 1, NULL, '2018-04-10 14:59:28', '2018-04-10 14:59:28', '3697275f-fba3-4819-89d8-8a4a92582fc1'),
  (112, 15, 67, 14, 1, NULL, '2018-04-10 14:59:28', '2018-04-10 14:59:28', 'ea56fbd2-ff0c-41e9-9a9b-47c48c8354d1'),
  (113, 15, 67, 14, 2, NULL, '2018-04-10 14:59:28', '2018-04-10 14:59:28', '7612d955-a2fa-4834-9f40-6c5b32849151'),
  (114, 12, 44, 9, 1, NULL, '2018-04-16 09:14:17', '2018-04-16 14:10:02', '89411781-94b7-4f1f-b8de-65b6b12a4610'),
  (124, 12, 76, 17, 1, NULL, '2018-04-16 12:08:50', '2018-04-16 14:10:02', 'ecb9c3f9-0d66-433c-b699-b1f1af282e28'),
  (125, 12, 76, 17, 2, NULL, '2018-04-16 12:08:50', '2018-04-16 14:10:02', '08f0a047-5ef2-4e76-a0b0-abc06186496d'),
  (127, 12, 76, 17, 3, NULL, '2018-04-16 12:13:44', '2018-04-16 14:10:02', '5ba822ac-6eb4-4d1e-86d9-7c55664250e1'),
  (146, 128, 85, 18, 1, NULL, '2018-04-16 15:18:32', '2018-04-16 16:08:23', '1d09135f-cc67-438f-abe5-ae1414e1dcdb'),
  (147, 128, 85, 18, 4, NULL, '2018-04-16 15:21:14', '2018-04-16 16:08:23', 'd6321c81-c252-455b-88ed-5eb9636ac05e'),
  (148, 128, 85, 18, 2, NULL, '2018-04-16 15:21:14', '2018-04-16 16:08:23', 'dd3b4759-520e-464a-91d8-13d45f39d6b0'),
  (149, 128, 85, 18, 3, NULL, '2018-04-16 15:21:14', '2018-04-16 16:08:23', '76614f02-7c22-4e39-a31f-0ab0c15d2a40'),
  (150, 128, 85, 18, 5, NULL, '2018-04-16 15:21:14', '2018-04-16 16:08:23', '69c1ef10-cff1-4ce2-9832-ce7d2ef15c76'),
  (154, 135, 85, 18, 1, NULL, '2018-04-16 15:26:48', '2018-04-16 15:26:48', '41c93548-26cb-43ba-a2d2-41fa0bd93074'),
  (155, 135, 85, 18, 2, NULL, '2018-04-16 15:26:48', '2018-04-16 15:26:48', 'fdaacaaa-525f-49df-ab23-32f480e45e6f'),
  (156, 135, 85, 18, 3, NULL, '2018-04-16 15:26:48', '2018-04-16 15:26:48', 'ed5c4c70-2f1d-43f7-800d-2d6c90993275'),
  (157, 140, 85, 18, 1, NULL, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '0badaa7f-e1ba-453d-b15b-e9872755b505'),
  (158, 140, 85, 18, 2, NULL, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '87836378-39eb-42f1-991e-d5209b4a1ebe'),
  (159, 140, 85, 18, 3, NULL, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '248c3c8b-ddf0-4bdb-ac9a-f31f02ef269f'),
  (160, 140, 85, 18, 4, NULL, '2018-04-16 15:28:12', '2018-04-16 16:19:43', '4bc51a46-de11-4bcf-87c8-9f41595cbb10'),
  (162, 16, 44, 9, 1, NULL, '2018-04-30 17:51:42', '2018-04-30 17:51:42', 'b7e3560a-0cc6-48c6-9fdf-38aabaaefc32');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixblocktypes`
--

CREATE TABLE `craft_matrixblocktypes` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixblocktypes`
--

INSERT INTO `craft_matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 12, 74, 'Carousel Block', 'carouselBlock', 1, '2018-04-02 14:51:43', '2018-04-04 08:20:04', 'c6fd41bb-4198-4d78-b3e3-901da46bff47'),
  (2, 20, 91, 'Panel', 'panel', 1, '2018-04-04 09:17:09', '2018-04-05 11:16:58', 'dd19b8fd-8238-4344-8bca-60a18a0a1128'),
  (3, 27, 134, 'Feedback Block', 'feedbackBlock', 1, '2018-04-05 08:29:57', '2018-04-10 12:27:34', '75cbd91a-d2ba-407f-a0e6-6027656ae96b'),
  (4, 32, 90, 'Ad Block', 'adBlock', 1, '2018-04-05 10:36:40', '2018-04-05 11:14:48', 'c08d480c-3461-4f8d-8068-4eb2ec778160'),
  (5, 37, 103, 'Services Block', 'servicesBlock', 1, '2018-04-05 11:28:04', '2018-04-05 11:45:24', 'b831f715-7d84-4077-9cae-df0d88f54dcd'),
  (6, 37, 104, 'Service Item', 'serviceItem', 2, '2018-04-05 11:28:04', '2018-04-05 11:45:24', '34768f00-6040-4de6-b15f-49191c39dc8f'),
  (7, 45, 112, 'General Block', 'generalBlock', 1, '2018-04-06 08:08:45', '2018-04-06 08:11:19', '2f7fef33-301a-4b20-88d5-62e17217766f'),
  (8, 45, 113, 'Service Block', 'serviceBlock', 2, '2018-04-06 08:08:45', '2018-04-06 08:11:19', '68c32545-118f-44a6-8830-48b260cce4ef'),
  (9, 44, 114, 'coverBlock', 'coverblock', 1, '2018-04-06 08:26:23', '2018-04-06 08:26:23', '1d794810-ead3-497f-a6fb-3a0e4e813e9a'),
  (10, 52, 118, 'Facility Block', 'facilityBlock', 1, '2018-04-06 12:30:25', '2018-04-06 12:30:25', '2242a37e-c8f0-4b21-bf6a-dc1df999630d'),
  (11, 56, 125, 'General Block', 'generalBlock', 1, '2018-04-09 11:02:39', '2018-04-09 11:03:02', 'da58344f-3c3e-4d32-9318-68a1d022bd0f'),
  (12, 56, 126, 'TrainingsBlock', 'trainingsblock', 2, '2018-04-09 11:02:39', '2018-04-09 11:03:02', 'da5e94cd-aa9f-4cf0-90b5-207187f65c0f'),
  (13, 63, 136, 'Video Block', 'videoBlock', 1, '2018-04-10 13:13:12', '2018-04-10 13:13:12', '266c0009-ad4e-41a9-9de1-f1cee0f8ad79'),
  (14, 67, 137, 'MapBlock', 'mapblock', 1, '2018-04-10 14:41:45', '2018-04-10 14:41:45', 'f30011b8-2f66-4c05-8db6-fc7bf87d6767'),
  (17, 76, 156, 'Item Block', 'itemBlock', 1, '2018-04-16 09:13:21', '2018-04-16 12:41:53', '0dbe466e-add3-4eed-bd7d-7e0e26714b64'),
  (18, 85, 166, 'Price Block', 'priceBlock', 1, '2018-04-16 15:17:04', '2018-04-16 15:19:57', 'a4a5cdfa-060f-4c03-a973-9874ca422930'),
  (19, 92, 174, 'Main Block', 'mainBlock', 1, '2018-05-03 11:07:54', '2018-05-03 11:15:07', 'f5992339-ed9a-4bda-8de2-ec94d97ab316'),
  (20, 95, 175, 'Main Block', 'mainBlock', 1, '2018-05-03 11:22:36', '2018-05-03 11:22:37', 'af379743-1307-4bb5-8cfb-44ea292acf72');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_advertisement`
--

CREATE TABLE `craft_matrixcontent_advertisement` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_adBlock_caption` text COLLATE utf8_unicode_ci,
  `field_adBlock_buttonText` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_adBlock_targetUrl` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_advertisement`
--

INSERT INTO `craft_matrixcontent_advertisement` (`id`, `elementId`, `locale`, `field_adBlock_caption`, `field_adBlock_buttonText`, `field_adBlock_targetUrl`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 39, 'en', 'Call us today: 01234 567 890', 'Order form', 'https://google.com', '2018-04-05 10:37:37', '2018-04-05 11:41:21', 'e0a959c1-2211-4f51-885a-4c0a92f0024a'),
  (2, 58, 'en', 'Call us today: 01234 567 890', 'Order form', 'https://google.com', '2018-04-06 09:42:52', '2018-04-06 09:42:52', '717e529c-381a-4c8a-a78b-bf4d331f3040');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_bookingpanel`
--

CREATE TABLE `craft_matrixcontent_bookingpanel` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_panel_topCaption` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_panel_tab1Title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_panel_tab1Input1Caption` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_panel_tab1Input2Caption` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_panel_tab1SubmitButtonCaption` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_panel_tab2Title` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_bookingpanel`
--

INSERT INTO `craft_matrixcontent_bookingpanel` (`id`, `elementId`, `locale`, `field_panel_topCaption`, `field_panel_tab1Title`, `field_panel_tab1Input1Caption`, `field_panel_tab1Input2Caption`, `field_panel_tab1SubmitButtonCaption`, `field_panel_tab2Title`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 33, 'en', 'Start your booking', 'Dog Boarding', 'Start', 'End', 'Book now', 'Dog Day Care ', '2018-04-04 11:01:27', '2018-04-05 11:41:21', 'd84175d1-a730-4a10-8439-c08f81a0fe27');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_covercaptionlinkbutton`
--

CREATE TABLE `craft_matrixcontent_covercaptionlinkbutton` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_itemBlock_caption` text COLLATE utf8_unicode_ci,
  `field_itemBlock_subcaption` text COLLATE utf8_unicode_ci,
  `field_itemBlock_targetLink` text COLLATE utf8_unicode_ci,
  `field_itemBlock_buttonCaption` text COLLATE utf8_unicode_ci,
  `field_itemBlock_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'fullSize',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_covercaptionlinkbutton`
--

INSERT INTO `craft_matrixcontent_covercaptionlinkbutton` (`id`, `elementId`, `locale`, `field_itemBlock_caption`, `field_itemBlock_subcaption`, `field_itemBlock_targetLink`, `field_itemBlock_buttonCaption`, `field_itemBlock_size`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 124, 'en', 'Download our Pricing', '', 'http://www.pdf995.com/samples/pdf.pdf', 'Download', 'fullSize', '2018-04-16 12:08:50', '2018-04-16 14:10:02', '4786ae1d-abaf-49bb-9164-dc341c114420'),
  (2, 125, 'en', 'Dog Boarding', '01234 567 890', 'https://google.com', 'Learn more', 'halfSize', '2018-04-16 12:08:50', '2018-04-16 14:10:02', 'ed412ac8-1424-4311-9f37-43bb28d17b3d'),
  (4, 127, 'en', 'Dog Taxi', '01234 567 890', 'https://google.com', 'Learn more', 'halfSize', '2018-04-16 12:13:44', '2018-04-16 14:10:02', '8d3bc37b-8656-4a3f-862d-10c740d9bcfa');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_facilitieslist`
--

CREATE TABLE `craft_matrixcontent_facilitieslist` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_facilityBlock_caption` text COLLATE utf8_unicode_ci,
  `field_facilityBlock_text` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_facilitieslist`
--

INSERT INTO `craft_matrixcontent_facilitieslist` (`id`, `elementId`, `locale`, `field_facilityBlock_caption`, `field_facilityBlock_text`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 88, 'en', 'Little Dog Pen', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.', '2018-04-06 12:37:18', '2018-04-06 12:37:18', '18c9ada7-ba18-408b-b30f-74c4e627c61d'),
  (2, 89, 'en', 'Big Dog Pen', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.', '2018-04-06 12:37:18', '2018-04-06 12:37:18', '673919be-52af-4601-af87-e1cc45c3db92'),
  (3, 90, 'en', 'Indoor Arena', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.', '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'f74589c8-b706-4a45-b4c0-1893e5f1c3ca'),
  (4, 91, 'en', 'Little Dog Field', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.', '2018-04-06 12:37:18', '2018-04-06 12:37:18', '18c0873a-d2f0-4f07-b2c8-041ab59cd567'),
  (5, 92, 'en', 'Big Dog Field', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.', '2018-04-06 12:37:18', '2018-04-06 12:37:18', '2b38461a-1fbe-4ab5-81ea-8bad9fb25c57'),
  (6, 93, 'en', 'Boarding Kennel', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero.', '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'd1c86f29-0b53-4714-8f9c-ee93c2c1e6a9');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_featuredservices`
--

CREATE TABLE `craft_matrixcontent_featuredservices` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_servicesBlock_blockTitle` text COLLATE utf8_unicode_ci,
  `field_servicesBlock_caption` text COLLATE utf8_unicode_ci,
  `field_servicesBlock_backgroundColor` char(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_serviceItem_caption` text COLLATE utf8_unicode_ci,
  `field_serviceItem_text` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_featuredservices`
--

INSERT INTO `craft_matrixcontent_featuredservices` (`id`, `elementId`, `locale`, `field_servicesBlock_blockTitle`, `field_servicesBlock_caption`, `field_servicesBlock_backgroundColor`, `field_serviceItem_caption`, `field_serviceItem_text`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 43, 'en', 'Your best friend deserves the best care.', 'At Bark n’ Fly we specialise in giving dogs more. Established in 2009 we provide premium dog boarding and dog day care throughout Edinburgh & the surrounding area.', '#6cb55a', NULL, NULL, '2018-04-05 11:34:09', '2018-04-05 11:41:21', '8f70c085-53a1-426d-9024-5fa8d7d433f3'),
  (2, 44, 'en', NULL, NULL, NULL, 'Pick ups and Drop Offs', 'We can safely and securely collect or return your furry friend. We even have a handy SMS service, notifying you directly to your mobile phone about pick ups or drop offs if you’re away from home.', '2018-04-05 11:35:39', '2018-04-05 11:41:21', 'd441e77f-4d18-40ea-96d6-3f6efa024c3f'),
  (3, 45, 'en', NULL, NULL, NULL, 'Spacious & Safe Playareas', 'Under the watchful eye of our experienced, fully insured and police vetted staff, your dog will love running around in our two outdoor exercise areas and dry indoor arena, keeping paws busy and tails wagging!', '2018-04-05 11:35:39', '2018-04-05 11:41:21', 'b825f329-056a-497b-ae6c-981b7cd847dd'),
  (4, 46, 'en', NULL, NULL, NULL, 'Dog Training', 'Our dog training service is there to help with any problems you might be experiencing with your dog. From every day obedience to behaviour issues, all the way to competition training and other specialised types of work.', '2018-04-05 11:35:39', '2018-04-05 11:41:21', '1cb2e27a-ba5a-466c-8039-df7adcb5ffe4');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_feedbacklist`
--

CREATE TABLE `craft_matrixcontent_feedbacklist` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_feedbackBlock_text` text COLLATE utf8_unicode_ci,
  `field_feedbackBlock_author` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_feedbackBlock_signature` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_feedbacklist`
--

INSERT INTO `craft_matrixcontent_feedbacklist` (`id`, `elementId`, `locale`, `field_feedbackBlock_text`, `field_feedbackBlock_author`, `field_feedbackBlock_signature`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 35, 'en', 'I love coming to Bark n’ Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I’m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I’m ready for cuddles and belly rubs!\r\n\r\n                            ', 'Eddie', 'Border Collie & Day Care Guest.\r\nHuman friend: Frances', '2018-04-05 10:19:28', '2018-04-05 11:41:21', 'd2e76c2b-cf03-461c-8127-ffaba4c9e74d'),
  (2, 36, 'en', 'I love coming to Bark n’ Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I’m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I’m ready for cuddles and belly rubs!', 'Eddie', 'Border Collie & Day Care Guest.\r\nHuman friend: Frances', '2018-04-05 10:19:28', '2018-04-05 11:41:21', 'eee4f18c-f27e-48fd-bff0-e2f156146c23'),
  (3, 37, 'en', 'I love coming to Bark n’ Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I’m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I’m ready for cuddles and belly rubs!', 'Eddie', 'Border Collie & Day Care Guest.\r\nHuman friend: Frances', '2018-04-05 10:19:28', '2018-04-05 11:41:21', 'd9e3839e-727b-4b8f-890a-548ee9276b83'),
  (4, 106, 'en', 'I love coming to Bark n’ Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I’m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I’m ready for cuddles and belly rubs!', 'Eddie', 'Border Collie & Day Care Guest.\r\nHuman friend: Frances', '2018-04-10 12:31:27', '2018-04-11 16:53:59', '2206fcee-2daf-4b2f-b453-1ea5448398d1'),
  (5, 107, 'en', 'I love coming to Bark n’ Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I’m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I’m ready for cuddles and belly rubs!', 'Dexter', 'Border Collie & Day Care Guest.\r\nHuman friend: Frances', '2018-04-10 12:31:27', '2018-04-11 16:53:59', '99950c30-3d01-48b2-8b3f-c60aaefa73d9'),
  (6, 108, 'en', 'I love coming to Bark n’ Fly every school day. I get to see all my friends, run about the big field chasing butterflies and sometimes my tail too. The human helpers here are very kind to me and give me delicious healthy treats if I’m a good boy. In the afternoon I get to play indoors and by the time I see my human friend at home time, I’m ready for cuddles and belly rubs!', 'George', 'Border Collie & Day Care Guest.\r\nHuman friend: Frances', '2018-04-10 12:31:27', '2018-04-11 16:53:59', '859185e7-a0e4-410e-82d8-7a531ce67b0e');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_mapsfield`
--

CREATE TABLE `craft_matrixcontent_mapsfield` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_mapblock_caption` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_mapsfield`
--

INSERT INTO `craft_matrixcontent_mapsfield` (`id`, `elementId`, `locale`, `field_mapblock_caption`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 112, 'en', 'Servicing the Nation', '2018-04-10 14:59:28', '2018-04-10 14:59:28', '09841109-f982-4878-9568-5d641b35e201'),
  (2, 113, 'en', 'Pick up & Drop off Locations', '2018-04-10 14:59:28', '2018-04-10 14:59:28', '9b734e50-d665-42e4-b40b-417235fae7a0');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_pagecover`
--

CREATE TABLE `craft_matrixcontent_pagecover` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_coverblock_caption` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_pagecover`
--

INSERT INTO `craft_matrixcontent_pagecover` (`id`, `elementId`, `locale`, `field_coverblock_caption`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 53, 'en', 'Our Services', '2018-04-06 08:26:49', '2018-04-06 09:42:52', 'f181baea-1ca3-447c-9b15-60a99a2f9b84'),
  (2, 87, 'en', 'Our Facilities', '2018-04-06 12:37:18', '2018-04-06 12:37:18', '7339d73c-7337-4b1a-872f-4bd0e8d47457'),
  (3, 94, 'en', 'Training', '2018-04-09 11:04:30', '2018-04-10 07:58:38', 'ef4aeacf-75c2-4928-a002-273ffc85a884'),
  (4, 109, 'en', 'About Us', '2018-04-10 14:48:46', '2018-04-10 14:59:28', '1e694f8b-e435-4fa3-bb33-c8189544ba84'),
  (5, 114, 'en', 'Pricing', '2018-04-16 09:14:17', '2018-04-16 14:10:02', 'f36a23a5-6dac-4d8c-a0aa-50d76e6a663a'),
  (6, 162, 'en', 'Pupparazzi', '2018-04-30 17:51:42', '2018-04-30 17:51:42', '09eff58e-a8e5-4600-89b5-5940039abbdb');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_post`
--

CREATE TABLE `craft_matrixcontent_post` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_mainBlock_caption` text COLLATE utf8_unicode_ci,
  `field_mainBlock_isFeatured` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `field_mainBlock_author` text COLLATE utf8_unicode_ci,
  `field_mainBlock_publishDate` datetime DEFAULT NULL,
  `field_mainBlock_brief` text COLLATE utf8_unicode_ci,
  `field_mainBlock_bodyContent` text COLLATE utf8_unicode_ci,
  `field_mainBlock_postType` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'blogPost',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_priceditems`
--

CREATE TABLE `craft_matrixcontent_priceditems` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_priceBlock_isMostPopular` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `field_priceBlock_caption` text COLLATE utf8_unicode_ci,
  `field_priceBlock_priceText` text COLLATE utf8_unicode_ci,
  `field_priceBlock_subcaption` text COLLATE utf8_unicode_ci,
  `field_priceBlock_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'singleSized',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_priceditems`
--

INSERT INTO `craft_matrixcontent_priceditems` (`id`, `elementId`, `locale`, `field_priceBlock_isMostPopular`, `field_priceBlock_caption`, `field_priceBlock_priceText`, `field_priceBlock_subcaption`, `field_priceBlock_size`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 146, 'en', 0, 'Full day', '£22.50', '', 'singleSized', '2018-04-16 15:18:32', '2018-04-16 16:08:23', '980d75a6-6d52-4926-b944-b08da45ab8e4'),
  (2, 147, 'en', 1, 'Half day\r\n(5 hours or less)', '£20.30', '', 'singleSized', '2018-04-16 15:21:14', '2018-04-16 16:08:23', '2d305fc1-cd83-4795-b08b-047d8ad5e9ba'),
  (3, 148, 'en', 0, 'Full day for two dogs\r\n(same household)', '£22.30', '', 'singleSized', '2018-04-16 15:21:14', '2018-04-16 16:08:23', '118149ff-1745-4a3f-8e17-eeafe2bcb025'),
  (4, 149, 'en', 0, 'Full day\r\n(more than 15 days per month)', '£37.50', '', 'singleSized', '2018-04-16 15:21:14', '2018-04-16 16:08:23', '7db12bbc-c2c6-46e8-9268-a56798b251c0'),
  (5, 150, 'en', 0, 'Pick up & drop off', '£3.00 per dog', '(£1.50 for each additional dog)', 'doubleSized', '2018-04-16 15:21:14', '2018-04-16 16:08:23', 'af3292da-2ca4-4cce-92b8-ba3e77bc6fae'),
  (9, 154, 'en', 0, 'Per night', '£26.00', '', 'singleSized', '2018-04-16 15:26:48', '2018-04-16 15:26:48', 'a116a5e2-3237-44a8-8fcd-f6d83c22ca94'),
  (10, 155, 'en', 1, 'Two dogs\r\n(same household)', '£37.00', '', 'singleSized', '2018-04-16 15:26:48', '2018-04-16 15:26:48', 'f803c352-eef4-448f-8858-fdd9b473a7b9'),
  (11, 156, 'en', 0, 'After mid-day collection', '£18.50', '', 'singleSized', '2018-04-16 15:26:48', '2018-04-16 15:26:48', '4d2d8d77-b148-47b7-8855-7468c5a7994a'),
  (12, 157, 'en', 0, 'One-on-one session\r\n(at centre).', '£95.00', '', 'singleSized', '2018-04-16 15:28:12', '2018-04-16 16:19:43', 'ad52300d-9529-4e38-a0a6-f21c342d3adb'),
  (13, 158, 'en', 1, 'Subsequent session\r\n(client residence)', '£65.00', '', 'singleSized', '2018-04-16 15:28:12', '2018-04-16 16:19:43', '8a8dac42-5e00-467d-bdc0-d2997d7ab1da'),
  (14, 159, 'en', 0, 'One-on-one session\r\n(client residence)', '£120.00', '', 'singleSized', '2018-04-16 15:28:12', '2018-04-16 16:19:43', 'bbb07345-a73f-4b4e-87bf-2779dc5fc121'),
  (15, 160, 'en', 0, 'Subsequent session\r\n(at centre)', '£50.00', '(per 30 minutes)', 'singleSized', '2018-04-16 15:28:12', '2018-04-16 16:19:43', 'edbe94f0-4a6d-4c50-ba56-223f129ec59b');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_serviceslist`
--

CREATE TABLE `craft_matrixcontent_serviceslist` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_generalBlock_callUsTodayCaption` text COLLATE utf8_unicode_ci,
  `field_serviceBlock_caption` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_serviceBlock_text` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_serviceslist`
--

INSERT INTO `craft_matrixcontent_serviceslist` (`id`, `elementId`, `locale`, `field_generalBlock_callUsTodayCaption`, `field_serviceBlock_caption`, `field_serviceBlock_text`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 52, 'en', 'Call us today: 01234 567 890', NULL, NULL, '2018-04-06 08:10:45', '2018-04-06 09:42:52', '5003e4d6-5da3-41cf-92c0-6e1bcdd0364c'),
  (2, 54, 'en', NULL, 'Dog Day Care', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.', '2018-04-06 08:55:23', '2018-04-06 09:42:52', '41fd1964-2468-41fa-a7d5-b656b986eae5'),
  (3, 55, 'en', NULL, 'Dog Boarding', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.', '2018-04-06 08:55:23', '2018-04-06 09:42:52', 'b90c4492-7a71-42ff-a914-1465d41ed375'),
  (4, 56, 'en', NULL, 'Pick up & Drop off', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.', '2018-04-06 08:55:23', '2018-04-06 09:42:52', '68ae0010-2988-48aa-8755-396ce55ba451'),
  (5, 57, 'en', NULL, 'Dog Training', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex.\r\n\r\nFusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui.', '2018-04-06 08:55:23', '2018-04-06 09:42:52', '07e04bc1-8e4e-4d8f-9951-1ba713a6f569');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_storyboardgallery`
--

CREATE TABLE `craft_matrixcontent_storyboardgallery` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_mainBlock_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'one-one',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_topcarousel`
--

CREATE TABLE `craft_matrixcontent_topcarousel` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_carouselBlock_caption` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_topcarousel`
--

INSERT INTO `craft_matrixcontent_topcarousel` (`id`, `elementId`, `locale`, `field_carouselBlock_caption`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 10, 'en', 'At Bark n’ Fly, we treat your dog\r\nas if it were our own', '2018-04-02 15:22:42', '2018-04-05 11:41:21', '5da26fe3-74c0-4a4a-b79f-c828184f2efd'),
  (2, 32, 'en', 'At Bark n’ Fly, we treat your dog\r\nas if it were our own', '2018-04-03 16:32:55', '2018-04-05 11:41:21', '90d5f2f3-ab4b-426e-b638-33dcd9a6b8e9');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_trainingslist`
--

CREATE TABLE `craft_matrixcontent_trainingslist` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_generalBlock_caption` text COLLATE utf8_unicode_ci,
  `field_generalBlock_text` text COLLATE utf8_unicode_ci,
  `field_trainingsblock_caption` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field_trainingsblock_text` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_trainingslist`
--

INSERT INTO `craft_matrixcontent_trainingslist` (`id`, `elementId`, `locale`, `field_generalBlock_caption`, `field_generalBlock_text`, `field_trainingsblock_caption`, `field_trainingsblock_text`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 100, 'en', 'Dog Training, Behaviour Correction and Puppy Education', 'Fusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta leo sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex tincidunt nibhtempor at leo.', NULL, NULL, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '33651d72-170c-4c50-87b4-a82d60bcb242'),
  (2, 101, 'en', NULL, NULL, 'Practical Obedience', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor ateget mi.', '2018-04-10 07:58:38', '2018-04-10 07:58:38', '7826a51f-8929-42e9-baea-209ebf32ab0d'),
  (3, 102, 'en', NULL, NULL, 'Puppy Education', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor ateget mi.', '2018-04-10 07:58:38', '2018-04-10 07:58:38', '9127543a-ae16-4b2d-84fc-6eea48d11316'),
  (4, 103, 'en', NULL, NULL, 'Behaviour Correction', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor ateget mi.', '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'e05d0e8e-7e0f-40f3-a64f-1f46c5c71506'),
  (5, 104, 'en', NULL, NULL, 'Activity Based', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor at leo nec, laoreet blandit ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum viverra diam a nisi vestibulum porta ac eget mi. Fusce risus ipsum, tempor ateget mi.', '2018-04-10 07:58:38', '2018-04-10 07:58:38', '8ce8ea27-df25-4e1b-8530-ae89cf6c3bfe');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_matrixcontent_videopanel`
--

CREATE TABLE `craft_matrixcontent_videopanel` (
  `id` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `field_videoBlock_caption` text COLLATE utf8_unicode_ci,
  `field_videoBlock_text` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_matrixcontent_videopanel`
--

INSERT INTO `craft_matrixcontent_videopanel` (`id`, `elementId`, `locale`, `field_videoBlock_caption`, `field_videoBlock_text`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 111, 'en', 'Bark n’ Fly', 'Fusce viverra, mauris eu pulvinar mattis, ligula justo viverra purus, non tristique ante ipsum a justo. Donec nec porta leo sapien. Maecenas pulvinar porttitor tincidunt. Ut arcu tellus, ornare sed magna sit amet, eleifend pulvinar erat. Vivamus suscipit, nulla eu pharetra placerat, leo leo semper ligula, vel gravida erat justo quis dui. Vestibulum tellus magna, pretium ut congue sit amet, tristique sit amet leo. Mauris eros enim, vestibulum at mollis at, tempor ut libero. Pellentesque tincidunt nibh et neque eleifend convallis. Fusce nisi orci, tincidunt at libero et, molestie convallis. Fusce nisi orci, tincidunt at libero et, molestie gravida ex tincidunt nibhtempor at leo.', '2018-04-10 14:59:28', '2018-04-10 14:59:28', 'a9ec7ee4-bb4d-457b-91c9-35ce9e0f8081');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_migrations`
--

CREATE TABLE `craft_migrations` (
  `id` int(11) NOT NULL,
  `pluginId` int(11) DEFAULT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_migrations`
--

INSERT INTO `craft_migrations` (`id`, `pluginId`, `version`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, NULL, 'm000000_000000_base', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'bc80f5d1-c0d5-4307-83ee-5373594488f9'),
  (2, NULL, 'm140730_000001_add_filename_and_format_to_transformindex', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'f307fc57-0fa7-4c3d-8033-d4d8d7668d37'),
  (3, NULL, 'm140815_000001_add_format_to_transforms', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '9489d177-66ae-48c8-9e5e-705f70401b3a'),
  (4, NULL, 'm140822_000001_allow_more_than_128_items_per_field', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'b1f983b5-4c2d-4c6e-a3d6-37a2b912b33b'),
  (5, NULL, 'm140829_000001_single_title_formats', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '055675a2-7adc-45e3-b3be-739ee8f51c37'),
  (6, NULL, 'm140831_000001_extended_cache_keys', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '10191aef-4920-41c7-be70-66b339f50dde'),
  (7, NULL, 'm140922_000001_delete_orphaned_matrix_blocks', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'fd8211de-06ae-4c6b-afe3-b069f3915f8c'),
  (8, NULL, 'm141008_000001_elements_index_tune', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'c60bd79a-2d17-47d9-babd-c6c6688815ee'),
  (9, NULL, 'm141009_000001_assets_source_handle', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'f442d138-d4e4-4f2f-9b8b-7ef8b2a6d2ee'),
  (10, NULL, 'm141024_000001_field_layout_tabs', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'b8d20f3d-8a61-4b50-854e-8a2b2cb6ddbb'),
  (11, NULL, 'm141030_000000_plugin_schema_versions', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '89fb46d9-358f-47b9-baa3-49200a3af65f'),
  (12, NULL, 'm141030_000001_drop_structure_move_permission', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '6e7d40c5-ffc7-4116-bdb0-91bfae2767b1'),
  (13, NULL, 'm141103_000001_tag_titles', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '32cbdf1e-456a-4da5-b108-f71a3d80bdf8'),
  (14, NULL, 'm141109_000001_user_status_shuffle', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'df88a709-1d04-4fbd-962f-cc68bfff814e'),
  (15, NULL, 'm141126_000001_user_week_start_day', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '95021546-03f3-4a06-8eca-9e2161c95e94'),
  (16, NULL, 'm150210_000001_adjust_user_photo_size', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '0f106fc4-6bca-48c0-849f-6aa7e06af4da'),
  (17, NULL, 'm150724_000001_adjust_quality_settings', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'd7f34ca9-cdfd-433e-aad4-59408e0211ff'),
  (18, NULL, 'm150827_000000_element_index_settings', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '5bfbbb0b-2f73-4c45-b9f7-b286247c1739'),
  (19, NULL, 'm150918_000001_add_colspan_to_widgets', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '13d4f0c3-f823-4624-9748-86fef8c1dc9f'),
  (20, NULL, 'm151007_000000_clear_asset_caches', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '772e6d00-6d77-4e71-91f6-7b330f2a8e89'),
  (21, NULL, 'm151109_000000_text_url_formats', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'fc207c99-e47f-4dce-8a7d-3795ce3f5558'),
  (22, NULL, 'm151110_000000_move_logo', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '3c462b6e-d538-4ab2-a29c-3af03d757cef'),
  (23, NULL, 'm151117_000000_adjust_image_widthheight', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '69a2faba-a8b0-4de6-bdc7-0c2eb90c3a3a'),
  (24, NULL, 'm151127_000000_clear_license_key_status', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'ce3b2d74-593d-4ba8-8610-0eb00a9b6539'),
  (25, NULL, 'm151127_000000_plugin_license_keys', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '229b80ea-ff34-453b-9950-5afd4fd5214a'),
  (26, NULL, 'm151130_000000_update_pt_widget_feeds', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '9156665a-c1fe-44c0-82cf-cdf59c5d622a'),
  (27, NULL, 'm160114_000000_asset_sources_public_url_default_true', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'fb1c1ffc-058b-4f92-b2f9-50a018a761da'),
  (28, NULL, 'm160223_000000_sortorder_to_smallint', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'b443e80d-d5b2-4e0b-815e-8b057d9d3ec2'),
  (29, NULL, 'm160229_000000_set_default_entry_statuses', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'c8286cfc-42ce-47af-a7ba-33f63d6fbf74'),
  (30, NULL, 'm160304_000000_client_permissions', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'e4d5d286-f4e1-4fc8-9228-9614744eaaad'),
  (31, NULL, 'm160322_000000_asset_filesize', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '1f4547b3-bee6-483a-a622-a9a16485a8cd'),
  (32, NULL, 'm160503_000000_orphaned_fieldlayouts', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '21d5354a-ee38-40a7-9d13-f14218eb6246'),
  (33, NULL, 'm160510_000000_tasksettings', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'f47ba42f-9be4-4b6c-8299-6e56b72c4899'),
  (34, NULL, 'm160829_000000_pending_user_content_cleanup', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'd97837ff-790b-4a7f-b2d2-68b0bfa78b16'),
  (35, NULL, 'm160830_000000_asset_index_uri_increase', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'bf4e6bc0-300f-4426-b7f1-8fd9025fb1b1'),
  (36, NULL, 'm160919_000000_usergroup_handle_title_unique', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '88168c36-044e-42d6-b562-ae489fc58216'),
  (37, NULL, 'm161108_000000_new_version_format', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '0e6362a3-3dcc-4ca2-a5e0-30c98c9edff6'),
  (38, NULL, 'm161109_000000_index_shuffle', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'b682748f-c98e-41e2-85b1-4f653bebb089'),
  (39, NULL, 'm170612_000000_route_index_shuffle', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'f4df8148-40a0-42d0-b6f5-f6db73f826e1'),
  (40, NULL, 'm171107_000000_assign_group_permissions', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2b9b937a-a506-4405-ac11-4ca1c1f1d2b2'),
  (41, NULL, 'm171117_000001_templatecache_index_tune', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '070591a3-198e-4021-9f42-e632538e9ed5'),
  (42, NULL, 'm171204_000001_templatecache_index_tune_deux', '2018-03-28 09:59:34', '2018-03-28 09:59:34', '2018-03-28 09:59:34', 'e4f4a128-3f0f-47a0-b758-2df92e6a8372'),
  (43, NULL, 'm180406_000000_pro_upgrade', '2018-04-09 10:52:30', '2018-04-09 10:52:30', '2018-04-09 10:52:30', '9ca24050-ef4b-473b-bfa5-3a25b4f2b27b');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_plugins`
--

CREATE TABLE `craft_plugins` (
  `id` int(11) NOT NULL,
  `class` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `schemaVersion` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKey` char(24) COLLATE utf8_unicode_ci DEFAULT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','unknown') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unknown',
  `enabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `settings` text COLLATE utf8_unicode_ci,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_rackspaceaccess`
--

CREATE TABLE `craft_rackspaceaccess` (
  `id` int(11) NOT NULL,
  `connectionKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `storageUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cdnUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_relations`
--

CREATE TABLE `craft_relations` (
  `id` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_relations`
--

INSERT INTO `craft_relations` (`id`, `fieldId`, `sourceId`, `sourceLocale`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (4, 19, 26, NULL, 24, 1, '2018-04-03 15:08:30', '2018-04-03 15:08:30', '223d7fd0-b0ab-48ae-828c-a4ecf41a9b6c'),
  (5, 19, 27, NULL, 22, 1, '2018-04-03 15:08:59', '2018-04-03 15:08:59', '8db9a9ac-46d8-4b00-9fc8-3d3fa81b27df'),
  (6, 19, 28, NULL, 25, 1, '2018-04-03 15:09:19', '2018-04-03 15:09:19', '0fd47496-a473-424f-8ee7-4d187a28fb78'),
  (7, 19, 29, NULL, 23, 1, '2018-04-03 15:09:33', '2018-04-03 15:09:33', '5c078a10-3f55-4343-8007-d101ac2496e1'),
  (85, 14, 10, NULL, 31, 1, '2018-04-05 11:41:21', '2018-04-05 11:41:21', '402fc4e4-820e-48f1-8237-840eeef8a778'),
  (86, 14, 32, NULL, 30, 1, '2018-04-05 11:41:21', '2018-04-05 11:41:21', 'ccc0b310-021c-4ecb-a4a1-9ed19df174f1'),
  (87, 28, 35, NULL, 34, 1, '2018-04-05 11:41:21', '2018-04-05 11:41:21', '263770c0-9e5d-47a5-878f-26aee7fbcf6b'),
  (88, 28, 36, NULL, 34, 1, '2018-04-05 11:41:21', '2018-04-05 11:41:21', '384ddeaf-52d2-41c8-a3e3-6b250be7d662'),
  (89, 28, 37, NULL, 34, 1, '2018-04-05 11:41:21', '2018-04-05 11:41:21', 'cd0385f4-3318-4570-aeef-4374e0484d87'),
  (90, 33, 39, NULL, 38, 1, '2018-04-05 11:41:21', '2018-04-05 11:41:21', '1e78efc1-59e5-440b-8d43-080f03a29998'),
  (91, 41, 44, NULL, 40, 1, '2018-04-05 11:41:21', '2018-04-05 11:41:21', 'af6b2c38-77aa-43a7-990c-4e925a2308d7'),
  (92, 41, 46, NULL, 41, 1, '2018-04-05 11:41:21', '2018-04-05 11:41:21', 'bd0e7da1-7751-4121-85b3-1a0e3c5e64a3'),
  (93, 44, 11, NULL, 51, 1, '2018-04-06 08:10:45', '2018-04-06 08:10:45', '8f89a268-3573-452b-9073-538d0e4c7223'),
  (100, 51, 53, NULL, 51, 1, '2018-04-06 09:42:52', '2018-04-06 09:42:52', '0676ca4d-a901-4122-b51f-da11e73718ce'),
  (101, 47, 54, NULL, 47, 1, '2018-04-06 09:42:52', '2018-04-06 09:42:52', '3a005f72-03a3-48f2-be40-eb6eafb35043'),
  (102, 47, 55, NULL, 48, 1, '2018-04-06 09:42:52', '2018-04-06 09:42:52', 'e5aa9dbe-81d4-42b1-b67e-5d319b2ca392'),
  (103, 47, 56, NULL, 49, 1, '2018-04-06 09:42:52', '2018-04-06 09:42:52', '664bb428-02ef-43e4-a156-99eba848e81e'),
  (104, 47, 57, NULL, 50, 1, '2018-04-06 09:42:52', '2018-04-06 09:42:52', 'ba212991-3d90-4744-9f0c-f4b47ea11399'),
  (105, 33, 58, NULL, 38, 1, '2018-04-06 09:42:52', '2018-04-06 09:42:52', 'ceb39487-4c71-4b29-899f-feb9ca001f5d'),
  (106, 51, 87, NULL, 60, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '4d612314-a5d6-41f5-a67b-8b4e3f4a148f'),
  (107, 55, 88, NULL, 69, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '1b23b657-4463-4ebb-a53d-31b84fa740b5'),
  (108, 55, 88, NULL, 70, 2, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '6e2efbbf-512f-4e53-9f41-405dda36bd57'),
  (109, 55, 88, NULL, 71, 3, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'd4b19516-32fb-4b66-af9f-067c184d2e2a'),
  (110, 55, 89, NULL, 72, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'c2ab8753-36c1-42ed-a407-c34f0c3ef0a3'),
  (111, 55, 89, NULL, 73, 2, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'faf83fe1-f34f-4ac9-b892-f689021a2bdf'),
  (112, 55, 89, NULL, 74, 3, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '75c35881-e0e4-41e6-9407-0f745939e4aa'),
  (113, 55, 90, NULL, 75, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '9dfe22f9-538b-440b-92f7-31c4f56191a2'),
  (114, 55, 90, NULL, 76, 2, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '66d432a7-19cc-4801-84e4-76480ae466a6'),
  (115, 55, 90, NULL, 77, 3, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'c3ab6e57-376e-4160-b919-6ec58a4968ff'),
  (116, 55, 91, NULL, 78, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '6f1f4992-4b01-443d-a5e1-4954fcd8e18a'),
  (117, 55, 91, NULL, 79, 2, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '2be14e5c-b25e-42d4-99e0-d08c8dfed404'),
  (118, 55, 91, NULL, 80, 3, '2018-04-06 12:37:18', '2018-04-06 12:37:18', 'dac86357-3fd2-4f80-b591-3e3c8b0016ed'),
  (119, 55, 92, NULL, 81, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '1b252a3c-e47b-4160-98be-78571ecd722c'),
  (120, 55, 92, NULL, 82, 2, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '4692f035-cd8e-483d-b481-fc8c3e5774ba'),
  (121, 55, 92, NULL, 83, 3, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '20f52c50-225c-45cf-b42f-9222c1d6eaae'),
  (122, 55, 93, NULL, 84, 1, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '496a08a3-9778-4214-85dd-413f2d5115f2'),
  (123, 55, 93, NULL, 85, 2, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '2ed5152a-95b1-4c4f-aa60-882b2eeecb66'),
  (124, 55, 93, NULL, 86, 3, '2018-04-06 12:37:18', '2018-04-06 12:37:18', '8d705302-7d22-4ead-b5c6-84ade83fc503'),
  (126, 51, 94, NULL, 63, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '1a4d3263-e265-48f1-95fd-26de4e8702c6'),
  (127, 59, 100, NULL, 95, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', 'a9bc3787-6a77-4f48-9a6e-59941120e6df'),
  (128, 62, 101, NULL, 96, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '90cd370e-8a4a-4882-8e6a-f48d5e79cb6d'),
  (129, 62, 102, NULL, 97, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '7807961b-2a7a-45fa-9aad-5c8081a04792'),
  (130, 62, 103, NULL, 98, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '266a1699-0429-407d-9e6b-0a060cd34d56'),
  (131, 62, 104, NULL, 99, 1, '2018-04-10 07:58:38', '2018-04-10 07:58:38', '3e9dbfb3-7e73-4f45-988e-4982b182d7a9'),
  (136, 51, 109, NULL, 59, 1, '2018-04-10 14:59:28', '2018-04-10 14:59:28', '40e6f726-cb1f-4583-9cde-6520459cb865'),
  (137, 66, 111, NULL, 110, 1, '2018-04-10 14:59:28', '2018-04-10 14:59:28', 'ceab7cc4-ef2d-4db7-876a-a08fed0dc6ce'),
  (138, 28, 106, NULL, 34, 1, '2018-04-11 16:53:59', '2018-04-11 16:53:59', '67f47bd5-361f-4f82-b898-82561ce46d6b'),
  (139, 28, 107, NULL, 34, 1, '2018-04-11 16:53:59', '2018-04-11 16:53:59', 'de095c48-9ea3-4d23-8769-97eae233ef58'),
  (140, 28, 108, NULL, 34, 1, '2018-04-11 16:53:59', '2018-04-11 16:53:59', 'fdee23ff-669a-4940-8c43-c6c6c94ccda6'),
  (157, 51, 114, NULL, 61, 1, '2018-04-16 14:10:02', '2018-04-16 14:10:02', '1aba5959-9eac-4b5d-8420-46edc2f586e6'),
  (158, 77, 124, NULL, 121, 1, '2018-04-16 14:10:02', '2018-04-16 14:10:02', 'ecdcb77e-52e8-4c77-b4ce-b5173a8a098a'),
  (159, 77, 125, NULL, 122, 1, '2018-04-16 14:10:02', '2018-04-16 14:10:02', 'a5eab622-63ac-413e-a873-3d02a2fc1627'),
  (160, 77, 127, NULL, 123, 1, '2018-04-16 14:10:02', '2018-04-16 14:10:02', '265d3e2c-432d-4a95-83b9-4eb3c51124dd'),
  (161, 84, 12, NULL, 128, 1, '2018-04-16 14:10:02', '2018-04-16 14:10:02', '9d75cf43-a495-460b-a17f-6d0dd33466f5'),
  (162, 84, 12, NULL, 135, 2, '2018-04-16 14:10:02', '2018-04-16 14:10:02', '5897f953-8fe6-4643-8a5f-ba11d55f5530'),
  (163, 84, 12, NULL, 140, 3, '2018-04-16 14:10:02', '2018-04-16 14:10:02', '10da3fdd-a426-42b3-a2d4-dbf14c75d02f'),
  (164, 51, 162, NULL, 161, 1, '2018-04-30 17:51:42', '2018-04-30 17:51:42', '34143d3f-f5c9-45bc-b0fc-292639bbe106');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_routes`
--

CREATE TABLE `craft_routes` (
  `id` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlParts` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `urlPattern` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_searchindex`
--

CREATE TABLE `craft_searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `fieldId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_searchindex`
--

INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`) VALUES
  (1, 'username', 0, 'en', ' admin '),
  (1, 'firstname', 0, 'en', ''),
  (1, 'lastname', 0, 'en', ''),
  (1, 'fullname', 0, 'en', ''),
  (1, 'email', 0, 'en', ' kiruha_i mail ru '),
  (1, 'slug', 0, 'en', ''),
  (2, 'slug', 0, 'en', ' homepage '),
  (2, 'title', 0, 'en', ' welcome to localhost '),
  (2, 'field', 1, 'en', ' it s true this site doesn t have a whole lot of content yet but don t worry our web developers have just installed the cms and they re setting things up for the content editors this very moment soon localhost will be an oasis of fresh perspectives sharp analyses and astute opinions that will keep you coming back again and again '),
  (3, 'field', 1, 'en', ' craft is the cms that s powering localhost it s beautiful powerful flexible and easy to use and it s made by pixel tonic we can t wait to dive in and see what it s capable of this is even more captivating content which you couldn t see on the news index page because it was entered after a page break and the news index template only likes to show the content on the first page craft a nice alternative to word if you re making a website '),
  (3, 'field', 2, 'en', ''),
  (3, 'slug', 0, 'en', ' we just installed craft '),
  (3, 'title', 0, 'en', ' we just installed craft '),
  (4, 'slug', 0, 'en', ''),
  (4, 'field', 3, 'en', ' 0131 333 1222 '),
  (4, 'field', 8, 'en', ' bark n fly '),
  (4, 'field', 4, 'en', ' 2017 '),
  (4, 'field', 5, 'en', ' hallyards farm house '),
  (4, 'field', 6, 'en', ' kirkliston '),
  (4, 'field', 7, 'en', ' eh29 9dz '),
  (4, 'field', 9, 'en', ''),
  (4, 'field', 10, 'en', ''),
  (8, 'filename', 0, 'en', ' logo png '),
  (7, 'title', 0, 'en', ' logo inverse '),
  (7, 'slug', 0, 'en', ' logo inverse '),
  (7, 'kind', 0, 'en', ' image '),
  (7, 'extension', 0, 'en', ' png '),
  (7, 'filename', 0, 'en', ' logo_inverse png '),
  (8, 'extension', 0, 'en', ' png '),
  (8, 'kind', 0, 'en', ' image '),
  (8, 'slug', 0, 'en', ' logo '),
  (8, 'title', 0, 'en', ' logo '),
  (9, 'field', 11, 'en', ' 1 '),
  (9, 'field', 12, 'en', ' at bark n fly we treat your dog as if it were our own art cover welcome at bark n fly we treat your dog as if it were our own art cover welcome 2 '),
  (9, 'slug', 0, 'en', ' homepage '),
  (9, 'title', 0, 'en', ' home '),
  (10, 'field', 13, 'en', ' at bark n fly we treat your dog as if it were our own '),
  (10, 'field', 14, 'en', ' art cover welcome '),
  (10, 'slug', 0, 'en', ''),
  (11, 'field', 11, 'en', ' 1 '),
  (11, 'slug', 0, 'en', ' services '),
  (11, 'title', 0, 'en', ' services '),
  (12, 'field', 11, 'en', ' 1 '),
  (12, 'slug', 0, 'en', ' pricing '),
  (12, 'title', 0, 'en', ' pricing '),
  (13, 'field', 11, 'en', ' 1 '),
  (13, 'field', 12, 'en', ''),
  (13, 'slug', 0, 'en', ' facilities '),
  (13, 'title', 0, 'en', ' facilities '),
  (14, 'field', 11, 'en', ' 1 '),
  (14, 'slug', 0, 'en', ' training '),
  (14, 'title', 0, 'en', ' training '),
  (15, 'field', 11, 'en', ' 1 '),
  (15, 'slug', 0, 'en', ' about us '),
  (15, 'title', 0, 'en', ' about us '),
  (16, 'field', 11, 'en', ' 1 '),
  (16, 'slug', 0, 'en', ' pupparazzi '),
  (16, 'title', 0, 'en', ' pupparazzi '),
  (17, 'field', 11, 'en', ' 1 '),
  (17, 'slug', 0, 'en', ' contact us '),
  (17, 'title', 0, 'en', ' contact us '),
  (11, 'field', 15, 'en', ' 1 '),
  (12, 'field', 15, 'en', ' 2 '),
  (13, 'field', 15, 'en', ' 3 '),
  (14, 'field', 15, 'en', ' 4 '),
  (15, 'field', 15, 'en', ' 5 '),
  (16, 'field', 15, 'en', ' 6 '),
  (17, 'field', 15, 'en', ' 7 '),
  (9, 'field', 15, 'en', ' 0 '),
  (14, 'field', 16, 'en', ' 1 '),
  (11, 'field', 16, 'en', ' 1 '),
  (16, 'field', 16, 'en', ' 1 '),
  (13, 'field', 16, 'en', ' 1 '),
  (15, 'field', 16, 'en', ' 1 '),
  (17, 'field', 16, 'en', ' 1 '),
  (11, 'field', 17, 'en', ' 1 '),
  (13, 'field', 17, 'en', ' 2 '),
  (15, 'field', 17, 'en', ' 3 '),
  (16, 'field', 17, 'en', ' 4 '),
  (14, 'field', 17, 'en', ' 5 '),
  (17, 'field', 17, 'en', ' 6 '),
  (18, 'field', 11, 'en', ' 0 '),
  (18, 'field', 15, 'en', ' 0 '),
  (18, 'field', 16, 'en', ' 1 '),
  (18, 'field', 17, 'en', ' 7 '),
  (18, 'slug', 0, 'en', ' privacy policy '),
  (18, 'title', 0, 'en', ' privacy policy '),
  (19, 'field', 11, 'en', ' 0 '),
  (19, 'field', 15, 'en', ' 0 '),
  (19, 'field', 16, 'en', ' 1 '),
  (19, 'field', 17, 'en', ' 8 '),
  (19, 'slug', 0, 'en', ' terms conditions '),
  (19, 'title', 0, 'en', ' terms conditions '),
  (20, 'field', 11, 'en', ' 0 '),
  (20, 'field', 15, 'en', ' 0 '),
  (20, 'field', 16, 'en', ' 0 '),
  (20, 'field', 17, 'en', ' 9 '),
  (20, 'slug', 0, 'en', ' cookies '),
  (20, 'title', 0, 'en', ' cookies '),
  (21, 'field', 11, 'en', ' 0 '),
  (21, 'field', 15, 'en', ' 0 '),
  (21, 'field', 16, 'en', ' 1 '),
  (21, 'field', 17, 'en', ' 10 '),
  (21, 'slug', 0, 'en', ' vat registration 264563488 '),
  (21, 'title', 0, 'en', ' vat registration 264563488 '),
  (22, 'filename', 0, 'en', ' facebook png '),
  (22, 'extension', 0, 'en', ' png '),
  (22, 'kind', 0, 'en', ' image '),
  (22, 'slug', 0, 'en', ' facebook '),
  (22, 'title', 0, 'en', ' facebook '),
  (23, 'filename', 0, 'en', ' instagram png '),
  (23, 'extension', 0, 'en', ' png '),
  (23, 'kind', 0, 'en', ' image '),
  (23, 'slug', 0, 'en', ' instagram '),
  (23, 'title', 0, 'en', ' instagram '),
  (24, 'filename', 0, 'en', ' twitter png '),
  (24, 'extension', 0, 'en', ' png '),
  (24, 'kind', 0, 'en', ' image '),
  (24, 'slug', 0, 'en', ' twitter '),
  (24, 'title', 0, 'en', ' twitter '),
  (25, 'filename', 0, 'en', ' youtube png '),
  (25, 'extension', 0, 'en', ' png '),
  (25, 'kind', 0, 'en', ' image '),
  (25, 'slug', 0, 'en', ' youtube '),
  (25, 'title', 0, 'en', ' youtube '),
  (26, 'field', 19, 'en', ' twitter '),
  (26, 'field', 18, 'en', ' https twitter com '),
  (26, 'slug', 0, 'en', ' twitter '),
  (26, 'title', 0, 'en', ' twitter '),
  (27, 'field', 19, 'en', ' facebook '),
  (27, 'field', 18, 'en', ' https facebook com '),
  (27, 'slug', 0, 'en', ' facebook '),
  (27, 'title', 0, 'en', ' facebook '),
  (28, 'field', 19, 'en', ' youtube '),
  (28, 'field', 18, 'en', ' https youtube com '),
  (28, 'slug', 0, 'en', ' youtube '),
  (28, 'title', 0, 'en', ' youtube '),
  (29, 'field', 19, 'en', ' instagram '),
  (29, 'field', 18, 'en', ' https instagram com '),
  (29, 'slug', 0, 'en', ' instagram '),
  (29, 'title', 0, 'en', ' instagram '),
  (30, 'filename', 0, 'en', ' art_cover_welcome_2 jpg '),
  (30, 'extension', 0, 'en', ' jpg '),
  (30, 'kind', 0, 'en', ' image '),
  (30, 'slug', 0, 'en', ' art cover welcome 2 '),
  (30, 'title', 0, 'en', ' art cover welcome 2 '),
  (31, 'filename', 0, 'en', ' art_cover_welcome jpg '),
  (31, 'extension', 0, 'en', ' jpg '),
  (31, 'kind', 0, 'en', ' image '),
  (31, 'slug', 0, 'en', ' art cover welcome '),
  (31, 'title', 0, 'en', ' art cover welcome '),
  (9, 'field', 16, 'en', ' 0 '),
  (9, 'field', 17, 'en', ' 0 '),
  (32, 'field', 13, 'en', ' at bark n fly we treat your dog as if it were our own '),
  (32, 'field', 14, 'en', ' art cover welcome 2 '),
  (32, 'slug', 0, 'en', ''),
  (12, 'field', 16, 'en', ' 0 '),
  (12, 'field', 17, 'en', ' 0 '),
  (9, 'field', 20, 'en', ' start end book now dog boarding dog day care start your booking '),
  (33, 'field', 21, 'en', ' start your booking '),
  (33, 'field', 22, 'en', ' dog boarding '),
  (33, 'field', 23, 'en', ' start '),
  (33, 'field', 24, 'en', ' end '),
  (33, 'field', 25, 'en', ' book now '),
  (33, 'field', 26, 'en', ' dog day care '),
  (33, 'slug', 0, 'en', ''),
  (34, 'filename', 0, 'en', ' art_testimonial_1 jpg '),
  (34, 'extension', 0, 'en', ' jpg '),
  (34, 'kind', 0, 'en', ' image '),
  (34, 'slug', 0, 'en', ' art testimonial 1 '),
  (34, 'title', 0, 'en', ' art testimonial 1 '),
  (9, 'field', 27, 'en', ' eddie art testimonial 1 border collie day care guest human friend frances i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs eddie art testimonial 1 border collie day care guest human friend frances i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs eddie art testimonial 1 border collie day care guest human friend frances i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs '),
  (35, 'field', 28, 'en', ' art testimonial 1 '),
  (35, 'field', 29, 'en', ' i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs '),
  (35, 'field', 30, 'en', ' eddie '),
  (35, 'field', 31, 'en', ' border collie day care guest human friend frances '),
  (35, 'slug', 0, 'en', ''),
  (36, 'field', 28, 'en', ' art testimonial 1 '),
  (36, 'field', 29, 'en', ' i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs '),
  (36, 'field', 30, 'en', ' eddie '),
  (36, 'field', 31, 'en', ' border collie day care guest human friend frances '),
  (36, 'slug', 0, 'en', ''),
  (37, 'field', 28, 'en', ' art testimonial 1 '),
  (37, 'field', 29, 'en', ' i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs '),
  (37, 'field', 30, 'en', ' eddie '),
  (37, 'field', 31, 'en', ' border collie day care guest human friend frances '),
  (37, 'slug', 0, 'en', ''),
  (38, 'filename', 0, 'en', ' ad_background png '),
  (38, 'extension', 0, 'en', ' png '),
  (38, 'kind', 0, 'en', ' image '),
  (38, 'slug', 0, 'en', ' order background '),
  (38, 'title', 0, 'en', ' natural instinct '),
  (9, 'field', 32, 'en', ' order form call us today 01234 567 890 natural instinct https google com '),
  (39, 'field', 33, 'en', ' natural instinct '),
  (39, 'field', 34, 'en', ' call us today 01234 567 890 '),
  (39, 'field', 35, 'en', ' order form '),
  (39, 'slug', 0, 'en', ''),
  (39, 'field', 36, 'en', ' https google com '),
  (40, 'filename', 0, 'en', ' card_1 png '),
  (40, 'extension', 0, 'en', ' png '),
  (40, 'kind', 0, 'en', ' image '),
  (40, 'slug', 0, 'en', ' card 1 '),
  (40, 'title', 0, 'en', ' card 1 '),
  (41, 'filename', 0, 'en', ' card_3 png '),
  (41, 'extension', 0, 'en', ' png '),
  (41, 'kind', 0, 'en', ' image '),
  (41, 'slug', 0, 'en', ' card 3 '),
  (41, 'title', 0, 'en', ' card 3 '),
  (42, 'filename', 0, 'en', ' card_4 png '),
  (42, 'extension', 0, 'en', ' png '),
  (42, 'kind', 0, 'en', ' image '),
  (42, 'slug', 0, 'en', ' card 4 '),
  (42, 'title', 0, 'en', ' card 4 '),
  (9, 'field', 37, 'en', ' 6cb55a your best friend deserves the best care at bark n fly we specialise in giving dogs more established in 2009 we provide premium dog boarding and dog day care throughout edinburgh the surrounding area pick ups and drop offs card 1 we can safely and securely collect or return your furry friend we even have a handy sms service notifying you directly to your mobile phone about pick ups or drop offs if you re away from home spacious safe playareas under the watchful eye of our experienced fully insured and police vetted staff your dog will love running around in our two outdoor exercise areas and dry indoor arena keeping paws busy and tails wagging dog training card 3 our dog training service is there to help with any problems you might be experiencing with your dog from every day obedience to behaviour issues all the way to competition training and other specialised types of work '),
  (43, 'field', 38, 'en', ' your best friend deserves the best care '),
  (43, 'field', 39, 'en', ' at bark n fly we specialise in giving dogs more established in 2009 we provide premium dog boarding and dog day care throughout edinburgh the surrounding area '),
  (43, 'field', 40, 'en', ' 6cb55a '),
  (43, 'slug', 0, 'en', ''),
  (44, 'field', 41, 'en', ' card 1 '),
  (44, 'field', 42, 'en', ' pick ups and drop offs '),
  (44, 'field', 43, 'en', ' we can safely and securely collect or return your furry friend we even have a handy sms service notifying you directly to your mobile phone about pick ups or drop offs if you re away from home '),
  (44, 'slug', 0, 'en', ''),
  (45, 'field', 41, 'en', ''),
  (45, 'field', 42, 'en', ' spacious safe playareas '),
  (45, 'field', 43, 'en', ' under the watchful eye of our experienced fully insured and police vetted staff your dog will love running around in our two outdoor exercise areas and dry indoor arena keeping paws busy and tails wagging '),
  (45, 'slug', 0, 'en', ''),
  (46, 'field', 41, 'en', ' card 3 '),
  (46, 'field', 42, 'en', ' dog training '),
  (46, 'field', 43, 'en', ' our dog training service is there to help with any problems you might be experiencing with your dog from every day obedience to behaviour issues all the way to competition training and other specialised types of work '),
  (46, 'slug', 0, 'en', ''),
  (47, 'filename', 0, 'en', ' service_1 png '),
  (47, 'extension', 0, 'en', ' png '),
  (47, 'kind', 0, 'en', ' image '),
  (47, 'slug', 0, 'en', ' service 1 '),
  (47, 'title', 0, 'en', ' service 1 '),
  (48, 'filename', 0, 'en', ' service_2 png '),
  (48, 'extension', 0, 'en', ' png '),
  (48, 'kind', 0, 'en', ' image '),
  (48, 'slug', 0, 'en', ' service 2 '),
  (48, 'title', 0, 'en', ' service 2 '),
  (49, 'filename', 0, 'en', ' service_3 png '),
  (49, 'extension', 0, 'en', ' png '),
  (49, 'kind', 0, 'en', ' image '),
  (49, 'slug', 0, 'en', ' service 3 '),
  (49, 'title', 0, 'en', ' service 3 '),
  (50, 'filename', 0, 'en', ' service_4 png '),
  (50, 'extension', 0, 'en', ' png '),
  (50, 'kind', 0, 'en', ' image '),
  (50, 'slug', 0, 'en', ' service 4 '),
  (50, 'title', 0, 'en', ' service 4 '),
  (51, 'filename', 0, 'en', ' art_cover_service jpg '),
  (51, 'extension', 0, 'en', ' jpg '),
  (51, 'kind', 0, 'en', ' image '),
  (51, 'slug', 0, 'en', ' art cover service '),
  (51, 'title', 0, 'en', ' art cover service '),
  (11, 'field', 44, 'en', ' our services art cover service '),
  (11, 'field', 45, 'en', ' call us today 01234 567 890 dog day care service 1 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui dog boarding service 2 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui pick up drop off service 3 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui dog training service 4 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui '),
  (52, 'field', 46, 'en', ' call us today 01234 567 890 '),
  (52, 'slug', 0, 'en', ''),
  (53, 'field', 50, 'en', ' our services '),
  (53, 'field', 51, 'en', ' art cover service '),
  (53, 'slug', 0, 'en', ''),
  (54, 'field', 47, 'en', ' service 1 '),
  (54, 'field', 48, 'en', ' dog day care '),
  (54, 'field', 49, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui '),
  (54, 'slug', 0, 'en', ''),
  (55, 'field', 47, 'en', ' service 2 '),
  (55, 'field', 48, 'en', ' dog boarding '),
  (55, 'field', 49, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui '),
  (55, 'slug', 0, 'en', ''),
  (56, 'field', 47, 'en', ' service 3 '),
  (56, 'field', 48, 'en', ' pick up drop off '),
  (56, 'field', 49, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui '),
  (56, 'slug', 0, 'en', ''),
  (57, 'field', 47, 'en', ' service 4 '),
  (57, 'field', 48, 'en', ' dog training '),
  (57, 'field', 49, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui '),
  (57, 'slug', 0, 'en', ''),
  (11, 'field', 32, 'en', ' order form call us today 01234 567 890 natural instinct https google com '),
  (58, 'field', 33, 'en', ' natural instinct '),
  (58, 'field', 34, 'en', ' call us today 01234 567 890 '),
  (58, 'field', 35, 'en', ' order form '),
  (58, 'field', 36, 'en', ' https google com '),
  (58, 'slug', 0, 'en', ''),
  (59, 'filename', 0, 'en', ' art_cover_about jpg '),
  (59, 'extension', 0, 'en', ' jpg '),
  (59, 'kind', 0, 'en', ' image '),
  (59, 'slug', 0, 'en', ' art cover about '),
  (59, 'title', 0, 'en', ' art cover about '),
  (60, 'filename', 0, 'en', ' art_cover_facilities jpg '),
  (60, 'extension', 0, 'en', ' jpg '),
  (60, 'kind', 0, 'en', ' image '),
  (60, 'slug', 0, 'en', ' art cover facilities '),
  (60, 'title', 0, 'en', ' art cover facilities '),
  (61, 'filename', 0, 'en', ' art_cover_pricing jpg '),
  (61, 'extension', 0, 'en', ' jpg '),
  (61, 'kind', 0, 'en', ' image '),
  (61, 'slug', 0, 'en', ' art cover pricing '),
  (61, 'title', 0, 'en', ' art cover pricing '),
  (67, 'kind', 0, 'en', ' image '),
  (67, 'slug', 0, 'en', ' art cover welcome 2 '),
  (67, 'extension', 0, 'en', ' jpg '),
  (67, 'filename', 0, 'en', ' art_cover_welcome_2 jpg '),
  (63, 'filename', 0, 'en', ' art_cover_training png '),
  (63, 'extension', 0, 'en', ' png '),
  (63, 'kind', 0, 'en', ' image '),
  (63, 'slug', 0, 'en', ' art cover training '),
  (63, 'title', 0, 'en', ' art cover training '),
  (68, 'kind', 0, 'en', ' image '),
  (68, 'slug', 0, 'en', ' art cover welcome '),
  (68, 'extension', 0, 'en', ' jpg '),
  (68, 'filename', 0, 'en', ' art_cover_welcome jpg '),
  (69, 'kind', 0, 'en', ' image '),
  (69, 'slug', 0, 'en', ' facility 1 1 '),
  (69, 'extension', 0, 'en', ' png '),
  (69, 'filename', 0, 'en', ' facility_1_1 png '),
  (66, 'filename', 0, 'en', ' art_cover_service jpg '),
  (66, 'extension', 0, 'en', ' jpg '),
  (66, 'kind', 0, 'en', ' image '),
  (66, 'slug', 0, 'en', ' art cover service '),
  (66, 'title', 0, 'en', ' art cover service '),
  (67, 'title', 0, 'en', ' art cover welcome 2 '),
  (68, 'title', 0, 'en', ' art cover welcome '),
  (69, 'title', 0, 'en', ' facility 1 1 '),
  (70, 'filename', 0, 'en', ' facility_1_2 png '),
  (70, 'extension', 0, 'en', ' png '),
  (70, 'kind', 0, 'en', ' image '),
  (70, 'slug', 0, 'en', ' facility 1 2 '),
  (70, 'title', 0, 'en', ' facility 1 2 '),
  (71, 'filename', 0, 'en', ' facility_1_3 png '),
  (71, 'extension', 0, 'en', ' png '),
  (71, 'kind', 0, 'en', ' image '),
  (71, 'slug', 0, 'en', ' facility 1 3 '),
  (71, 'title', 0, 'en', ' facility 1 3 '),
  (72, 'filename', 0, 'en', ' facility_2_1 png '),
  (72, 'extension', 0, 'en', ' png '),
  (72, 'kind', 0, 'en', ' image '),
  (72, 'slug', 0, 'en', ' facility 2 1 '),
  (72, 'title', 0, 'en', ' facility 2 1 '),
  (73, 'filename', 0, 'en', ' facility_2_2 png '),
  (73, 'extension', 0, 'en', ' png '),
  (73, 'kind', 0, 'en', ' image '),
  (73, 'slug', 0, 'en', ' facility 2 2 '),
  (73, 'title', 0, 'en', ' facility 2 2 '),
  (74, 'filename', 0, 'en', ' facility_2_3 png '),
  (74, 'extension', 0, 'en', ' png '),
  (74, 'kind', 0, 'en', ' image '),
  (74, 'slug', 0, 'en', ' facility 2 3 '),
  (74, 'title', 0, 'en', ' facility 2 3 '),
  (75, 'filename', 0, 'en', ' facility_3_1 png '),
  (75, 'extension', 0, 'en', ' png '),
  (75, 'kind', 0, 'en', ' image '),
  (75, 'slug', 0, 'en', ' facility 3 1 '),
  (75, 'title', 0, 'en', ' facility 3 1 '),
  (76, 'filename', 0, 'en', ' facility_3_2 png '),
  (76, 'extension', 0, 'en', ' png '),
  (76, 'kind', 0, 'en', ' image '),
  (76, 'slug', 0, 'en', ' facility 3 2 '),
  (76, 'title', 0, 'en', ' facility 3 2 '),
  (77, 'filename', 0, 'en', ' facility_3_3 png '),
  (77, 'extension', 0, 'en', ' png '),
  (77, 'kind', 0, 'en', ' image '),
  (77, 'slug', 0, 'en', ' facility 3 3 '),
  (77, 'title', 0, 'en', ' facility 3 3 '),
  (78, 'filename', 0, 'en', ' facility_4_1 png '),
  (78, 'extension', 0, 'en', ' png '),
  (78, 'kind', 0, 'en', ' image '),
  (78, 'slug', 0, 'en', ' facility 4 1 '),
  (78, 'title', 0, 'en', ' facility 4 1 '),
  (79, 'filename', 0, 'en', ' facility_4_2 png '),
  (79, 'extension', 0, 'en', ' png '),
  (79, 'kind', 0, 'en', ' image '),
  (79, 'slug', 0, 'en', ' facility 4 2 '),
  (79, 'title', 0, 'en', ' facility 4 2 '),
  (80, 'filename', 0, 'en', ' facility_4_3 png '),
  (80, 'extension', 0, 'en', ' png '),
  (80, 'kind', 0, 'en', ' image '),
  (80, 'slug', 0, 'en', ' facility 4 3 '),
  (80, 'title', 0, 'en', ' facility 4 3 '),
  (81, 'filename', 0, 'en', ' facility_5_1 png '),
  (81, 'extension', 0, 'en', ' png '),
  (81, 'kind', 0, 'en', ' image '),
  (81, 'slug', 0, 'en', ' facility 5 1 '),
  (81, 'title', 0, 'en', ' facility 5 1 '),
  (82, 'filename', 0, 'en', ' facility_5_2 png '),
  (82, 'extension', 0, 'en', ' png '),
  (82, 'kind', 0, 'en', ' image '),
  (82, 'slug', 0, 'en', ' facility 5 2 '),
  (82, 'title', 0, 'en', ' facility 5 2 '),
  (83, 'filename', 0, 'en', ' facility_5_3 png '),
  (83, 'extension', 0, 'en', ' png '),
  (83, 'kind', 0, 'en', ' image '),
  (83, 'slug', 0, 'en', ' facility 5 3 '),
  (83, 'title', 0, 'en', ' facility 5 3 '),
  (84, 'filename', 0, 'en', ' facility_6_1 png '),
  (84, 'extension', 0, 'en', ' png '),
  (84, 'kind', 0, 'en', ' image '),
  (84, 'slug', 0, 'en', ' facility 6 1 '),
  (84, 'title', 0, 'en', ' facility 6 1 '),
  (85, 'filename', 0, 'en', ' facility_6_2 png '),
  (85, 'extension', 0, 'en', ' png '),
  (85, 'kind', 0, 'en', ' image '),
  (85, 'slug', 0, 'en', ' facility 6 2 '),
  (85, 'title', 0, 'en', ' facility 6 2 '),
  (86, 'filename', 0, 'en', ' facility_6_3 png '),
  (86, 'extension', 0, 'en', ' png '),
  (86, 'kind', 0, 'en', ' image '),
  (86, 'slug', 0, 'en', ' facility 6 3 '),
  (86, 'title', 0, 'en', ' facility 6 3 '),
  (13, 'field', 44, 'en', ' our facilities art cover facilities '),
  (13, 'field', 52, 'en', ' little dog pen facility 1 1 facility 1 2 facility 1 3 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero big dog pen facility 2 1 facility 2 2 facility 2 3 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero indoor arena facility 3 1 facility 3 2 facility 3 3 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero little dog field facility 4 1 facility 4 2 facility 4 3 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero big dog field facility 5 1 facility 5 2 facility 5 3 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero boarding kennel facility 6 1 facility 6 2 facility 6 3 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero '),
  (87, 'field', 50, 'en', ' our facilities '),
  (87, 'field', 51, 'en', ' art cover facilities '),
  (87, 'slug', 0, 'en', ''),
  (88, 'field', 53, 'en', ' little dog pen '),
  (88, 'field', 54, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero '),
  (88, 'field', 55, 'en', ' facility 1 1 facility 1 2 facility 1 3 '),
  (88, 'slug', 0, 'en', ''),
  (89, 'field', 53, 'en', ' big dog pen '),
  (89, 'field', 54, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero '),
  (89, 'field', 55, 'en', ' facility 2 1 facility 2 2 facility 2 3 '),
  (89, 'slug', 0, 'en', ''),
  (90, 'field', 53, 'en', ' indoor arena '),
  (90, 'field', 54, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero '),
  (90, 'field', 55, 'en', ' facility 3 1 facility 3 2 facility 3 3 '),
  (90, 'slug', 0, 'en', ''),
  (91, 'field', 53, 'en', ' little dog field '),
  (91, 'field', 54, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero '),
  (91, 'field', 55, 'en', ' facility 4 1 facility 4 2 facility 4 3 '),
  (91, 'slug', 0, 'en', ''),
  (92, 'field', 53, 'en', ' big dog field '),
  (92, 'field', 54, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero '),
  (92, 'field', 55, 'en', ' facility 5 1 facility 5 2 facility 5 3 '),
  (92, 'slug', 0, 'en', ''),
  (93, 'field', 53, 'en', ' boarding kennel '),
  (93, 'field', 54, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie gravida ex fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero '),
  (93, 'field', 55, 'en', ' facility 6 1 facility 6 2 facility 6 3 '),
  (93, 'slug', 0, 'en', ''),
  (14, 'field', 44, 'en', ' training art cover training '),
  (14, 'field', 56, 'en', ' dog training behaviour correction and puppy education fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta leo sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie convallis fusce nisi orci tincidunt at libero et molestie gravida ex tincidunt nibhtempor at leo training video practical obedience training 1 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor ateget mi puppy education training 2 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor ateget mi behaviour correction training 3 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor ateget mi activity based training 4 lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor ateget mi '),
  (94, 'field', 50, 'en', ' training '),
  (94, 'field', 51, 'en', ' art cover training '),
  (94, 'slug', 0, 'en', ''),
  (95, 'filename', 0, 'en', ' training_video png '),
  (95, 'extension', 0, 'en', ' png '),
  (95, 'kind', 0, 'en', ' image '),
  (95, 'slug', 0, 'en', ' training video '),
  (95, 'title', 0, 'en', ' training video '),
  (96, 'filename', 0, 'en', ' training_1 png '),
  (96, 'extension', 0, 'en', ' png '),
  (96, 'kind', 0, 'en', ' image '),
  (96, 'slug', 0, 'en', ' training 1 '),
  (96, 'title', 0, 'en', ' training 1 '),
  (97, 'filename', 0, 'en', ' training_2 png '),
  (97, 'extension', 0, 'en', ' png '),
  (97, 'kind', 0, 'en', ' image '),
  (97, 'slug', 0, 'en', ' training 2 '),
  (97, 'title', 0, 'en', ' training 2 '),
  (98, 'filename', 0, 'en', ' training_3 png '),
  (98, 'extension', 0, 'en', ' png '),
  (98, 'kind', 0, 'en', ' image '),
  (98, 'slug', 0, 'en', ' training 3 '),
  (98, 'title', 0, 'en', ' training 3 '),
  (99, 'filename', 0, 'en', ' training_4 png '),
  (99, 'extension', 0, 'en', ' png '),
  (99, 'kind', 0, 'en', ' image '),
  (99, 'slug', 0, 'en', ' training 4 '),
  (99, 'title', 0, 'en', ' training 4 '),
  (100, 'field', 57, 'en', ' dog training behaviour correction and puppy education '),
  (100, 'field', 58, 'en', ' fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta leo sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie convallis fusce nisi orci tincidunt at libero et molestie gravida ex tincidunt nibhtempor at leo '),
  (100, 'field', 59, 'en', ' training video '),
  (100, 'slug', 0, 'en', ''),
  (101, 'field', 60, 'en', ' practical obedience '),
  (101, 'field', 61, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor ateget mi '),
  (101, 'field', 62, 'en', ' training 1 '),
  (101, 'slug', 0, 'en', ''),
  (102, 'field', 60, 'en', ' puppy education '),
  (102, 'field', 61, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor ateget mi '),
  (102, 'field', 62, 'en', ' training 2 '),
  (102, 'slug', 0, 'en', ''),
  (103, 'field', 60, 'en', ' behaviour correction '),
  (103, 'field', 61, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor ateget mi '),
  (103, 'field', 62, 'en', ' training 3 '),
  (103, 'slug', 0, 'en', ''),
  (104, 'field', 60, 'en', ' activity based '),
  (104, 'field', 61, 'en', ' lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor at leo nec laoreet blandit ligula lorem ipsum dolor sit amet consectetur adipiscing elit vestibulum viverra diam a nisi vestibulum porta ac eget mi fusce risus ipsum tempor ateget mi '),
  (104, 'field', 62, 'en', ' training 4 '),
  (104, 'slug', 0, 'en', ''),
  (105, 'field', 27, 'en', ' eddie art testimonial 1 border collie day care guest human friend frances i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs dexter art testimonial 1 border collie day care guest human friend frances i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs george art testimonial 1 border collie day care guest human friend frances i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs '),
  (105, 'slug', 0, 'en', ' feedback list '),
  (105, 'title', 0, 'en', ' feedback list '),
  (106, 'field', 28, 'en', ' art testimonial 1 '),
  (106, 'field', 29, 'en', ' i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs '),
  (106, 'field', 30, 'en', ' eddie '),
  (106, 'field', 31, 'en', ' border collie day care guest human friend frances '),
  (106, 'slug', 0, 'en', ''),
  (107, 'field', 28, 'en', ' art testimonial 1 '),
  (107, 'field', 29, 'en', ' i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs '),
  (107, 'field', 30, 'en', ' dexter '),
  (107, 'field', 31, 'en', ' border collie day care guest human friend frances '),
  (107, 'slug', 0, 'en', ''),
  (108, 'field', 28, 'en', ' art testimonial 1 '),
  (108, 'field', 29, 'en', ' i love coming to bark n fly every school day i get to see all my friends run about the big field chasing butterflies and sometimes my tail too the human helpers here are very kind to me and give me delicious healthy treats if i m a good boy in the afternoon i get to play indoors and by the time i see my human friend at home time i m ready for cuddles and belly rubs '),
  (108, 'field', 30, 'en', ' george '),
  (108, 'field', 31, 'en', ' border collie day care guest human friend frances '),
  (108, 'slug', 0, 'en', ''),
  (15, 'field', 44, 'en', ' about us art cover about '),
  (15, 'field', 63, 'en', ' bark n fly fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta leo sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie convallis fusce nisi orci tincidunt at libero et molestie gravida ex tincidunt nibhtempor at leo about video '),
  (15, 'field', 67, 'en', ' servicing the nation pick up drop off locations '),
  (109, 'field', 50, 'en', ' about us '),
  (109, 'field', 51, 'en', ' art cover about '),
  (109, 'slug', 0, 'en', ''),
  (110, 'filename', 0, 'en', ' about_video png '),
  (110, 'extension', 0, 'en', ' png '),
  (110, 'kind', 0, 'en', ' image '),
  (110, 'slug', 0, 'en', ' about video '),
  (110, 'title', 0, 'en', ' about video '),
  (111, 'field', 64, 'en', ' bark n fly '),
  (111, 'field', 65, 'en', ' fusce viverra mauris eu pulvinar mattis ligula justo viverra purus non tristique ante ipsum a justo donec nec porta leo sapien maecenas pulvinar porttitor tincidunt ut arcu tellus ornare sed magna sit amet eleifend pulvinar erat vivamus suscipit nulla eu pharetra placerat leo leo semper ligula vel gravida erat justo quis dui vestibulum tellus magna pretium ut congue sit amet tristique sit amet leo mauris eros enim vestibulum at mollis at tempor ut libero pellentesque tincidunt nibh et neque eleifend convallis fusce nisi orci tincidunt at libero et molestie convallis fusce nisi orci tincidunt at libero et molestie gravida ex tincidunt nibhtempor at leo '),
  (111, 'field', 66, 'en', ' about video '),
  (111, 'slug', 0, 'en', ''),
  (112, 'field', 68, 'en', ' servicing the nation '),
  (112, 'slug', 0, 'en', ''),
  (113, 'field', 68, 'en', ' pick up drop off locations '),
  (113, 'slug', 0, 'en', ''),
  (12, 'field', 44, 'en', ' pricing art cover pricing '),
  (12, 'field', 69, 'en', ' day care rates include vat note that day care and boarding pick up drop off service is restricted to certain areas please contact us for details please note dogmore does not charge for days or periods when your dog is not using our services including last minute cancellations full day 0 singlesizedblock 22 50 full day for two dogs same household 0 singlesizedblock 22 30 full day more than 15 days per month 0 singlesizedblock 37 50 half day 5 hours or less 1 singlesizedblock 22 30 pick up drop off 0 doublesizedblock 1 50 for each additional dog 3 00 per dog '),
  (12, 'field', 76, 'en', ' download download our pricing flat grid view 1 fullsize http www pdf995 com samples pdf pdf learn more dog boarding flat grid view 2 halfsize 01234 567 890 https google com learn more dog taxi flat grid view 3 halfsize 01234 567 890 https google com '),
  (114, 'field', 50, 'en', ' pricing '),
  (114, 'field', 51, 'en', ' art cover pricing '),
  (114, 'slug', 0, 'en', ''),
  (158, 'field', 88, 'en', ' 65 00 '),
  (156, 'field', 89, 'en', ''),
  (156, 'field', 90, 'en', ' singlesized '),
  (156, 'slug', 0, 'en', ''),
  (157, 'slug', 0, 'en', ''),
  (158, 'field', 86, 'en', ' 1 '),
  (158, 'field', 87, 'en', ' subsequent session client residence '),
  (156, 'field', 88, 'en', ' 18 50 '),
  (157, 'field', 87, 'en', ' one on one session at centre ');
INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `locale`, `keywords`) VALUES
  (157, 'field', 88, 'en', ' 95 00 '),
  (157, 'field', 86, 'en', ' 0 '),
  (157, 'field', 90, 'en', ' singlesized '),
  (157, 'field', 89, 'en', ''),
  (135, 'field', 91, 'en', ' please note regardless of the time your dog is checked in the charge of 26 would apply from the check in time until midday the next day collection after midday would incur an additional charge of half a day boarding 18 50 for one dog '),
  (121, 'filename', 0, 'en', ' flat_grid_view_1 jpg '),
  (121, 'extension', 0, 'en', ' jpg '),
  (121, 'kind', 0, 'en', ' image '),
  (121, 'slug', 0, 'en', ' flat grid view 1 '),
  (121, 'title', 0, 'en', ' flat grid view 1 '),
  (122, 'filename', 0, 'en', ' flat_grid_view_2 jpg '),
  (122, 'extension', 0, 'en', ' jpg '),
  (122, 'kind', 0, 'en', ' image '),
  (122, 'slug', 0, 'en', ' flat grid view 2 '),
  (122, 'title', 0, 'en', ' flat grid view 2 '),
  (123, 'filename', 0, 'en', ' flat_grid_view_3 jpg '),
  (123, 'extension', 0, 'en', ' jpg '),
  (123, 'kind', 0, 'en', ' image '),
  (123, 'slug', 0, 'en', ' flat grid view 3 '),
  (123, 'title', 0, 'en', ' flat grid view 3 '),
  (124, 'field', 77, 'en', ' flat grid view 1 '),
  (124, 'field', 78, 'en', ' download our pricing '),
  (124, 'field', 79, 'en', ''),
  (124, 'field', 80, 'en', ' http www pdf995 com samples pdf pdf '),
  (124, 'slug', 0, 'en', ''),
  (125, 'field', 77, 'en', ' flat grid view 2 '),
  (125, 'field', 78, 'en', ' dog boarding '),
  (125, 'field', 79, 'en', ' 01234 567 890 '),
  (125, 'field', 80, 'en', ' https google com '),
  (125, 'slug', 0, 'en', ''),
  (124, 'field', 82, 'en', ' download '),
  (124, 'field', 83, 'en', ' fullsize '),
  (125, 'field', 82, 'en', ' learn more '),
  (125, 'field', 83, 'en', ' halfsize '),
  (128, 'field', 69, 'en', ' day care rates include vat note that day care and boarding pick up drop off service is restricted to certain areas please contact us for details please note dogmore does not charge for days or periods when your dog is not using our services including last minute cancellations full day 0 singlesizedblock 22 50 full day for two dogs same household 0 singlesizedblock 22 30 full day more than 15 days per month 0 singlesizedblock 37 50 half day 5 hours or less 1 singlesizedblock 22 30 pick up drop off 0 doublesizedblock 1 50 for each additional dog 3 00 per dog '),
  (127, 'field', 77, 'en', ' flat grid view 3 '),
  (127, 'field', 78, 'en', ' dog taxi '),
  (127, 'field', 79, 'en', ' 01234 567 890 '),
  (127, 'field', 80, 'en', ' https google com '),
  (127, 'field', 82, 'en', ' learn more '),
  (127, 'field', 83, 'en', ' halfsize '),
  (127, 'slug', 0, 'en', ''),
  (128, 'slug', 0, 'en', ' day care '),
  (128, 'title', 0, 'en', ' day care '),
  (155, 'field', 88, 'en', ' 37 00 '),
  (155, 'field', 89, 'en', ''),
  (155, 'field', 90, 'en', ' singlesized '),
  (155, 'slug', 0, 'en', ''),
  (156, 'field', 86, 'en', ' 0 '),
  (156, 'field', 87, 'en', ' after mid day collection '),
  (155, 'field', 87, 'en', ' two dogs same household '),
  (135, 'field', 85, 'en', ' per night 0 26 00 singlesized two dogs same household 1 37 00 singlesized after mid day collection 0 18 50 singlesized '),
  (150, 'field', 90, 'en', ' doublesized '),
  (150, 'slug', 0, 'en', ''),
  (128, 'field', 91, 'en', ' rates include vat note that day care and boarding pick up drop off service is restricted to certain areas please contact us for details please note dogmore does not charge for days or periods when your dog is not using our services including last minute cancellations '),
  (150, 'field', 89, 'en', ' 1 50 for each additional dog '),
  (150, 'field', 87, 'en', ' pick up drop off '),
  (150, 'field', 88, 'en', ' 3 00 per dog '),
  (150, 'field', 86, 'en', ' 0 '),
  (149, 'slug', 0, 'en', ''),
  (149, 'field', 88, 'en', ' 37 50 '),
  (149, 'field', 89, 'en', ''),
  (149, 'field', 90, 'en', ' singlesized '),
  (135, 'field', 69, 'en', ' boarding kennels please note regardless of the time your dog is checked in the charge of 26 would apply from the check in time until midday the next day collection after midday would incur an additional charge of half a day boarding 18 50 for one dog per night 0 singlesizedblock 26 00 two dogs same household 1 singlesizedblock 37 00 after mid day collection 0 singlesizedblock 18 50 '),
  (135, 'slug', 0, 'en', ' boarding kennels '),
  (135, 'title', 0, 'en', ' boarding kennels '),
  (155, 'field', 86, 'en', ' 1 '),
  (154, 'field', 87, 'en', ' per night '),
  (154, 'field', 88, 'en', ' 26 00 '),
  (154, 'field', 89, 'en', ''),
  (154, 'field', 90, 'en', ' singlesized '),
  (154, 'slug', 0, 'en', ''),
  (154, 'field', 86, 'en', ' 0 '),
  (148, 'slug', 0, 'en', ''),
  (149, 'field', 86, 'en', ' 0 '),
  (149, 'field', 87, 'en', ' full day more than 15 days per month '),
  (148, 'field', 90, 'en', ' singlesized '),
  (148, 'field', 88, 'en', ' 22 30 '),
  (148, 'field', 89, 'en', ''),
  (148, 'field', 87, 'en', ' full day for two dogs same household '),
  (147, 'field', 89, 'en', ''),
  (147, 'field', 90, 'en', ' singlesized '),
  (147, 'slug', 0, 'en', ''),
  (148, 'field', 86, 'en', ' 0 '),
  (140, 'field', 69, 'en', ' dog training behaviour correction and puppy education please note we like to differentiate between performance and practical obedience performance obedience is suited for owners who like the idea of working with their dog and would like to engage in dog training as an activity either competitively or not one on one session at centre 0 singlesizedblock 95 00 subsequent session client residence 1 singlesizedblock 65 00 one on one session client residence 0 singlesizedblock 120 00 subsequent session at centre 0 singlesizedblock per 30 minutes 50 00 '),
  (140, 'slug', 0, 'en', ' dog training behaviour correction and puppy education '),
  (140, 'title', 0, 'en', ' dog training behaviour correction and puppy education '),
  (140, 'field', 85, 'en', ' one on one session at centre 0 95 00 singlesized subsequent session client residence 1 65 00 singlesized one on one session client residence 0 120 00 singlesized subsequent session at centre 0 50 00 singlesized per 30 minutes '),
  (140, 'field', 91, 'en', ' please note we like to differentiate between performance and practical obedience performance obedience is suited for owners who like the idea of working with their dog and would like to engage in dog training as an activity either competitively or not '),
  (147, 'field', 88, 'en', ' 20 30 '),
  (147, 'field', 86, 'en', ' 1 '),
  (147, 'field', 87, 'en', ' half day 5 hours or less '),
  (146, 'field', 89, 'en', ''),
  (146, 'field', 90, 'en', ' singlesized '),
  (146, 'slug', 0, 'en', ''),
  (146, 'field', 88, 'en', ' 22 50 '),
  (146, 'field', 86, 'en', ' 0 '),
  (146, 'field', 87, 'en', ' full day '),
  (128, 'field', 85, 'en', ' full day 0 22 50 singlesized full day for two dogs same household 0 22 30 singlesized full day more than 15 days per month 0 37 50 singlesized half day 5 hours or less 1 20 30 singlesized pick up drop off 0 3 00 per dog doublesized 1 50 for each additional dog '),
  (12, 'field', 84, 'en', ' day care boarding kennels dog training behaviour correction and puppy education '),
  (158, 'field', 89, 'en', ''),
  (158, 'field', 90, 'en', ' singlesized '),
  (158, 'slug', 0, 'en', ''),
  (159, 'field', 86, 'en', ' 0 '),
  (159, 'field', 87, 'en', ' one on one session client residence '),
  (159, 'field', 88, 'en', ' 120 00 '),
  (159, 'field', 89, 'en', ''),
  (159, 'field', 90, 'en', ' singlesized '),
  (159, 'slug', 0, 'en', ''),
  (160, 'field', 86, 'en', ' 0 '),
  (160, 'field', 87, 'en', ' subsequent session at centre '),
  (160, 'field', 88, 'en', ' 50 00 '),
  (160, 'field', 89, 'en', ' per 30 minutes '),
  (160, 'field', 90, 'en', ' singlesized '),
  (160, 'slug', 0, 'en', ''),
  (161, 'filename', 0, 'en', ' pupparazzi cover png '),
  (161, 'extension', 0, 'en', ' png '),
  (161, 'kind', 0, 'en', ' image '),
  (161, 'slug', 0, 'en', ' pupparazzi cover '),
  (161, 'title', 0, 'en', ' pupparazzi cover '),
  (16, 'field', 44, 'en', ' pupparazzi pupparazzi cover '),
  (162, 'field', 50, 'en', ' pupparazzi '),
  (162, 'field', 51, 'en', ' pupparazzi cover '),
  (162, 'slug', 0, 'en', '');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_sections`
--

CREATE TABLE `craft_sections` (
  `id` int(11) NOT NULL,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('single','channel','structure') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'channel',
  `hasUrls` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `template` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enableVersioning` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_sections`
--

INSERT INTO `craft_sections` (`id`, `structureId`, `name`, `handle`, `type`, `hasUrls`, `template`, `enableVersioning`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, NULL, 'Homepage', 'homepage', 'single', 1, 'homepage', 1, '2018-03-28 09:59:41', '2018-04-02 08:22:03', '1d5ce7d5-1405-40b9-b996-f2ae92355ceb'),
  (2, NULL, 'News', 'news', 'channel', 1, 'news/_entry', 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '0560b209-efa4-414c-9035-05b9aaa16667'),
  (3, NULL, 'Pages', 'pages', 'channel', 1, 'pages/_page_switcher.twig', 1, '2018-04-02 13:55:56', '2018-04-04 08:38:44', 'ca3afe18-4537-468b-a43c-f08950943a77'),
  (4, NULL, 'Social Links', 'socialLinks', 'channel', 0, NULL, 1, '2018-04-03 14:57:48', '2018-04-03 14:57:48', '61b7e0ee-3fac-49f2-8512-0786d9300507'),
  (5, NULL, 'Feedback List', 'feedbackList', 'channel', 1, 'feedback-list/_entry', 1, '2018-04-10 12:10:46', '2018-04-10 12:10:46', '4cb7a612-1f84-47fd-9c43-58f0bce8ad45'),
  (6, NULL, 'Price Lists', 'priceLists', 'channel', 1, 'price-lists/_entry', 1, '2018-04-16 13:52:02', '2018-04-16 13:52:02', '571165bd-9efc-46c9-8c63-b3b739e05eee'),
  (7, NULL, 'Blog', 'blog', 'channel', 1, 'blog/_entry', 1, '2018-05-03 11:13:44', '2018-05-03 11:13:44', '10464e79-3986-49a1-95ba-d470ca421a1b');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_sections_i18n`
--

CREATE TABLE `craft_sections_i18n` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `enabledByDefault` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `urlFormat` text COLLATE utf8_unicode_ci,
  `nestedUrlFormat` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_sections_i18n`
--

INSERT INTO `craft_sections_i18n` (`id`, `sectionId`, `locale`, `enabledByDefault`, `urlFormat`, `nestedUrlFormat`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 1, 'en', 1, '__home__', NULL, '2018-03-28 09:59:41', '2018-03-28 09:59:41', '24c1c313-c903-4846-a805-5e49e23336b3'),
  (2, 2, 'en', 1, 'news/{postDate.year}/{slug}', NULL, '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'a270d4a7-180c-4237-a8b2-15f6cf890133'),
  (3, 3, 'en', 1, 'pages/{slug}', NULL, '2018-04-02 13:55:56', '2018-04-04 08:38:44', '7df66c74-ab18-44a9-8213-2e38a523dd74'),
  (4, 4, 'en', 1, NULL, NULL, '2018-04-03 14:57:48', '2018-04-03 14:57:48', '22771be0-6923-4a06-aa54-440248b19aca'),
  (5, 5, 'en', 1, 'feedback-list/{slug}', NULL, '2018-04-10 12:10:46', '2018-04-10 12:10:46', 'e086655d-b901-4c4c-8cfa-156afb4c1eb8'),
  (6, 6, 'en', 1, 'price-lists/{slug}', NULL, '2018-04-16 13:52:02', '2018-04-16 13:52:02', 'f6c3bcd2-dbc8-429a-bc45-20bd5fded0c2'),
  (7, 7, 'en', 1, 'blog/{slug}', NULL, '2018-05-03 11:13:44', '2018-05-03 11:13:44', '89fdf58a-12d7-43d6-adbe-c87bca1f8d9f');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_sessions`
--

CREATE TABLE `craft_sessions` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_sessions`
--

INSERT INTO `craft_sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 1, '2f2d139a05b4a6cc082ec6146a1ecc16690c4905czozMjoiZmplT1hTQn56MUw2Um9KdndIV0FuVjZKT3d4M3NMWHciOw==', '2018-03-28 09:59:41', '2018-03-28 09:59:41', '4e229a12-845e-4d9a-a5cc-4a90129f2178'),
  (2, 1, '24f194a35e69e3e7fcb5e9a60ca39f71308cfa01czozMjoiNkNjNlBxYnNuOTB2cHd4bnVtWVBWTjBiN3VjTFd4Qk8iOw==', '2018-03-28 11:51:54', '2018-03-28 11:51:54', '194147ec-44e3-4e4c-bd78-17b926e8f479'),
  (8, 1, 'bd6ccb017cf1bc02fee4cd0cde1a6038616ca8f2czozMjoiRFdrSlJMWmdlZWN3QjV3bkhCeXJha1dZWlhiVktaSk0iOw==', '2018-03-28 14:49:39', '2018-03-28 14:49:39', '4d84094a-60d3-40fa-bc09-8408febc774a'),
  (10, 1, 'fd1105854801fa7d9fa66685059281fe8f9b7663czozMjoiQ3d5NjFzS1BnYkdXbVdMQzgya29MMmp5V2VEaFQwQ20iOw==', '2018-04-03 09:07:48', '2018-04-03 09:07:48', 'c92ecfc1-e5d0-4720-97fa-798c005be899'),
  (11, 1, '7ebc9c69b14dfa04265e424f5caf3361981ba5c0czozMjoidVZ6dThsVnU3ckhKYVUxQnNzYXlXV1ZOTm1WUnpjVWMiOw==', '2018-04-03 14:53:24', '2018-04-03 14:53:24', '1f908f30-08ec-4a19-a6d2-d0445021fdb1'),
  (12, 1, 'eb4b51356171742388eabf1cd26904c1abdf126fczozMjoiTXpsdGZHMHphVmpCUXRSQWxwVTV+ZGw5eVRQVjRkakIiOw==', '2018-04-04 08:11:29', '2018-04-04 08:11:29', 'a128bcbf-7f3f-4ff6-b160-084785ec317c'),
  (13, 1, '4c0f5d14158e42c33782eed909cce29b0ccc389bczozMjoidmpFS0VVMWNTMFY4Y1lBd1pUSGRQTzZDWVFhS2ljZ0kiOw==', '2018-04-04 12:43:29', '2018-04-04 12:43:29', '8dcd17c9-252b-4e58-a523-d685bbda82ca'),
  (14, 1, 'a57c297d9fc2bd6329c0fc3513b2c217a542a9deczozMjoibWdsa2tTSnd6eWpPYzRMRXlxVVZYRGFDenZFRUlzZ1AiOw==', '2018-04-05 07:57:57', '2018-04-05 07:57:57', 'b57b2005-1021-4465-8a70-cafa41882cc5'),
  (15, 1, '54f707a4799bfd7d74e0ab9b073d070ebb7e145fczozMjoiM3A3a3pVTm5IbHRSb1EySmJkTnJJT2JEWmtRckFJSDciOw==', '2018-04-05 09:31:01', '2018-04-05 09:31:01', '24e810d5-f948-4ebf-a99b-38ea2e99baef'),
  (18, 1, '8a6bdcb79b614349f8aa69d1acdd1ea424d4b4c8czozMjoiVndOTHJTdDU0ajdpMG1xUFRoY2R6TENMVmhVNGlFWDQiOw==', '2018-04-30 14:37:09', '2018-04-30 14:37:09', 'efff254e-0ae5-4e82-ba93-9d68aa2de018');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_shunnedmessages`
--

CREATE TABLE `craft_shunnedmessages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_structureelements`
--

CREATE TABLE `craft_structureelements` (
  `id` int(11) NOT NULL,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) UNSIGNED DEFAULT NULL,
  `lft` int(11) UNSIGNED NOT NULL,
  `rgt` int(11) UNSIGNED NOT NULL,
  `level` smallint(6) UNSIGNED NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_structures`
--

CREATE TABLE `craft_structures` (
  `id` int(11) NOT NULL,
  `maxLevels` smallint(6) UNSIGNED DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_systemsettings`
--

CREATE TABLE `craft_systemsettings` (
  `id` int(11) NOT NULL,
  `category` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_systemsettings`
--

INSERT INTO `craft_systemsettings` (`id`, `category`, `settings`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 'email', '{\"protocol\":\"php\",\"emailAddress\":\"kiruha_i@mail.ru\",\"senderName\":\"Bark And Fly\"}', '2018-03-28 09:59:41', '2018-03-28 09:59:41', '86be1a7d-d6ef-439d-8250-a8a2bc3d2493');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_taggroups`
--

CREATE TABLE `craft_taggroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldLayoutId` int(10) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_taggroups`
--

INSERT INTO `craft_taggroups` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 'Default', 'default', 1, '2018-03-28 09:59:41', '2018-03-28 09:59:41', 'bad64e83-4073-463b-ad5c-68d623defc06');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_tags`
--

CREATE TABLE `craft_tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_tasks`
--

CREATE TABLE `craft_tasks` (
  `id` int(11) NOT NULL,
  `root` int(11) UNSIGNED DEFAULT NULL,
  `lft` int(11) UNSIGNED NOT NULL,
  `rgt` int(11) UNSIGNED NOT NULL,
  `level` smallint(6) UNSIGNED NOT NULL,
  `currentStep` int(11) UNSIGNED DEFAULT NULL,
  `totalSteps` int(11) UNSIGNED DEFAULT NULL,
  `status` enum('pending','error','running') COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `settings` mediumtext COLLATE utf8_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_templatecachecriteria`
--

CREATE TABLE `craft_templatecachecriteria` (
  `id` int(11) NOT NULL,
  `cacheId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `criteria` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_templatecacheelements`
--

CREATE TABLE `craft_templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_templatecaches`
--

CREATE TABLE `craft_templatecaches` (
  `id` int(11) NOT NULL,
  `cacheKey` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locale` char(12) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_tokens`
--

CREATE TABLE `craft_tokens` (
  `id` int(11) NOT NULL,
  `token` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `route` text COLLATE utf8_unicode_ci,
  `usageLimit` tinyint(3) UNSIGNED DEFAULT NULL,
  `usageCount` tinyint(3) UNSIGNED DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_usergroups`
--

CREATE TABLE `craft_usergroups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `handle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_usergroups_users`
--

CREATE TABLE `craft_usergroups_users` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_userpermissions`
--

CREATE TABLE `craft_userpermissions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_userpermissions_usergroups`
--

CREATE TABLE `craft_userpermissions_usergroups` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_userpermissions_users`
--

CREATE TABLE `craft_userpermissions_users` (
  `id` int(11) NOT NULL,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `craft_users`
--

CREATE TABLE `craft_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `preferredLocale` char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weekStartDay` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `admin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `client` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `locked` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `suspended` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `pending` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `archived` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIPAddress` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(4) UNSIGNED DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `verificationCode` char(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passwordResetRequired` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_users`
--

INSERT INTO `craft_users` (`id`, `username`, `photo`, `firstName`, `lastName`, `email`, `password`, `preferredLocale`, `weekStartDay`, `admin`, `client`, `locked`, `suspended`, `pending`, `archived`, `lastLoginDate`, `lastLoginAttemptIPAddress`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 'Admin', NULL, '', '', 'kiruha_i@mail.ru', '$2y$13$hYLDToNy0gb6Xw2eXqMAVelcdNVKr8ZXacR9Ql/winegaoiQpyUc2', NULL, 1, 1, 0, 0, 0, 0, 0, '2018-04-30 14:37:09', '::1', NULL, NULL, '2018-03-28 14:49:31', NULL, NULL, NULL, NULL, 0, '2018-03-28 14:48:51', '2018-03-28 09:59:37', '2018-04-30 14:37:09', '906f7001-4960-4195-9b42-2a8be5b0c20a');

-- --------------------------------------------------------

--
-- Структура таблицы `craft_widgets`
--

CREATE TABLE `craft_widgets` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `type` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sortOrder` smallint(6) UNSIGNED DEFAULT NULL,
  `colspan` tinyint(4) UNSIGNED DEFAULT NULL,
  `settings` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `craft_widgets`
--

INSERT INTO `craft_widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`) VALUES
  (1, 1, 'RecentEntries', 1, NULL, NULL, 1, '2018-03-28 09:59:58', '2018-03-28 09:59:58', '36736246-2d86-42f4-b8cf-1ce5e56c3342'),
  (2, 1, 'GetHelp', 2, NULL, NULL, 1, '2018-03-28 09:59:58', '2018-03-28 09:59:58', '9560b09d-956d-4a88-90cf-147933f6f507'),
  (3, 1, 'Updates', 3, NULL, NULL, 1, '2018-03-28 09:59:58', '2018-03-28 09:59:58', 'f02c4657-e589-4cb2-ba81-1c03d0c1a340'),
  (4, 1, 'Feed', 4, NULL, '{\"url\":\"https:\\/\\/craftcms.com\\/news.rss\",\"title\":\"Craft News\"}', 1, '2018-03-28 09:59:58', '2018-03-28 09:59:58', '263febd5-a940-4c6c-bc68-c565858591a2');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `craft_assetfiles`
--
ALTER TABLE `craft_assetfiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_assetfiles_filename_folderId_unq_idx` (`filename`,`folderId`),
  ADD KEY `craft_assetfiles_sourceId_fk` (`sourceId`),
  ADD KEY `craft_assetfiles_folderId_fk` (`folderId`);

--
-- Индексы таблицы `craft_assetfolders`
--
ALTER TABLE `craft_assetfolders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_assetfolders_name_parentId_sourceId_unq_idx` (`name`,`parentId`,`sourceId`),
  ADD KEY `craft_assetfolders_parentId_fk` (`parentId`),
  ADD KEY `craft_assetfolders_sourceId_fk` (`sourceId`);

--
-- Индексы таблицы `craft_assetindexdata`
--
ALTER TABLE `craft_assetindexdata`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_assetindexdata_sessionId_sourceId_offset_unq_idx` (`sessionId`,`sourceId`,`offset`),
  ADD KEY `craft_assetindexdata_sourceId_fk` (`sourceId`);

--
-- Индексы таблицы `craft_assetsources`
--
ALTER TABLE `craft_assetsources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_assetsources_name_unq_idx` (`name`),
  ADD UNIQUE KEY `craft_assetsources_handle_unq_idx` (`handle`),
  ADD KEY `craft_assetsources_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Индексы таблицы `craft_assettransformindex`
--
ALTER TABLE `craft_assettransformindex`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_assettransformindex_sourceId_fileId_location_idx` (`sourceId`,`fileId`,`location`);

--
-- Индексы таблицы `craft_assettransforms`
--
ALTER TABLE `craft_assettransforms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_assettransforms_name_unq_idx` (`name`),
  ADD UNIQUE KEY `craft_assettransforms_handle_unq_idx` (`handle`);

--
-- Индексы таблицы `craft_categories`
--
ALTER TABLE `craft_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_categories_groupId_fk` (`groupId`);

--
-- Индексы таблицы `craft_categorygroups`
--
ALTER TABLE `craft_categorygroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_categorygroups_name_unq_idx` (`name`),
  ADD UNIQUE KEY `craft_categorygroups_handle_unq_idx` (`handle`),
  ADD KEY `craft_categorygroups_structureId_fk` (`structureId`),
  ADD KEY `craft_categorygroups_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Индексы таблицы `craft_categorygroups_i18n`
--
ALTER TABLE `craft_categorygroups_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_categorygroups_i18n_groupId_locale_unq_idx` (`groupId`,`locale`),
  ADD KEY `craft_categorygroups_i18n_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_content`
--
ALTER TABLE `craft_content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_content_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_content_title_idx` (`title`),
  ADD KEY `craft_content_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_deprecationerrors`
--
ALTER TABLE `craft_deprecationerrors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`);

--
-- Индексы таблицы `craft_elementindexsettings`
--
ALTER TABLE `craft_elementindexsettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_elementindexsettings_type_unq_idx` (`type`);

--
-- Индексы таблицы `craft_elements`
--
ALTER TABLE `craft_elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_elements_type_idx` (`type`),
  ADD KEY `craft_elements_enabled_idx` (`enabled`),
  ADD KEY `craft_elements_archived_dateCreated_idx` (`archived`,`dateCreated`);

--
-- Индексы таблицы `craft_elements_i18n`
--
ALTER TABLE `craft_elements_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_elements_i18n_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD UNIQUE KEY `craft_elements_i18n_uri_locale_unq_idx` (`uri`,`locale`),
  ADD KEY `craft_elements_i18n_slug_locale_idx` (`slug`,`locale`),
  ADD KEY `craft_elements_i18n_enabled_idx` (`enabled`),
  ADD KEY `craft_elements_i18n_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_emailmessages`
--
ALTER TABLE `craft_emailmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_emailmessages_key_locale_unq_idx` (`key`,`locale`),
  ADD KEY `craft_emailmessages_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_entries`
--
ALTER TABLE `craft_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_entries_sectionId_idx` (`sectionId`),
  ADD KEY `craft_entries_typeId_idx` (`typeId`),
  ADD KEY `craft_entries_postDate_idx` (`postDate`),
  ADD KEY `craft_entries_expiryDate_idx` (`expiryDate`),
  ADD KEY `craft_entries_authorId_fk` (`authorId`);

--
-- Индексы таблицы `craft_entrydrafts`
--
ALTER TABLE `craft_entrydrafts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_entrydrafts_entryId_locale_idx` (`entryId`,`locale`),
  ADD KEY `craft_entrydrafts_sectionId_fk` (`sectionId`),
  ADD KEY `craft_entrydrafts_creatorId_fk` (`creatorId`),
  ADD KEY `craft_entrydrafts_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_entrytypes`
--
ALTER TABLE `craft_entrytypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_entrytypes_name_sectionId_unq_idx` (`name`,`sectionId`),
  ADD UNIQUE KEY `craft_entrytypes_handle_sectionId_unq_idx` (`handle`,`sectionId`),
  ADD KEY `craft_entrytypes_sectionId_fk` (`sectionId`),
  ADD KEY `craft_entrytypes_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Индексы таблицы `craft_entryversions`
--
ALTER TABLE `craft_entryversions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_entryversions_entryId_locale_idx` (`entryId`,`locale`),
  ADD KEY `craft_entryversions_sectionId_fk` (`sectionId`),
  ADD KEY `craft_entryversions_creatorId_fk` (`creatorId`),
  ADD KEY `craft_entryversions_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_fieldgroups`
--
ALTER TABLE `craft_fieldgroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_fieldgroups_name_unq_idx` (`name`);

--
-- Индексы таблицы `craft_fieldlayoutfields`
--
ALTER TABLE `craft_fieldlayoutfields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  ADD KEY `craft_fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  ADD KEY `craft_fieldlayoutfields_tabId_fk` (`tabId`),
  ADD KEY `craft_fieldlayoutfields_fieldId_fk` (`fieldId`);

--
-- Индексы таблицы `craft_fieldlayouts`
--
ALTER TABLE `craft_fieldlayouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_fieldlayouts_type_idx` (`type`);

--
-- Индексы таблицы `craft_fieldlayouttabs`
--
ALTER TABLE `craft_fieldlayouttabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  ADD KEY `craft_fieldlayouttabs_layoutId_fk` (`layoutId`);

--
-- Индексы таблицы `craft_fields`
--
ALTER TABLE `craft_fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_fields_handle_context_unq_idx` (`handle`,`context`),
  ADD KEY `craft_fields_context_idx` (`context`),
  ADD KEY `craft_fields_groupId_fk` (`groupId`);

--
-- Индексы таблицы `craft_globalsets`
--
ALTER TABLE `craft_globalsets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_globalsets_name_unq_idx` (`name`),
  ADD UNIQUE KEY `craft_globalsets_handle_unq_idx` (`handle`),
  ADD KEY `craft_globalsets_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Индексы таблицы `craft_info`
--
ALTER TABLE `craft_info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `craft_locales`
--
ALTER TABLE `craft_locales`
  ADD PRIMARY KEY (`locale`),
  ADD KEY `craft_locales_sortOrder_idx` (`sortOrder`);

--
-- Индексы таблицы `craft_matrixblocks`
--
ALTER TABLE `craft_matrixblocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_matrixblocks_ownerId_idx` (`ownerId`),
  ADD KEY `craft_matrixblocks_fieldId_idx` (`fieldId`),
  ADD KEY `craft_matrixblocks_typeId_idx` (`typeId`),
  ADD KEY `craft_matrixblocks_sortOrder_idx` (`sortOrder`),
  ADD KEY `craft_matrixblocks_ownerLocale_fk` (`ownerLocale`);

--
-- Индексы таблицы `craft_matrixblocktypes`
--
ALTER TABLE `craft_matrixblocktypes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  ADD UNIQUE KEY `craft_matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  ADD KEY `craft_matrixblocktypes_fieldId_fk` (`fieldId`),
  ADD KEY `craft_matrixblocktypes_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Индексы таблицы `craft_matrixcontent_advertisement`
--
ALTER TABLE `craft_matrixcontent_advertisement`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_advertisement_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_advertisement_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_bookingpanel`
--
ALTER TABLE `craft_matrixcontent_bookingpanel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_bookingpanel_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_bookingpanel_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_covercaptionlinkbutton`
--
ALTER TABLE `craft_matrixcontent_covercaptionlinkbutton`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixconten_covercaptionlinkbutto_elementId_locale_unq_id` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_covercaptionlinkbutton_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_facilitieslist`
--
ALTER TABLE `craft_matrixcontent_facilitieslist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_facilitieslist_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_facilitieslist_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_featuredservices`
--
ALTER TABLE `craft_matrixcontent_featuredservices`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_featuredservices_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_featuredservices_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_feedbacklist`
--
ALTER TABLE `craft_matrixcontent_feedbacklist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_feedbacklist_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_feedbacklist_locale_idx` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_mapsfield`
--
ALTER TABLE `craft_matrixcontent_mapsfield`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_mapsfield_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_mapsfield_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_pagecover`
--
ALTER TABLE `craft_matrixcontent_pagecover`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_pagecover_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_pagecover_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_post`
--
ALTER TABLE `craft_matrixcontent_post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_post_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_post_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_priceditems`
--
ALTER TABLE `craft_matrixcontent_priceditems`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_priceditems_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_priceditems_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_serviceslist`
--
ALTER TABLE `craft_matrixcontent_serviceslist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_serviceslist_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_serviceslist_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_storyboardgallery`
--
ALTER TABLE `craft_matrixcontent_storyboardgallery`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_storyboardgallery_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_storyboardgallery_locale_idx` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_topcarousel`
--
ALTER TABLE `craft_matrixcontent_topcarousel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_topcarousel_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_topcarousel_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_trainingslist`
--
ALTER TABLE `craft_matrixcontent_trainingslist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_trainingslist_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_trainingslist_locale_idx` (`locale`);

--
-- Индексы таблицы `craft_matrixcontent_videopanel`
--
ALTER TABLE `craft_matrixcontent_videopanel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_matrixcontent_videopanel_elementId_locale_unq_idx` (`elementId`,`locale`),
  ADD KEY `craft_matrixcontent_videopanel_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_migrations`
--
ALTER TABLE `craft_migrations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_migrations_version_unq_idx` (`version`),
  ADD KEY `craft_migrations_pluginId_fk` (`pluginId`);

--
-- Индексы таблицы `craft_plugins`
--
ALTER TABLE `craft_plugins`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `craft_rackspaceaccess`
--
ALTER TABLE `craft_rackspaceaccess`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_rackspaceaccess_connectionKey_unq_idx` (`connectionKey`);

--
-- Индексы таблицы `craft_relations`
--
ALTER TABLE `craft_relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_relations_fieldId_sourceId_sourceLocale_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceLocale`,`targetId`),
  ADD KEY `craft_relations_sourceId_fk` (`sourceId`),
  ADD KEY `craft_relations_sourceLocale_fk` (`sourceLocale`),
  ADD KEY `craft_relations_targetId_fk` (`targetId`);

--
-- Индексы таблицы `craft_routes`
--
ALTER TABLE `craft_routes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_routes_locale_idx` (`locale`),
  ADD KEY `craft_routes_urlPattern_idx` (`urlPattern`);

--
-- Индексы таблицы `craft_searchindex`
--
ALTER TABLE `craft_searchindex`
  ADD PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`locale`);
ALTER TABLE `craft_searchindex` ADD FULLTEXT KEY `craft_searchindex_keywords_idx` (`keywords`);

--
-- Индексы таблицы `craft_sections`
--
ALTER TABLE `craft_sections`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_sections_name_unq_idx` (`name`),
  ADD UNIQUE KEY `craft_sections_handle_unq_idx` (`handle`),
  ADD KEY `craft_sections_structureId_fk` (`structureId`);

--
-- Индексы таблицы `craft_sections_i18n`
--
ALTER TABLE `craft_sections_i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_sections_i18n_sectionId_locale_unq_idx` (`sectionId`,`locale`),
  ADD KEY `craft_sections_i18n_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_sessions`
--
ALTER TABLE `craft_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_sessions_uid_idx` (`uid`),
  ADD KEY `craft_sessions_token_idx` (`token`),
  ADD KEY `craft_sessions_dateUpdated_idx` (`dateUpdated`),
  ADD KEY `craft_sessions_userId_fk` (`userId`);

--
-- Индексы таблицы `craft_shunnedmessages`
--
ALTER TABLE `craft_shunnedmessages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_shunnedmessages_userId_message_unq_idx` (`userId`,`message`);

--
-- Индексы таблицы `craft_structureelements`
--
ALTER TABLE `craft_structureelements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  ADD KEY `craft_structureelements_root_idx` (`root`),
  ADD KEY `craft_structureelements_lft_idx` (`lft`),
  ADD KEY `craft_structureelements_rgt_idx` (`rgt`),
  ADD KEY `craft_structureelements_level_idx` (`level`),
  ADD KEY `craft_structureelements_elementId_fk` (`elementId`);

--
-- Индексы таблицы `craft_structures`
--
ALTER TABLE `craft_structures`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `craft_systemsettings`
--
ALTER TABLE `craft_systemsettings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_systemsettings_category_unq_idx` (`category`);

--
-- Индексы таблицы `craft_taggroups`
--
ALTER TABLE `craft_taggroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_taggroups_name_unq_idx` (`name`),
  ADD UNIQUE KEY `craft_taggroups_handle_unq_idx` (`handle`),
  ADD KEY `craft_taggroups_fieldLayoutId_fk` (`fieldLayoutId`);

--
-- Индексы таблицы `craft_tags`
--
ALTER TABLE `craft_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_tags_groupId_fk` (`groupId`);

--
-- Индексы таблицы `craft_tasks`
--
ALTER TABLE `craft_tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_tasks_root_idx` (`root`),
  ADD KEY `craft_tasks_lft_idx` (`lft`),
  ADD KEY `craft_tasks_rgt_idx` (`rgt`),
  ADD KEY `craft_tasks_level_idx` (`level`);

--
-- Индексы таблицы `craft_templatecachecriteria`
--
ALTER TABLE `craft_templatecachecriteria`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_templatecachecriteria_cacheId_fk` (`cacheId`),
  ADD KEY `craft_templatecachecriteria_type_idx` (`type`);

--
-- Индексы таблицы `craft_templatecacheelements`
--
ALTER TABLE `craft_templatecacheelements`
  ADD KEY `craft_templatecacheelements_cacheId_fk` (`cacheId`),
  ADD KEY `craft_templatecacheelements_elementId_fk` (`elementId`);

--
-- Индексы таблицы `craft_templatecaches`
--
ALTER TABLE `craft_templatecaches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_templatecaches_cacheKey_locale_expiryDate_path_idx` (`cacheKey`,`locale`,`expiryDate`,`path`),
  ADD KEY `craft_templatecaches_cacheKey_locale_expiryDate_idx` (`cacheKey`,`locale`,`expiryDate`),
  ADD KEY `craft_templatecaches_locale_fk` (`locale`);

--
-- Индексы таблицы `craft_tokens`
--
ALTER TABLE `craft_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_tokens_token_unq_idx` (`token`),
  ADD KEY `craft_tokens_expiryDate_idx` (`expiryDate`);

--
-- Индексы таблицы `craft_usergroups`
--
ALTER TABLE `craft_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_usergroups_name_unq_idx` (`name`),
  ADD UNIQUE KEY `craft_usergroups_handle_unq_idx` (`handle`);

--
-- Индексы таблицы `craft_usergroups_users`
--
ALTER TABLE `craft_usergroups_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  ADD KEY `craft_usergroups_users_userId_fk` (`userId`);

--
-- Индексы таблицы `craft_userpermissions`
--
ALTER TABLE `craft_userpermissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_userpermissions_name_unq_idx` (`name`);

--
-- Индексы таблицы `craft_userpermissions_usergroups`
--
ALTER TABLE `craft_userpermissions_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  ADD KEY `craft_userpermissions_usergroups_groupId_fk` (`groupId`);

--
-- Индексы таблицы `craft_userpermissions_users`
--
ALTER TABLE `craft_userpermissions_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  ADD KEY `craft_userpermissions_users_userId_fk` (`userId`);

--
-- Индексы таблицы `craft_users`
--
ALTER TABLE `craft_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `craft_users_username_unq_idx` (`username`),
  ADD UNIQUE KEY `craft_users_email_unq_idx` (`email`),
  ADD KEY `craft_users_verificationCode_idx` (`verificationCode`),
  ADD KEY `craft_users_uid_idx` (`uid`),
  ADD KEY `craft_users_preferredLocale_fk` (`preferredLocale`);

--
-- Индексы таблицы `craft_widgets`
--
ALTER TABLE `craft_widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `craft_widgets_userId_fk` (`userId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `craft_assetfolders`
--
ALTER TABLE `craft_assetfolders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `craft_assetindexdata`
--
ALTER TABLE `craft_assetindexdata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_assetsources`
--
ALTER TABLE `craft_assetsources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `craft_assettransformindex`
--
ALTER TABLE `craft_assettransformindex`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_assettransforms`
--
ALTER TABLE `craft_assettransforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_categorygroups`
--
ALTER TABLE `craft_categorygroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_categorygroups_i18n`
--
ALTER TABLE `craft_categorygroups_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_content`
--
ALTER TABLE `craft_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT для таблицы `craft_deprecationerrors`
--
ALTER TABLE `craft_deprecationerrors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_elementindexsettings`
--
ALTER TABLE `craft_elementindexsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `craft_elements`
--
ALTER TABLE `craft_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT для таблицы `craft_elements_i18n`
--
ALTER TABLE `craft_elements_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT для таблицы `craft_emailmessages`
--
ALTER TABLE `craft_emailmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_entrydrafts`
--
ALTER TABLE `craft_entrydrafts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_entrytypes`
--
ALTER TABLE `craft_entrytypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `craft_entryversions`
--
ALTER TABLE `craft_entryversions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT для таблицы `craft_fieldgroups`
--
ALTER TABLE `craft_fieldgroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `craft_fieldlayoutfields`
--
ALTER TABLE `craft_fieldlayoutfields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=506;

--
-- AUTO_INCREMENT для таблицы `craft_fieldlayouts`
--
ALTER TABLE `craft_fieldlayouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT для таблицы `craft_fieldlayouttabs`
--
ALTER TABLE `craft_fieldlayouttabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT для таблицы `craft_fields`
--
ALTER TABLE `craft_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT для таблицы `craft_info`
--
ALTER TABLE `craft_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `craft_matrixblocktypes`
--
ALTER TABLE `craft_matrixblocktypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_advertisement`
--
ALTER TABLE `craft_matrixcontent_advertisement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_bookingpanel`
--
ALTER TABLE `craft_matrixcontent_bookingpanel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_covercaptionlinkbutton`
--
ALTER TABLE `craft_matrixcontent_covercaptionlinkbutton`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_facilitieslist`
--
ALTER TABLE `craft_matrixcontent_facilitieslist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_featuredservices`
--
ALTER TABLE `craft_matrixcontent_featuredservices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_feedbacklist`
--
ALTER TABLE `craft_matrixcontent_feedbacklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_mapsfield`
--
ALTER TABLE `craft_matrixcontent_mapsfield`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_pagecover`
--
ALTER TABLE `craft_matrixcontent_pagecover`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_post`
--
ALTER TABLE `craft_matrixcontent_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_priceditems`
--
ALTER TABLE `craft_matrixcontent_priceditems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_serviceslist`
--
ALTER TABLE `craft_matrixcontent_serviceslist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_storyboardgallery`
--
ALTER TABLE `craft_matrixcontent_storyboardgallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_topcarousel`
--
ALTER TABLE `craft_matrixcontent_topcarousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_trainingslist`
--
ALTER TABLE `craft_matrixcontent_trainingslist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `craft_matrixcontent_videopanel`
--
ALTER TABLE `craft_matrixcontent_videopanel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `craft_migrations`
--
ALTER TABLE `craft_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT для таблицы `craft_plugins`
--
ALTER TABLE `craft_plugins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_rackspaceaccess`
--
ALTER TABLE `craft_rackspaceaccess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_relations`
--
ALTER TABLE `craft_relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT для таблицы `craft_routes`
--
ALTER TABLE `craft_routes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_sections`
--
ALTER TABLE `craft_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `craft_sections_i18n`
--
ALTER TABLE `craft_sections_i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `craft_sessions`
--
ALTER TABLE `craft_sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `craft_shunnedmessages`
--
ALTER TABLE `craft_shunnedmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_structureelements`
--
ALTER TABLE `craft_structureelements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_structures`
--
ALTER TABLE `craft_structures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_systemsettings`
--
ALTER TABLE `craft_systemsettings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `craft_taggroups`
--
ALTER TABLE `craft_taggroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `craft_tasks`
--
ALTER TABLE `craft_tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `craft_templatecachecriteria`
--
ALTER TABLE `craft_templatecachecriteria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_templatecaches`
--
ALTER TABLE `craft_templatecaches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_tokens`
--
ALTER TABLE `craft_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_usergroups`
--
ALTER TABLE `craft_usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_usergroups_users`
--
ALTER TABLE `craft_usergroups_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_userpermissions`
--
ALTER TABLE `craft_userpermissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_userpermissions_usergroups`
--
ALTER TABLE `craft_userpermissions_usergroups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_userpermissions_users`
--
ALTER TABLE `craft_userpermissions_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `craft_widgets`
--
ALTER TABLE `craft_widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `craft_assetfiles`
--
ALTER TABLE `craft_assetfiles`
  ADD CONSTRAINT `craft_assetfiles_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_assetfiles_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_assetfiles_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_assetfolders`
--
ALTER TABLE `craft_assetfolders`
  ADD CONSTRAINT `craft_assetfolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `craft_assetfolders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_assetfolders_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_assetindexdata`
--
ALTER TABLE `craft_assetindexdata`
  ADD CONSTRAINT `craft_assetindexdata_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_assetsources` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_assetsources`
--
ALTER TABLE `craft_assetsources`
  ADD CONSTRAINT `craft_assetsources_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `craft_categories`
--
ALTER TABLE `craft_categories`
  ADD CONSTRAINT `craft_categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_categories_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_categorygroups`
--
ALTER TABLE `craft_categorygroups`
  ADD CONSTRAINT `craft_categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `craft_categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_categorygroups_i18n`
--
ALTER TABLE `craft_categorygroups_i18n`
  ADD CONSTRAINT `craft_categorygroups_i18n_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_categorygroups_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_content`
--
ALTER TABLE `craft_content`
  ADD CONSTRAINT `craft_content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_content_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_elements_i18n`
--
ALTER TABLE `craft_elements_i18n`
  ADD CONSTRAINT `craft_elements_i18n_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_elements_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_emailmessages`
--
ALTER TABLE `craft_emailmessages`
  ADD CONSTRAINT `craft_emailmessages_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_entries`
--
ALTER TABLE `craft_entries`
  ADD CONSTRAINT `craft_entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entries_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_entrytypes` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_entrydrafts`
--
ALTER TABLE `craft_entrydrafts`
  ADD CONSTRAINT `craft_entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entrydrafts_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_entrytypes`
--
ALTER TABLE `craft_entrytypes`
  ADD CONSTRAINT `craft_entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `craft_entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_entryversions`
--
ALTER TABLE `craft_entryversions`
  ADD CONSTRAINT `craft_entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `craft_entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_entryversions_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_fieldlayoutfields`
--
ALTER TABLE `craft_fieldlayoutfields`
  ADD CONSTRAINT `craft_fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `craft_fieldlayouttabs` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_fieldlayouttabs`
--
ALTER TABLE `craft_fieldlayouttabs`
  ADD CONSTRAINT `craft_fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_fields`
--
ALTER TABLE `craft_fields`
  ADD CONSTRAINT `craft_fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_fieldgroups` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_globalsets`
--
ALTER TABLE `craft_globalsets`
  ADD CONSTRAINT `craft_globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `craft_globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixblocks`
--
ALTER TABLE `craft_matrixblocks`
  ADD CONSTRAINT `craft_matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixblocks_ownerLocale_fk` FOREIGN KEY (`ownerLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixblocktypes`
--
ALTER TABLE `craft_matrixblocktypes`
  ADD CONSTRAINT `craft_matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_advertisement`
--
ALTER TABLE `craft_matrixcontent_advertisement`
  ADD CONSTRAINT `craft_matrixcontent_advertisement_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_advertisement_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_bookingpanel`
--
ALTER TABLE `craft_matrixcontent_bookingpanel`
  ADD CONSTRAINT `craft_matrixcontent_bookingpanel_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_bookingpanel_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_covercaptionlinkbutton`
--
ALTER TABLE `craft_matrixcontent_covercaptionlinkbutton`
  ADD CONSTRAINT `craft_matrixcontent_covercaptionlinkbutton_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_covercaptionlinkbutton_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_facilitieslist`
--
ALTER TABLE `craft_matrixcontent_facilitieslist`
  ADD CONSTRAINT `craft_matrixcontent_facilitieslist_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_facilitieslist_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_featuredservices`
--
ALTER TABLE `craft_matrixcontent_featuredservices`
  ADD CONSTRAINT `craft_matrixcontent_featuredservices_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_featuredservices_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_feedbacklist`
--
ALTER TABLE `craft_matrixcontent_feedbacklist`
  ADD CONSTRAINT `craft_matrixcontent_feedbacklist_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_feedbacklist_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_mapsfield`
--
ALTER TABLE `craft_matrixcontent_mapsfield`
  ADD CONSTRAINT `craft_matrixcontent_mapsfield_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_mapsfield_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_pagecover`
--
ALTER TABLE `craft_matrixcontent_pagecover`
  ADD CONSTRAINT `craft_matrixcontent_pagecover_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_pagecover_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_post`
--
ALTER TABLE `craft_matrixcontent_post`
  ADD CONSTRAINT `craft_matrixcontent_post_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_post_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_priceditems`
--
ALTER TABLE `craft_matrixcontent_priceditems`
  ADD CONSTRAINT `craft_matrixcontent_priceditems_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_priceditems_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_serviceslist`
--
ALTER TABLE `craft_matrixcontent_serviceslist`
  ADD CONSTRAINT `craft_matrixcontent_serviceslist_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_serviceslist_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_storyboardgallery`
--
ALTER TABLE `craft_matrixcontent_storyboardgallery`
  ADD CONSTRAINT `craft_matrixcontent_storyboardgallery_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_storyboardgallery_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_topcarousel`
--
ALTER TABLE `craft_matrixcontent_topcarousel`
  ADD CONSTRAINT `craft_matrixcontent_topcarousel_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_topcarousel_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_trainingslist`
--
ALTER TABLE `craft_matrixcontent_trainingslist`
  ADD CONSTRAINT `craft_matrixcontent_trainingslist_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_trainingslist_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_matrixcontent_videopanel`
--
ALTER TABLE `craft_matrixcontent_videopanel`
  ADD CONSTRAINT `craft_matrixcontent_videopanel_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_matrixcontent_videopanel_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_migrations`
--
ALTER TABLE `craft_migrations`
  ADD CONSTRAINT `craft_migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `craft_plugins` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_relations`
--
ALTER TABLE `craft_relations`
  ADD CONSTRAINT `craft_relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_relations_sourceLocale_fk` FOREIGN KEY (`sourceLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_routes`
--
ALTER TABLE `craft_routes`
  ADD CONSTRAINT `craft_routes_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_sections`
--
ALTER TABLE `craft_sections`
  ADD CONSTRAINT `craft_sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `craft_sections_i18n`
--
ALTER TABLE `craft_sections_i18n`
  ADD CONSTRAINT `craft_sections_i18n_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `craft_sections_i18n_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_sessions`
--
ALTER TABLE `craft_sessions`
  ADD CONSTRAINT `craft_sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_shunnedmessages`
--
ALTER TABLE `craft_shunnedmessages`
  ADD CONSTRAINT `craft_shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_structureelements`
--
ALTER TABLE `craft_structureelements`
  ADD CONSTRAINT `craft_structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_taggroups`
--
ALTER TABLE `craft_taggroups`
  ADD CONSTRAINT `craft_taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL;

--
-- Ограничения внешнего ключа таблицы `craft_tags`
--
ALTER TABLE `craft_tags`
  ADD CONSTRAINT `craft_tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_taggroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_tags_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_templatecachecriteria`
--
ALTER TABLE `craft_templatecachecriteria`
  ADD CONSTRAINT `craft_templatecachecriteria_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_templatecacheelements`
--
ALTER TABLE `craft_templatecacheelements`
  ADD CONSTRAINT `craft_templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_templatecaches`
--
ALTER TABLE `craft_templatecaches`
  ADD CONSTRAINT `craft_templatecaches_locale_fk` FOREIGN KEY (`locale`) REFERENCES `craft_locales` (`locale`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_usergroups_users`
--
ALTER TABLE `craft_usergroups_users`
  ADD CONSTRAINT `craft_usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_userpermissions_usergroups`
--
ALTER TABLE `craft_userpermissions_usergroups`
  ADD CONSTRAINT `craft_userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_userpermissions_users`
--
ALTER TABLE `craft_userpermissions_users`
  ADD CONSTRAINT `craft_userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_users`
--
ALTER TABLE `craft_users`
  ADD CONSTRAINT `craft_users_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `craft_users_preferredLocale_fk` FOREIGN KEY (`preferredLocale`) REFERENCES `craft_locales` (`locale`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `craft_widgets`
--
ALTER TABLE `craft_widgets`
  ADD CONSTRAINT `craft_widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE;
