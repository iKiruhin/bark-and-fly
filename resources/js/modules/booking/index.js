import $ from 'jquery';
import moment from 'moment';
import _debounce from 'lodash/debounce';
import datepicker from 'air-datepicker';
import 'air-datepicker/dist/js/i18n/datepicker.en';
import 'air-datepicker/dist/css/datepicker.min.css';

import './style.scss';

export default class Booking {
    constructor(params) {
        if (!params.el) {
            console.warn('Booking calculator element not found');
            return;
        }

        this._currentInst = null;
        this._calendarBoarding = null;
        this._calendarDaycare = null;
        this._selectedDate = [];
        this._currentDate = [];
        this._dayDuration = null;
        this._params = params;

        Array.prototype.diff = function(a) {
            return this.filter(function(i) {return a.indexOf(i) < 0;});
        };
        this._debounceToggleHalfDay = _debounce(this._toggleHalfDayBg.bind(this), 0);
    }

    init() {
        this._$el = $(this._params.el);
        this._$calendarPlace = $('.js-calendar');
        this._$boardingButton = this._$el.find('.js-service-boarding');
        this._$daycareButton = this._$el.find('.js-service-daycare');

        this._$boardingButton.on('click', this._initBoardingCalendar.bind(this));
        this._$daycareButton.on('click', this._initDaycareCalendar.bind(this));

        if (this._$boardingButton.hasClass('active')) {
            this._initBoardingCalendar();
        }

        if (this._$daycareButton.hasClass('active')) {
            this._initDaycareCalendar();
        }
    }

    /**
     * Init boarding calendar
     * @private
     */
    _initBoardingCalendar() {
        this._clear().then(() => {
            this._calendarBoarding = this._$calendarPlace.datepicker({
                inline: true,
                language: 'en',
                firstDay: 1,
                range: true,
                minHours: 8,
                maxHours: 17,
                minutesStep: 5,
                timepicker: true,
                minDate: new Date(),
                onSelect: (formattedDate, date) => {
                    this._currentDate = date;
                    this._initSaveButton();
                },
            });
            this._$saveBoardingButton = this._$calendarPlace.append(this._getSaveButton()).find('.js-save-button');
            this._$resultList = this._$el.find('.js-calendar-result');
            this._$timePicker = this._$el.find('.datepicker--time');

            this._$resultList.on('click', '.list-group-rm-btn', this._onClickRemoveDate.bind(this));
            this._$saveBoardingButton.on('click', this._onClickSaveBoardingDate.bind(this));
        });
    }

    /**
     * Init Daycare calendar
     * @private
     */
    _initDaycareCalendar() {
        this._clear().then(() => {
            this._calendarDaycare = this._$calendarPlace.datepicker({
                inline: true,
                multipleDates: true,
                language: 'en',
                firstDay: 1,
                minHours: 8,
                maxHours: 17,
                minutesStep: 5,
                minDate: new Date(),
                onSelect: (formattedDate, date) => {
                    let diff = undefined;

                    if (this._currentDate.length && date.length) {
                        diff = this._currentDate.diff(date.map(item => item.toString()))[0];
                    }

                    this._toggleSelectDay(date.length ? diff || date.splice(-1)[0] : null);

                    this._currentDate = date.length ? date.map(item => item.toString()) : [];
                },
                onChangeMonth: this._debounceToggleHalfDay,
                onChangeYear: this._debounceToggleHalfDay,
                onChangeDecade: this._debounceToggleHalfDay,
            });

            this._dayDuration = 'full';
            this._$dayDurationButtons = this._$calendarPlace.append(this._getTypeButtons()).find('.js-duration-buttons');
            this._$dayDurationButtons.on('click', 'button', this._onClickToggleDuration.bind(this));
        });
    }

    /**
     * Init Boarding calendar save button
     * @private
     */
    _initSaveButton() {
        if (this._currentDate.length > 1) {
            this._$saveBoardingButton.show();
            this._$timePicker.addClass('-active-');
        } else {
            this._$saveBoardingButton.hide();
            this._$timePicker.removeClass('-active-');
        }
    }

    /**
     * Event handler - click on boarding save button
     * @private
     */
    _onClickSaveBoardingDate() {
        this._toggleSelectedDate();
        this._$resultList.append(this._getResultListItem());
        this._currentDate = [];
        this._calendarBoarding.data('datepicker').clear();
    }

    /**
     * Event handler - click on remove in selected dates item
     * @param event
     * @private
     */
    _onClickRemoveDate(event) {
        const $el = $(event.target);
        const $parent = $el.parent('.list-group-item');
        const id = parseInt($el.data('id'), 10);
        const index = this._selectedDate.findIndex(item => item.id === id);

        event.stopPropagation();

        this._selectedDate.splice(index, 1);
        $parent.fadeOut(200, () => ($parent.remove()));
    }

    /**
     * Event handler - toggle daycare day duration
     * @param event
     * @private
     */
    _onClickToggleDuration(event) {
        const $wrap = $(event.delegateTarget);

        $wrap.find('button').removeClass('active');
        this._dayDuration = event.target.dataset.duration;
        event.target.classList.add('active');
    }

    /**
     * Get selected data list item template
     * @returns {string}
     * @private
     */
    _getResultListItem() {
        const id = this._selectedDate.length;
        const data = this._selectedDate.find(item => item.id === id);
        const momentDates = data.dates.map(date => (moment(date).format('Do MMMM')));

        return `
            <li class="list-group-item">
                ${momentDates.join(' - ')} in ${data.time}
                <i class="list-group-rm-btn" title="Remove" data-id="${id}">x</i>
            </li>
        `;
    }

    /**
     * Get Boarding save button template
     * @returns {string}
     * @private
     */
    _getSaveButton() {
        return `
            <button class="js-save-button btn btn-sm btn-outline-primary calendar-save-btn">
                Save dates range
            </button>
        `;
    }

    /**
     * Duration type buttons template for daycare
     * @returns {string}
     * @private
     */
    _getTypeButtons() {
        const fullDay = this._dayDuration === 'full' ? 'active' : '';
        const halfDay = this._dayDuration === 'half' ? 'active' : '';

        return `
            <div class="btn-group calendar-btn-group js-duration-buttons" role="group">
                <button type="button" data-duration="full" class="btn btn-outline-primary ${fullDay}">Full Day</button>
                <button type="button" data-duration="half" class="btn btn-outline-primary ${halfDay}">Half Day</button>
            </div>
        `;
    }

    /**
     * Event handler - toggle selected date on Boarding calendar
     * @param removedId
     * @private
     */
    _toggleSelectedDate(removedId = null) {
        if (removedId !== null) {
            return;
        }

        let countDays = moment(this._getRoundedDate(this._currentDate[1]))
            .diff(moment(this._getRoundedDate(this._currentDate[0])), 'days');
        const time = moment(this._currentDate.slice(-1)[0]).format('HH:mm');
        const id = this._selectedDate.length + 1;

        // if before 12 am, ++countDays
        if (moment(this._currentDate.slice(-1)[0]).hours() < 12) {
            ++countDays;
        }

        this._selectedDate.push({ id, countDays, time, dates: this._currentDate });
    }

    /**
     * Toggle selected daycare days
     * @param day
     * @private
     */
    _toggleSelectDay(day = null) {
        this._$calendarPlace.find('.datepicker--cell-day').removeClass('-half-');

        if (day === null) {
            this._selectedDate = [];
            return;
        }

        const index = this._selectedDate.findIndex(item => item.day.toString() === day.toString());

        if (index !== -1) {
            this._selectedDate.splice(index, 1);
        } else {
            this._selectedDate.push({
                id: moment().format('x'),
                day: new Date(day),
                duration: this._dayDuration
            });
        }

        this._debounceToggleHalfDay();
    }

    /**
     * Toggle select day with param 'half'
     * @private
     */
    _toggleHalfDayBg() {
        this._$calendarPlace.find('.datepicker--cell-day').map((i, item) => {
            $(item).removeClass('-half- -selected-');
        });

        if (!this._selectedDate) {
            return;
        }

        this._selectedDate.map(dayData => {
            this._$calendarPlace.find('.datepicker--cell-day').map((i, item) => {
                const $item = $(item);
                const { date, month, year } = item.dataset;
                const momentDay = moment(dayData.day);

                if (
                    (parseInt(date, 10) === momentDay.date()) &&
                    (parseInt(month, 10) === momentDay.month()) &&
                    (parseInt(year, 10) === momentDay.year())
                ) {
                    if (dayData.duration === 'half') {
                        $item.addClass('-selected- -half-');
                    } else {
                        $item.addClass('-selected-');
                    }
                }
            });
        });
    }

    /**
     * Set to 0 in hours, minutes and seconds
     * @param date
     * @returns {moment.Moment}
     * @private
     */
    _getRoundedDate(date) {
        return moment(date).hours(0).minutes(0).seconds(0);
    }

    /**
     * Clear all data, unbind events
     * @returns {Promise<any>}
     * @private
     */
    _clear() {
        return new Promise((resolve, reject) => {
            if (this._calendarBoarding) {
                this._calendarBoarding.data('datepicker').destroy();
                this._$resultList.off('click', '.list-group-rm-btn');
                this._$saveBoardingButton.off('click');
                this._$saveBoardingButton.remove();
                this._$resultList.empty();
            }

            if (this._calendarDaycare) {
                this._calendarDaycare.data('datepicker').destroy();
                this._$dayDurationButtons.off('click');
                this._$dayDurationButtons.remove();
            }

            this._currentInst = null;
            this._calendarBoarding = null;
            this._calendarDaycare = null;
            this._selectedDate = [];
            this._currentDate = [];
            this._dayDuration = null;

            resolve();
        });
    }
}
