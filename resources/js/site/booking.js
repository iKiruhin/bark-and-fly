import $ from 'jquery';

import Booking from '../modules/booking';

$(() => {
    const booking = new Booking({
        el: '.js-booking-calculator'
    });

    booking.init();
});