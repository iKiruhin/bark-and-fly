import $ from 'jquery';
import 'bootstrap/dist/js/bootstrap.min';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'owl.carousel/dist/owl.carousel.min';
import '@fengyuanchen/datepicker/dist/datepicker.min';

var App = (function ($) {
    var breakPoints = {
        desktop: 1280,
        laptop: 992,
        tablet: 768,
        mobile: 592
    };

    var winWidth = $(window).outerWidth();
    var winHeight = $(window).outerHeight();

    var pub = {
        setWindowSizes: function() {
            $(window).on('resize', function(){
                winWidth = $(window).outerWidth();
                winHeight = $(window).outerHeight();
            });
        },
        handleCarousels: function() {
            var $carousels = $('.carousel');
            var standardCarousels = [];
            var screenSizes = {
                'laptop': breakPoints.laptop,
                'tablet': breakPoints.tablet,
                'mobile': breakPoints.mobile
            };

            $carousels.each(function (i, e) {
                var $carousel = $(e);
                var $section = $carousel.parents('.slider');
                var options = {
                    items: 1,
                    smartSpeed: 600,
                    autoplayHoverPause: false,
                    loop: true,
                    dots: false,
                    startPosition: 0,
                    responsiveBaseElement: '.slider'
                };

                standardCarousels[i] = $carousel;

                if ($carousel.is('[data-carousel-with-fade]')) {
                    options['animateOut'] = 'fadeOut';
                    options['animateIn'] = 'fadeIn';
                }

                if ($carousel.is('[data-carousel-with-nav]')) {
                    options['nav'] = true;
                }

                if ($carousel.is('[data-carousel-start-position]')) {
                    options['startPosition'] = $carousel.data('carousel-start-position');
                }

                if ($carousel.is('[data-carousel-without-loop]')) {
                    options['loop'] = false;
                }

                if ($carousel.is('[data-carousel-without-autoplay]')) {
                    options['autoplay'] = false;
                }

                if ($carousel.is('[data-carousel-with-dots]')) {
                    options['dots'] = true;
                }

                if ($carousel.is('[data-carousel-navtext]')) {
                    options['navText'] = $carousel.attr('data-carousel-navtext').split('|');
                }

                if ($carousel.is('[data-carousel-auto-width]')) {
                    options['autoWidth'] = true;
                }

                if ($carousel.is('[data-carousel-position-center]')) {
                    options['center'] = true;
                }

                if ($carousel.is('[data-carousel-stage-padding]')) {
                    options['stagePadding'] = $carousel.attr('data-carousel-stage-padding');
                }

                if ($carousel.is('[data-carousel-page-slide]')) {
                    options['slideBy'] = 'page';
                }

                if ($carousel.is('[data-carousel-items]')) {
                    options['items'] = $carousel.attr('data-carousel-items');
                }

                if ($carousel.is('[data-carousel-items-laptop]') && $(window).width() < screenSizes.laptop) {
                    options['items'] = $carousel.attr('data-carousel-items-laptop');
                }

                if ($carousel.is('[data-carousel-items-tablet]') && $(window).width() < screenSizes.tablet) {
                    options['items'] = $carousel.attr('data-carousel-items-tablet');
                }

                if ($carousel.is('[data-carousel-items-mobile]') && $(window).width() < screenSizes.mobile) {
                    options['items'] = $carousel.attr('data-carousel-items-mobile');
                }

                if ($carousel.is('[data-carousel-sizes-enabled]')) {
                    var sizesArray = $carousel.attr('data-carousel-sizes-enabled').split('|');
                    var skipCarousel = true;

                    for (var y in sizesArray) {
                        if (sizesArray.hasOwnProperty(y) && sizesArray[y] === 'desktop' && $(window).width() >= screenSizes.laptop) {
                            skipCarousel = false;
                            break;
                        } else if (sizesArray.hasOwnProperty(y) && sizesArray[y] === 'laptop' && $(window).width() < screenSizes.laptop && $(window).width() >= screenSizes.tablet) {
                            skipCarousel = false;
                            break;
                        } else if (sizesArray.hasOwnProperty(y) && sizesArray[y] === 'tablet' && $(window).width() < screenSizes.tablet && $(window).width() >= screenSizes.mobile) {
                            skipCarousel = false;
                            break;
                        } else if (sizesArray.hasOwnProperty(y) && sizesArray[y] === 'mobile' && $(window).width() < screenSizes.mobile) {
                            skipCarousel = false;
                            break;
                        }
                    }

                    if (skipCarousel) {
                        return true; //continue
                    }
                }

                $carousel.owlCarousel(options);

                $carousel.on('changed.owl.carousel', function (event) {
                    var index = event.page.index;
                    var loopIndex = index + 1;

                    standardCarousels[i].active_index = index;

                    if ($carousel.is('[data-carousel-with-links]')) {
                        $section.find('.active-link').removeClass('active');
                        $section.find('.active-link[data-loop-index*='|' + loopIndex + '|']').addClass('active');
                    }

                    if ($carousel.is('[data-carousel-with-count]')) {
                        $section.find('.slide-nav .slide-count').empty().append((loopIndex.toString().length === 1 ? '0' : '') + loopIndex);
                    }

                    if ($carousel.is('[data-carousel-with-progress]')) {
                        var $progressBar = $section.find('.progress-bar'),
                        itemCount = event.item.count - 1;
                        $section.find('.active-link[data-loop-index*='|' + event.item.index + '|']').addClass('current').removeClass('active').prevAll().addClass('active current');
                        $section.find('.active-link[data-loop-index*='|' + event.item.index + '|']').nextAll().removeClass('active current');

                        $progressBar.find('.fill-bar').css({'width': (event.item.index / itemCount) * 100 + '%'});
                    }
                });

                if ($carousel.is('[data-carousel-with-arrows]')) {
                    $section.find('.next').on('click', function (e) {
                        e.preventDefault();
                        $carousel.trigger('next.owl.carousel');
                    });

                    $section.find('.prev').on('click', function (e) {
                        e.preventDefault();
                        $carousel.trigger('prev.owl.carousel');
                    });
                }

                $section.find('a[data-carousel-goto]').on('click mouseover mouseout', function (e) {
                    e.preventDefault();

                    var $a = $(this);
                    var newIndex = parseInt($a.attr('data-carousel-goto')) - (options['loop'] ? -1 : 0);
                    var hashSplit = $a.attr('href').split('#');

                    if (e.type === 'mouseover') {
                        if (!$carousel.is('[data-carousel-with-progress]')) {
                            $a.addClass('active').siblings().removeClass('active');
                        }

                        $carousel.trigger('stop.owl.autoplay');
                        return;
                    } else if (e.type === 'mouseout') {
                        $carousel.trigger('play.owl.autoplay', [5000]);

                        if (!$carousel.is('[data-carousel-with-progress]')) {
                            $a.parent().find('a[data-carousel-goto]').eq(standardCarousels[i].active_index).addClass('active').siblings().removeClass('active');
                        }
                        return;
                    }

                    if (hashSplit[1]) {
                        window.location.hash = hashSplit[1];
                    }

                    if (typeof newIndex == 'number' && !isNaN(newIndex)) {
                        $carousel.trigger('to.owl.carousel', newIndex);
                    }
                });
            });
        },
        toggleMenu: function() {
            $('.header__bar, .header__overlay').on('click', function(e){
                e.preventDefault();
                $('.header__bar .bar').toggleClass('active');
                $('.header__bar').toggleClass('open');
                $('.header__migrant').toggleClass('open');
                $('.header__overlay').toggleClass('open');
            });
        },
        btnLabel: function () {
            $(document).on('change', '.btn-label>input', function(){
                var _this = $(this);
                var _parent = _this.parent();
                var _rel = _parent.attr('rel');
                var _type = _this.attr('type');

                if(_type == 'checkbox') {
                    if(_this.is(':checked')) {
                        _parent.addClass('active');
                    } else {
                        _parent.removeClass('active');
                    }
                } else if(_type == 'radio') {
                    if(!_parent.hasClass('active')) {
                        $('.btn-label[rel="'+_rel+'"]').not(_parent).removeClass('active');
                        _parent.addClass('active');
                    } else {
                        _this.prop('checked', false);
                        _parent.removeClass('active');
                    }
                }
            });
        },
        init: function () {
            pub.setWindowSizes();
            pub.handleCarousels();
            pub.toggleMenu();
            pub.btnLabel();

            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="datepicker"]').datepicker();
        }
    }

    return pub;
})($);

$(() => {
    App.init();
});
