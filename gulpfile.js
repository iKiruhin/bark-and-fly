'use strict';

const path = require('path');
const gulp = require('gulp');
const gulplog = require('gulplog');
const babel = require('gulp-babel');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const useref = require('gulp-useref');
const uglify = require('gulp-uglify');
const gulpIf = require('gulp-if');
const cssnano = require('gulp-cssnano');
const imagemin = require('gulp-imagemin');
const cache = require('gulp-cache');
const del = require('del');
const runSequence = require('run-sequence');
const browserify = require('gulp-browserify');
const twig = require('gulp-twig');
const notify = require('gulp-notify');
const concat = require('gulp-concat');
const svgSprite = require('gulp-svg-sprite');
const webpackStream = require('webpack-stream');
const webpack = webpackStream.webpack;
const plumber = require('gulp-plumber');
const named = require('vinyl-named');
const notifier = require('node-notifier');
const AssetsPlugin = require('assets-webpack-plugin');
const revReplace = require('gulp-rev-replace');
const livereload = require('livereload');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

if(isDevelopment) {
    const server = livereload.createServer({
        port: 35729,
        debug: true,
    });
}

gulp.task('browser-sync', function() {
    browserSync.init({
        port: 8011,
        // open: false,
        server: 'public',
        // reloadDebounce: 1000,
        // watch: true,
        // files: [
        //      './resources/**/*.*',
        //     './craft/**/*.*',
        //     './public/**/*.*',
        // ],
    });
    server.watch(__dirname + '/public');
});

gulp.task('styles:sass', function() {
    return gulp.src('resources/scss/**/*.scss') // Gets all files ending with .scss in resources/scss
        .pipe(sass({ outputStyle: 'compressed' }))
        .on('error', notify.onError({
            message: "<%= error.message %>",
            title: "SASS Error"
        }))
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/assets/css'))
        .pipe(browserSync.stream({
            match: '**/*.css'
        }));
});

gulp.task('assets', function() {
    return gulp.src('resources/js/**/*.*', { since: gulp.lastRun('assets') })
        .pipe(gulpIf(!isDevelopment, revReplace({
            manifest: gulp.src('manifest/webpack.json', { allowEmpty: true })
        })))
        .pipe(gulp.dest('public/assets/js'));
});

gulp.task('webpack', function(callback) {
    let firstBuildReady = false;
    const options = {
        output: {
            publicPath: '/assets/js/',
            filename: isDevelopment ? '[name].js' : '[name]-[chunkhash:10].js',
        },
        entry: {
            main: './resources/js/site/app',
            booking: './resources/js/site/booking',
        },
        watch: isDevelopment,
        devtool: isDevelopment ? 'cheap-module-inline-source-map' : false,
        module: {
            loaders: [{
                test: /\.js$/,
                include: path.join(__dirname, 'resources/js/'),
                loader: 'babel-loader?presets[]=env',
            }, {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            }, {
                test: /\.scss$/,
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader"
                }, {
                    loader: "sass-loader"
                }]
            }],
        },
        plugins: [
            new webpack.NoEmitOnErrorsPlugin(),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                'window.$': 'jquery',
            }),
        ],
    };

    function done(err, stats) {
        firstBuildReady = true;

        if (err) {
            return;
        }

        gulplog[stats.hasErrors() ? 'error' : 'info'](stats.toString({
            colors: true
        }));
    }

    if (!isDevelopment) {
        options.plugins.push(new AssetsPlugin({
            filename: 'webpack.json',
            path: __dirname + '/manifest',
            processOutput(assets) {
                for (let key in assets) {
                    assets[key + '.js'] = assets[key].js.slice(options.output.publicPath.length);
                    delete assets[key];
                }
                return JSON.stringify(assets);
            }
        }));
    }

    return gulp.src('resources/js/site/**/*.js')
        .pipe(plumber({
            errorHandler: notify.onError(err => ({
                title: 'Webpack',
                message: err.message,
            }))
        }))
        .pipe(named())
        .pipe(webpackStream(options, null, done))
        .pipe(gulpIf(!isDevelopment, uglify()))
        .pipe(gulp.dest('public/assets/js'))
        .on('data', function() {
            if (firstBuildReady) {
                callback();
            }
        });
});

gulp.task('useref', function() {
    return gulp.src('public/assets/*.html')
        .pipe(useref())
        .pipe(gulpIf('*.js', uglify()))
        .pipe(browserify())
        .pipe(babel({
            presets: ['env']
        }))
        // Minifies only if it's a CSS file
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulp.dest('public/assets'));
});

// Compress all images and move them to /public/assets/images
gulp.task('images', function() {
    return gulp.src('resources/images/**/*.+(png|jpg|jpeg|gif|svg)')
    // TODO: when 1st run, image not found in public/assets
    // Caching images that ran through imagemin
        .pipe(cache(imagemin({
            interlaced: true
        })))
        .pipe(gulp.dest('public/assets/images'));
});

// Moves all fonts into /public/assets folder
gulp.task('fonts', function() {
    return gulp.src('resources/fonts/**/*.*')
        .pipe(gulp.dest('public/assets/fonts'));
});

// Gulp will delete the `public/assets` folder for you whenever gulp clean:assets is run.
gulp.task('clean:assets', function() {
    return del([
        'public/assets/css',
        'public/assets/fonts',
        'public/assets/images',
        'public/assets/js'
    ]);
});

gulp.task('reload', function() {
    browserSync.reload({ stream: true });
});

gulp.task('watch', function(done) {
    gulp.watch('resources/images/**/*.+(png|jpg|jpeg|gif|svg)', gulp.series('images'));
    gulp.watch('resources/scss/**/*.scss', gulp.series('styles:sass'));
    gulp.watch('craft/**/*.html').on('change', function() {
        return server.refresh('public');
    });
    gulp.watch('craft/**/*.twig').on('change', function() {
        return server.refresh('public');
    });
    gulp.watch('resources/fonts/**/*.*', gulp.series('fonts'));
    gulp.watch('resources/js/**/*.js', gulp.series('webpack', 'assets'));
    gulp.watch('public/**/*.*').on('change', function() {
        browserSync.reload({ stream: true });
    });
    return done();
});

gulp.task('icons:svg', function() {
    return gulp.src('resources/images/svg/*.svg')
        .pipe(svgSprite({
            // Relative path from the stylesheet resource to the SVG sprite
            shape: {
                dimension: {      // Set maximum dimensions
                    maxWidth: 32,
                    maxHeight: 32
                },
                spacing: {      // Add padding
                    padding: 10
                },
                dest: 'svg'  // Keep the intermediate files
            },
            mode: {
                symbol: true    // Activate the �symbol� mode
            }
        }))
        .pipe(gulp.dest('public/assets/images/svg'));
});

gulp.task('build', gulp.series(
    'clean:assets',
    'icons:svg',
    gulp.parallel('fonts', 'images', 'styles:sass'),
    'assets',
    'webpack'
));

gulp.task('default', gulp.series('build', 'watch', 'browser-sync'));

gulp.task('dev', gulp.series('build', 'watch', 'browser-sync'));
